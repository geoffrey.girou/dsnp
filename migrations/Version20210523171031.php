<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210523171031 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE siret (id INT AUTO_INCREMENT NOT NULL, siren_id INT NOT NULL, siret_group_id INT DEFAULT NULL, nic INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_26E94372C1288C9 (siren_id), INDEX IDX_26E94372EE1EB795 (siret_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE siret_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE siret ADD CONSTRAINT FK_26E94372C1288C9 FOREIGN KEY (siren_id) REFERENCES siren (id)');
        $this->addSql('ALTER TABLE siret ADD CONSTRAINT FK_26E94372EE1EB795 FOREIGN KEY (siret_group_id) REFERENCES siret_group (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE siret DROP FOREIGN KEY FK_26E94372EE1EB795');
        $this->addSql('DROP TABLE siret');
        $this->addSql('DROP TABLE siret_group');
    }
}
