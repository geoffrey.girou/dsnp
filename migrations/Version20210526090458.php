<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210526090458 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE siret DROP FOREIGN KEY FK_26E94372EE1EB795');
        $this->addSql('DROP TABLE siret_group');
        $this->addSql('DROP INDEX IDX_26E94372EE1EB795 ON siret');
        $this->addSql('ALTER TABLE siret DROP siret_group_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE siret_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE siret ADD siret_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE siret ADD CONSTRAINT FK_26E94372EE1EB795 FOREIGN KEY (siret_group_id) REFERENCES siret_group (id)');
        $this->addSql('CREATE INDEX IDX_26E94372EE1EB795 ON siret (siret_group_id)');
    }
}
