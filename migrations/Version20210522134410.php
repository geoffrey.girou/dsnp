<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210522134410 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE siren_access (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, siren_id INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_40870DB8A76ED395 (user_id), INDEX IDX_40870DB8C1288C9 (siren_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE siren_access ADD CONSTRAINT FK_40870DB8A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE siren_access ADD CONSTRAINT FK_40870DB8C1288C9 FOREIGN KEY (siren_id) REFERENCES siren (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE siren_access');
    }
}
