<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210418135824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD is_reset_password TINYINT(1) DEFAULT NULL, ADD reset_password_asked_at DATETIME DEFAULT NULL, ADD password_updated_at DATETIME DEFAULT NULL, ADD reset_password_link_sent_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP is_reset_password, DROP reset_password_asked_at, DROP password_updated_at, DROP reset_password_link_sent_at, DROP updated_at');
    }
}
