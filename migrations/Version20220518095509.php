<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220518095509 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE businessteam_user (team_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_7A8269C4296CD8AE (team_id), INDEX IDX_7A8269C4A76ED395 (user_id), PRIMARY KEY(team_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE managerteam_user (team_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_86D81F36296CD8AE (team_id), INDEX IDX_86D81F36A76ED395 (user_id), PRIMARY KEY(team_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE businessteam_user ADD CONSTRAINT FK_7A8269C4296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE businessteam_user ADD CONSTRAINT FK_7A8269C4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE managerteam_user ADD CONSTRAINT FK_86D81F36296CD8AE FOREIGN KEY (team_id) REFERENCES team (id)');
        $this->addSql('ALTER TABLE managerteam_user ADD CONSTRAINT FK_86D81F36A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD github_id VARCHAR(255) DEFAULT NULL, ADD google_id VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE businessteam_user DROP FOREIGN KEY FK_7A8269C4296CD8AE');
        $this->addSql('ALTER TABLE managerteam_user DROP FOREIGN KEY FK_86D81F36296CD8AE');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE businessteam_user');
        $this->addSql('DROP TABLE managerteam_user');
        $this->addSql('ALTER TABLE user DROP github_id, DROP google_id');
    }
}
