<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210523105500 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE dsn_file_declaration (id INT AUTO_INCREMENT NOT NULL, dsn_file_id INT DEFAULT NULL, siret VARCHAR(255) NOT NULL, month DATETIME NOT NULL, personnel_numbers_count INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_7374DD2680F47783 (dsn_file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dsn_file_declaration ADD CONSTRAINT FK_7374DD2680F47783 FOREIGN KEY (dsn_file_id) REFERENCES dsn_file (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE dsn_file_declaration');
    }
}
