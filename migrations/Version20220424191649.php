<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220424191649 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dsn_file_declaration DROP FOREIGN KEY FK_7374DD2680F47783');
        $this->addSql('ALTER TABLE dsn_file_declaration CHANGE dsn_file_id dsn_file_id INT NOT NULL');
        $this->addSql('ALTER TABLE dsn_file_declaration ADD CONSTRAINT FK_7374DD2680F47783 FOREIGN KEY (dsn_file_id) REFERENCES dsn_file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user DROP github_id, DROP google_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE dsn_file_declaration DROP FOREIGN KEY FK_7374DD2680F47783');
        $this->addSql('ALTER TABLE dsn_file_declaration CHANGE dsn_file_id dsn_file_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dsn_file_declaration ADD CONSTRAINT FK_7374DD2680F47783 FOREIGN KEY (dsn_file_id) REFERENCES dsn_file (id)');
        $this->addSql('ALTER TABLE user ADD github_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD google_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
