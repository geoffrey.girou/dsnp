<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210624142832 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE group_siret DROP FOREIGN KEY FK_76C5681533B63F32');
        $this->addSql('DROP TABLE group_siret');
        $this->addSql('DROP TABLE siret');
        $this->addSql('ALTER TABLE `group` ADD sirets LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE group_siret (group_id INT NOT NULL, siret_id INT NOT NULL, INDEX IDX_76C56815FE54D947 (group_id), INDEX IDX_76C5681533B63F32 (siret_id), PRIMARY KEY(group_id, siret_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE siret (id INT AUTO_INCREMENT NOT NULL, siren_id INT NOT NULL, nic INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_26E94372C1288C9 (siren_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE group_siret ADD CONSTRAINT FK_76C5681533B63F32 FOREIGN KEY (siret_id) REFERENCES siret (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_siret ADD CONSTRAINT FK_76C56815FE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE siret ADD CONSTRAINT FK_26E94372C1288C9 FOREIGN KEY (siren_id) REFERENCES siren (id)');
        $this->addSql('ALTER TABLE `group` DROP sirets');
    }
}
