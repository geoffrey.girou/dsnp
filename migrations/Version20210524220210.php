<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210524220210 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE siret DROP INDEX UNIQ_26E94372C1288C9, ADD INDEX IDX_26E94372C1288C9 (siren_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE siret DROP INDEX IDX_26E94372C1288C9, ADD UNIQUE INDEX UNIQ_26E94372C1288C9 (siren_id)');
    }
}
