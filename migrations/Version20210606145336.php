<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210606145336 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE wizard ADD is_first_siren_added TINYINT(1) NOT NULL, ADD is_first_dsn_file_added_and_status_ok TINYINT(1) NOT NULL, DROP is_siren_view_displayed, DROP is_dsn_file_view_displayed, DROP is_dashboard_view_displayed');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE wizard ADD is_siren_view_displayed TINYINT(1) NOT NULL, ADD is_dsn_file_view_displayed TINYINT(1) NOT NULL, ADD is_dashboard_view_displayed TINYINT(1) NOT NULL, DROP is_first_siren_added, DROP is_first_dsn_file_added_and_status_ok');
    }
}
