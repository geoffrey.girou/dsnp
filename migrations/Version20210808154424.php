<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210808154424 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE siren_access DROP FOREIGN KEY FK_40870DB8C1288C9');
        $this->addSql('ALTER TABLE siren_access ADD CONSTRAINT FK_40870DB8C1288C9 FOREIGN KEY (siren_id) REFERENCES siren (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE siren_access DROP FOREIGN KEY FK_40870DB8C1288C9');
        $this->addSql('ALTER TABLE siren_access ADD CONSTRAINT FK_40870DB8C1288C9 FOREIGN KEY (siren_id) REFERENCES siren (id)');
    }
}
