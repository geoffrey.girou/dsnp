<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210506151807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE dsnp_data (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', roles LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', matricule LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', UNIQUE INDEX UNIQ_23061C69A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dsnp_data ADD CONSTRAINT FK_23061C69A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE dsn_file DROP INDEX UNIQ_C96564A2A76ED395, ADD INDEX IDX_C96564A2A76ED395 (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE dsnp_data');
        $this->addSql('ALTER TABLE dsn_file DROP INDEX IDX_C96564A2A76ED395, ADD UNIQUE INDEX UNIQ_C96564A2A76ED395 (user_id)');
    }
}
