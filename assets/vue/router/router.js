import Vue from 'vue';
import stores from '../stores';
import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

let router = new VueRouter({
  routes: routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (stores.getters['logged'] === true) {
      next();
    } else {
      stores.dispatch('onRefresh')
        .then(() => {
          next()
        })
        .catch(() => {
          if (to.fullPath !== '/') {
            next({
              path: "/connexion",
              query: {redirect: to.fullPath}
            });
          } else {
            next({
              path: "/connexion"
            });
          }
        }
        );
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router;