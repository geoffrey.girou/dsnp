import Home from '../pages/Home'
import Login from '../pages/Login'
import Profile from '../pages/User/Profile/Profile'
import Register from '../pages/Register'
import OauthRegister from '../pages/OauthRegister'
import ForgotterPassword from '../pages/ForgotterPassword'
import DsnDashboard from '../pages/DsnDashboard'
import SirenList from '../pages/User/Siren/SirenList'
import ChildManagement from '../pages/User/Child/ChildList'
import AddChild from '../pages/User/Child/AddChild'
import EditChild from '../pages/User/Child/EditChild'
import DsnFileManagement from '../pages/User/DsnFile/DsnFileList'
import GroupManagement from '../pages/User/Group/GroupList'
import EditGroup from '../pages/User/Group/EditGroup'
import AccountActivation from '../pages/AccountActivation'
import TermsOfService from '../pages/TermsOfService'
import Privacy from '../pages/Privacy'
import Cookies from '../pages/Cookies'
import LegalNotice from '../pages/LegalNotice'
import AddGroup from "../pages/User/Group/AddGroup";
import Maintenance from "../pages/Maintenance";
import AdminHome from '../pages/Admin/AdminHome'
import AdminAddManager from "../pages/Admin/Manager/AdminAddManager";
import AdminEditManager from "../pages/Admin/Manager/AdminEditManager";
import AdminAddTeam from "../pages/Admin/Team/AdminAddTeam";
import AdminEditTeam from "../pages/Admin/Team/AdminEditTeam";
import ManagerTeamList from "../pages/Manager/Team/ManagerTeamList";
import ManagerBusinessList from "../pages/Manager/Business/ManagerBusinessList";
import ManagerAddBusiness from "../pages/Manager/Business/ManagerAddBusiness";

const routes = [
  {
    path: '/',
    redirect: { path: '/connexion' }
  },
  {
    path: '/maintenance',
    component: Maintenance,
    name: 'maintenance'
  },
  {
    path: '/conditions-generales-utilisation',
    component: TermsOfService,
    name: 'tos'
  },
  {
    path: '/politique-confidentialite',
    component: Privacy,
    name: 'privacy'
  },
  {
    path: '/utilisation-cookies',
    component: Cookies,
    name: 'cookies'
  },
  {
    path: '/mentions-legales',
    component: LegalNotice,
    name: 'legal-notice'
  },
  {
    path: '/tableau-de-bord',
    component: Home,
    name: 'home',
    meta: {requiresAuth: true},
    children: [
      {
        path: '/compte-utilisateur',
        component: Profile,
        meta: {requiresAuth: true},
      },
      {
        path: '/sirens',
        component: SirenList,
        meta: {requiresAuth: true},
      },
      {
        path: '/groupements',
        component: GroupManagement,
        meta: {requiresAuth: true},
      },
      {
        path: '/groupements/ajouter',
        component: AddGroup,
        meta: {requiresAuth: true},
      },
      {
        path: '/groupements/modifier/:groupIdentifier',
        component: EditGroup,
        meta: {requiresAuth: true},
      },
      {
        path: '/utilisateurs',
        component: ChildManagement,
        meta: {requiresAuth: true},
      },
      {
        path: '/utilisateurs/ajouter',
        component: AddChild,
        meta: {requiresAuth: true},
      },
      {
        path: '/utilisateurs/modifier/:id',
        component: EditChild,
        meta: {requiresAuth: true},
      },
      {
        path: '/fichiers-dsn',
        component: DsnFileManagement,
        meta: {requiresAuth: true},
      },
      {
        path: '/tableau-de-bord',
        component: DsnDashboard,
        meta: {requiresAuth: true},
      },
    ],
  },
  {
    path: '/admin',
    component: Home,
    name: 'home',
    meta: {requiresAuth: true},
    children: [
      {
        path: '/admin/accueil',
        component: AdminHome,
        meta: {requiresAuth: true},
      },
      {
        path: '/admin/gestionnaire/ajouter',
        component: AdminAddManager,
        meta: {requiresAuth: true},
      },
      {
        path: '/admin/gestionnaire/modifier/:id',
        component: AdminEditManager,
        meta: {requiresAuth: true},
      },
      {
        path: '/admin/equipe/ajouter',
        component: AdminAddTeam,
        meta: {requiresAuth: true},
      },
      {
        path: '/admin/equipe/modifier/:id',
        component: AdminEditTeam,
        meta: {requiresAuth: true},
      },
    ]
  },
  {
    path: '/gestionnaire',
    component: Home,
    name: 'home',
    meta: {requiresAuth: true},
    children: [
      {
        path: '/gestionnaire/equipes',
        component: ManagerTeamList,
        meta: {requiresAuth: true},
      },
      {
        path: '/gestionnaire/societes',
        component: ManagerBusinessList,
        meta: {requiresAuth: true},
      },
      {
        path: '/gestionnaire/societe/ajouter',
        component: ManagerAddBusiness,
        meta: {requiresAuth: true},
      },
    ]
  },
  {
    path: '/connexion/:email?',
    name: 'login',
    component: Login,
  },
  {
    path: '/inscription',
    name: 'register',
    component: Register,
  },
  {
    path: '/complete_inscription',
    name: 'oauthRegister',
    component: OauthRegister,
  },
  {
    path: '/compte-activation/:email?',
    name: 'account-activation',
    component: AccountActivation,
  },
  {
    path: '/mot-de-passe-oublie',
    name: 'forgotten-password',
    component: ForgotterPassword,
  },
  {
    path: '/support',
    beforeEnter() {location.href = 'https://support.dsnplus.fr'}
  }
]
export default routes