import api from '../api';

export default {
  one(groupIdentifier) {
    return api
      .get('/api/group/'+groupIdentifier)
      .then((response) => response.data);
  },
  all() {
    return api
      .get('/api/groups')
      .then((response) => response.data);
  },
  create(group) {
    return api
      .post('/api/groups', group)
      .then((response) => response.data);
  },
  delete(id) {
    return api
      .delete('/api/groups/'+id)
      .then((response) => response.data);
  },
  update(group) {
    return api
      .put('/api/groups', group)
      .then((response) => response.data);
  }
}