import api from '../api';

export default {
  welcomeOk() {
    return api
      .post('/api/wizards/welcome-ok')
      .then((response) => response.data);
  },
  firstSirenAdded() {
    return api
      .post('/api/wizards/first-siren-added')
      .then((response) => response.data);
  },
  firstDsnFileAddedAndStatusOk() {
    return api
      .post('/api/wizards/first-dsn-file-added')
      .then((response) => response.data);
  },
}