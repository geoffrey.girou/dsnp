import api from '../api';

export default {
  sendArchive(formData) {
    return api.post('/api/dsnarchive/uploadarchive', formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    ).then((response) => response.data);
  }
}