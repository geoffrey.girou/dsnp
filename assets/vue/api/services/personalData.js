import api from '../api';

export default {
  personalData(userId,personalDataId) {
    return api.get('/api/users/'+userId+'/personal-datas'+personalDataId)
      .then((response) => response.data);
  },
  create(personalData) {
    return api.post('/api/users/'+personalData.userId+'/personal-datas', personalData)
      .then((response) => response.data);
  },
  update(userId,personalData) {
    return api.put('/api/users/'+userId+'/personal-datas', personalData)
      .then((response) => response.data);
  },
}