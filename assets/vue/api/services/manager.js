import api from '../api';

export default {
  managers() {
    return api.get('/api/managers')
      .then((response) => response.data);
  },
  manager(id) {
    return api.get('/api/managers/'+id)
      .then((response) => response.data);
  },
  create(email) {
    return api.post('/api/managers', {email : email})
      .then((response) => response.data);
  },
  update(manager) {
    return api.put('/api/managers/'+manager.id,manager)
      .then((response) => response.data);
  },
  delete(id) {
    return api.delete('/api/managers/'+id)
      .then((response) => response.data);
  },
}