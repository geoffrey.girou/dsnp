import api from '../api';

export default {
  businesses() {
    return api.get('/api/businesses')
      .then((response) => response.data);
  },
  create() {
    return api.post('/api/businesses')
      .then((response) => response.data);
  },
  valid(id) {
    return api.get('/api/businesses/'+id+'/valid')
      .then((response) => response.data);
  },
  invalid(id) {
    return api.get('/api/businesses/'+id+'/invalid')
      .then((response) => response.data);
  },
  lock(id) {
    return api.get('/api/businesses/'+id+'/lock')
      .then((response) => response.data);
  },
  unlock(id) {
    return api.get('/api/businesses/'+id+'/unlock')
      .then((response) => response.data);
  }
}