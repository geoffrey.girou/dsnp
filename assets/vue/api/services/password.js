import api from '../api';

export default {
  passwordCode(email) {
    return api
      .get('/password/code?email='+email)
      .then((response) => response.data);
  },
  checkCode(codeAndEmail) {
    return api
      .post('/password/code', codeAndEmail)
      .then((response) => response.data);
  },
  changePassword(emailCodeAndPassword) {
    return api
      .post('/password/change', emailCodeAndPassword)
      .then((response) => response.data);
  }
}