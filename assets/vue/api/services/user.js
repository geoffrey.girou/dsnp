import api from '../api';

export default {
  profile() {
    return api
      .get('/api/user/profile')
      .then((response) => response.data);
  },
  updateProfile(profile) {
    return api
      .put('/api/user/profile', profile)
      .then((response) => response.data);
  },
  changePassword(email, code, password) {
    return api
      .post('/changePassword', {
        'email': email,
        'code': code,
        'password': password,
      })
      .then((response) => response.data);
  },
  fetchChildren() {
    return api
      .get('/api/child')
      .then((response) => response.data);
  },
  fetchChild(id) {
    return api
      .get('/api/child/'+id)
      .then((response) => response.data);
  },
  createChild(child) {
    return api
      .post('/api/child', child)
      .then((response) => response.data);
  },
  updateChild(child) {
    return api
      .put('/api/child', child)
      .then((response) => response.data);
  },
  deleteChild(id) {
    return api
      .delete('/api/child/'+id)
      .then((response) => response.data);
  }
}