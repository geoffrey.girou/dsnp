import api from '../api';

export default {
  all(filters = {}) {
    return api
      .get('/api/sirens', { params: filters })
      .then((response) => response.data);
  },
  toDelete(id) {
    return api
      .get('/api/sirens/'+id+'/todelete')
      .then((response) => response.data);
  },
  allIntegrated() {
    return api
      .get('/api/sirens?integrated=true')
      .then((response) => response.data);
  },
  create(siren) {
    return api
      .post('/api/sirens', siren)
      .then((response) => response.data);
  },
  update(siren) {
    return api
      .put('/api/sirens', siren)
      .then((response) => response.data);
  },
  delete(id) {
    return api
      .delete('/api/sirens/'+id)
      .then((response) => response.data);
  }
}