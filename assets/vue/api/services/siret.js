import api from '../api';

export default {
  all() {
    return api
      .get('/api/sirets')
      .then((response) => response.data);
  }
}