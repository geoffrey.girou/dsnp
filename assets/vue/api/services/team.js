import api from '../api';

export default {
  teams() {
    return api.get('/api/teams')
      .then((response) => response.data);
  },
  team(id) {
    return api.get('/api/teams/'+id)
      .then((response) => response.data);
  },
  create(description) {
    return api.post('/api/teams', {description : description})
      .then((response) => response.data);
  },
  update(team) {
    return api.put('/api/teams/'+team.id,team)
      .then((response) => response.data);
  },
  delete(id) {
    return api.delete('/api/teams/'+id)
      .then((response) => response.data);
  },
  addManager(teamId,managerId) {
    return api.post('/api/teams/'+teamId+'/managers',{managerId: managerId})
      .then((response) => response.data);
  },
  addBusiness(teamId,businessId) {
    return api.post('/api/teams/'+teamId+'/businesses',{businessId: businessId})
      .then((response) => response.data);
  },
  deleteManager(teamId,managerId) {
    return api.delete('/api/teams/'+teamId+'/managers/'+managerId)
      .then((response) => response.data);
  },
  deleteBusiness(teamId,businessId) {
    return api.delete('/api/teams/'+teamId+'/businesses/'+businessId)
      .then((response) => response.data);
  },
}