import api from '../api';

export default {
  access() {
    return api.get('/api/tableau-de-bord')
      .then((response) => response.data);
  }
}