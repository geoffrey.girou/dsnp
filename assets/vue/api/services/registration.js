import api from '../api';

export default {
  register(email,password,companyName,firstname,lastname,
    addressLine1,addressLine2,postalCode,city,phoneNumber) {
    return api.post('/register', {
      email: email,
      password: password,
      companyName: companyName,
      firstname: firstname,
      lastname: lastname,
      addressLine1: addressLine1,
      addressLine2: addressLine2,
      postalCode: postalCode,
      phoneNumber: phoneNumber,
      city: city,
    }).then((response) => response.data);
  },
  oauthRegister(companyName,firstname,lastname,
    addressLine1,addressLine2,postalCode,city,phoneNumber) {
    return api.post('/oauth/register', {
      companyName: companyName,
      firstname: firstname,
      lastname: lastname,
      addressLine1: addressLine1,
      addressLine2: addressLine2,
      postalCode: postalCode,
      phoneNumber: phoneNumber,
      city: city,
    }).then((response) => response.data);
  }
}