import api from '../api';

export default {
  login(email,password) {
    return api.post('/login', {
      username: email,
      password: password,
    }).then((response) => response.data);
  },
  logout() {
    return api.get('/logout')
      .then((response) => response);
  },
  refresh() {
    return api.get('/refresh')
      .then((response) => response);
  },
  connect(service) {
    return api.get('/connect/'+service)
      .then((response) => response.data);
  },
  impersonate(email) {
    return api.get('/api/impersonate?_switch_user='+email)
      .then((response) => response.data);
  },
  exitImpersonate() {
    return api.get('/api/impersonate?_switch_user=_exit')
      .then((response) => response.data);
  }
}