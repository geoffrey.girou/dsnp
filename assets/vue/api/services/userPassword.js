import api from '../api';

export default {
  passwordCode() {
    return api
      .get('/api/password/code')
      .then((response) => response.data);
  },
  checkCode(code) {
    return api
      .post('/api/password/code', {code})
      .then((response) => response.data);
  },
  changePassword(codeAndPassword) {
    return api
      .post('/api/password/change', codeAndPassword)
      .then((response) => response.data);
  }
}