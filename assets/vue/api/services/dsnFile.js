import api from '../api';

export default {
  sendFiles(formData) {
    return api.post('/api/dsnfiles/uploadfiles', formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    ).then((response) => response.data);
  },
  dsnFiles() {
    return api.get('/api/dsnfiles')
      .then((response) => response.data);
  },
  refreshStatus(dsnFilesToRefresh) {
    return api.post('/api/dsnfiles/refresh', dsnFilesToRefresh)
      .then((response) => response.data);
  },
  deleteDsnFile(id, choice) {
    return api
      .delete('/api/dsnfiles/'+id+'?withData='+choice)
      .then((response) => response.data);
  }
}