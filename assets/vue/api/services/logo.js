import api from '../api';

export default {
  sendLogo(formData) {
    return api.post('/api/logo', formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    ).then((response) => response.data);
  },
  removeLogo() {
    return api.delete('/api/logo')
      .then((response) => response.data);
  }
}