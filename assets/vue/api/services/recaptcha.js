import api from '../api';

export default {
  validate(token) {
    return api
      .post('/recaptcha/validate',{'recaptcha':token})
      .then((response) => response.data);
  }
}