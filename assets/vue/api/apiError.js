import VanillaToasts from 'vanillatoasts';

export default {
  handle(error) {
    if(error.status===500) {
      VanillaToasts.create({
        title: 'Une erreur est survenue',
        text: 'Notre équipe technique est informée',
        type: 'error',
        timeout: 5000, // hide after 5000ms, // optional paremter
      });
    }else {
      VanillaToasts.create({
        title: error.data.title,
        text: error.data.message,
        type: (error.status >= 500) ? 'error' : (error.status >= 400) ? 'warning' : (error.status >= 200) ? 'success' : 'info',
        timeout: 5000, // hide after 5000ms, // optional paremter
      });
    }
  }
}
