import axios from 'axios'
import config from './api.conf'
import stores from '../stores/index'
import apiError from './apiError'
import VanillaToasts from 'vanillatoasts'
import router from './../router/router'

const api = axios.create(config)

// Add a request interceptor
api.interceptors.request.use((request) => {
  var csrf_token = stores.getters.csrfToken

  if (csrf_token !== null) {
    api.defaults.headers.common['anti-csrf-token'] = csrf_token
    
    request.withCredentials = true
  }
  
  return request
}, (error) => {
  // Do something with request error
  return Promise.reject(error)
})

// Add a response interceptor
api.interceptors.response.use((response) => {
  let commit = stores.commit;
  if(response.data.errors!==undefined) {
    response.data.errors.forEach(validationError => {
      VanillaToasts.create({
        title: "Validation du formulaire",
        text: validationError.message,
        type: 'info',
        timeout: 5000
      });
    });
    commit('setLoading',false)
    return Promise.reject(response)
  }

  return response
}, (error) => {
  let commit = stores.commit;
  if (error.response.status === 401) {
    if(error.response.data==="no session") {
      if( router.currentRoute.path !== '/connexion') {
        router.push({path: "/connexion"});
      }
      return Promise.reject(error);
    }else if(error.response.error!=="Invalid credentials.") {
      VanillaToasts.create({
        title: 'Identification',
        text: 'Nom d\'utilisateur et/ou mot de passe incorrect',
        type: 'error',
        timeout: 5000, // hide after 5000ms, // optional paremter
      });
      commit('setLoading',false)
      return Promise.reject(error);
    }
    commit('logout')
  }else if(error.response.status === 503) {
    router.push({path: "/maintenance"});
  }

  if(error.response.data.errors!==undefined) {
    error.response.data.errors.forEach(validationError => {
      VanillaToasts.create({
        title: "Validation du formulaire",
        text: validationError.message,
        type: 'info',
        timeout: 5000
      });
    });
    commit('setLoading',false)
    return Promise.reject(error);
  }

  if(error.response.config.url !== "/refresh" && error.response.status !== 503) {
    apiError.handle(error.response)
  }

  commit('setLoading',false)
  return Promise.reject(error);
})

export default api
