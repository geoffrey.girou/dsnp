<template>
  <AnonymousPage>
    <template #title>
      Mot de passe oublié
    </template>
    <template #content>
      <div
        class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3"
      >
        <form
          id="forgottenPasswordForm"
          novalidate="novalidate"
        >
          <div class="form-group">
            <label for="email">Email&nbsp;:</label>
            <input
              id="email"
              v-model="email"
              name="email"
              type="email"
              class="form-control"
              required="required"
            >
          </div>
          <div
            v-if="passwordCodeSent===true"
            class="form-group"
          >
            <label for="code">Code&nbsp;:</label>
            <input
              id="code"
              v-model="code"
              name="code"
              type="text"
              class="form-control"
              required="required"
            >
          </div>
          <div
            v-if="passwordCodeSent===true&&isCodeValid===true"
            class="form-group"
          >
            <label for="password">Nouveau mot de passe&nbsp;:</label>
            <div class="input-group">
              <input
                id="password"
                v-model="password"
                name="password"
                :type="passwordVisibility"
                class="form-control"
                autocomplete="off"
                required="required"
              >
              <span class="input-group-btn">
                <button
                  class="btn btn-default"
                  type="button"
                  @mousedown="showPassword"
                  @mouseup="hidePassword"
                ><span class="fa fa-eye" /></button>
              </span>
            </div>
          </div>
          <div
            v-if="passwordCodeSent===true&&isCodeValid===true"
            class="form-group"
          >
            <label for="passwordConfirm">Confirmation de mot de passe&nbsp;:</label>
            <div class="input-group">
              <input
                id="passwordConfirm"
                v-model="passwordConfirm"
                name="passwordConfirm"
                :type="passwordVisibility"
                class="form-control"
                autocomplete="off"
                required="required"
              >
              <span class="input-group-btn">
                <button
                  class="btn btn-default"
                  type="button"
                  @mousedown="showPassword"
                  @mouseup="hidePassword"
                ><span class="fa fa-eye" /></button>
              </span>
            </div>
          </div>
          <div class="recaptcha">
            <recaptcha @validate="afterRecaptchaValidation" />
          </div>
          <div
            class="form-group btn-list"
          >
            <input
              class="btn btn-primary"
              :disabled="isPasswordFormSubmitDisabled"
              type="submit"
              :value="passwordFormSubmitLabel"
              @click="processPassword()"
            >
          </div>
        </form>
        <p>
          <router-link :to="{ path: '/connexion'}">
            <a href="#">Retour</a>
          </router-link>
        </p>
      </div>
    </template>
  </AnonymousPage>
</template>

<script>
import { mapActions, mapGetters } from 'vuex'
import AnonymousPage from '../components/Page/AnonymousPage'
import VanillaToasts from 'vanillatoasts'
import Recaptcha from './../components/Recaptcha/Recaptcha.vue'

export default {
  name: "ForgotterPassword",
  components: {AnonymousPage,Recaptcha},
  data() {
    return {
      passwordHidden: true,
      email: '',
      validator: null,
      code: '',
      password: '',
      passwordConfirm: '',
      checkPasswordResetSuccess: false,
      isRecaptchaValidated: false
    };
  },
  computed: {
    ...mapGetters([
      'isLoading',
      'passwordCodeSent',
      'isCodeValid'
    ]),
    passwordVisibility() {
      return (this.passwordHidden) ? 'password' : 'text'
    },
    isPasswordFormSubmitDisabled () {
      if(this.passwordCodeSent===true&&this.isCodeValid===true) {
        return this.isLoading || this.code.length===0 || this.password.length===0
            || this.passwordConfirm.length===0 || this.password !== this.passwordConfirm
      } else if(this.passwordCodeSent===true) {
        return this.isLoading || this.code.length===0
      }else {
        return !this.isRecaptchaValidated
      }
    },
    passwordFormSubmitLabel() {
      if(this.passwordCodeSent===true&&this.isCodeValid===true) {
        return 'Valider mon changement'
      }else if(this.passwordCodeSent) {
        return 'Vérifier mon code'
      }else {
        return 'Modifier mon mot de passe'
      }
    }
  },
  mounted() {
    this.validator = $('#forgottenPasswordForm').submit(function(e) {
      e.preventDefault();
    }).validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        code: {
          required: true,
        },
        password: {
          complexPassword: true
        },
        passwordConfirm: {
          required: true,
          minlength: 12,
          equalTo: '#password'
        },
      },
      errorElement: "em",
      errorPlacement: function (error, element) {
        // Add the `help-block` class to the error element
        error.addClass("help-block");
        if(element[0]['name']=== "password" || element[0]['name']=== "passwordConfirm") {
          error.insertAfter(element[0]['parentElement'])
        }else if (element.prop("type") === "checkbox") {
          error.insertAfter(element.parent("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element) {
        $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
      },
      unhighlight: function (element) {
        $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
      }
    });
  },
  methods: {
    ...mapActions([
      'loadingStarts',
      'loadingEnds',
      'passwordCode',
      'checkCode',
      'changePassword'
    ]),
    processPassword() {
      if($("#forgottenPasswordForm").valid()) {
        this.loadingStarts()
        if (this.passwordCodeSent === true && this.isCodeValid === true) {
          this.change()
        } else if (this.passwordCodeSent) {
          this.check()
        } else {
          this.sendPasswordResetCode()
        }
      }
    },
    sendPasswordResetCode () {
      this.passwordCode(this.email)
        .then(
          () => {
            VanillaToasts.create({
              title: 'Mot de passe',
              text: 'Un code de vérification a été envoyé sur votre addresse email.',
              type: 'success',
              timeout: 10000, // hide after 5000ms, // optional paremter
            });
            this.loadingEnds();
          }
        )
    },
    check() {
      this.checkCode({ email: this.email, code: this.code }).then(
        () => {
          VanillaToasts.create({
            title: 'Mot de passe',
            text: 'Code valide. Vous pouvez modifier votre mot de passe.',
            type: 'success',
            timeout: 5000, // hide after 5000ms, // optional paremter
          });
          this.loadingEnds();
        }
      )
    },
    change() {
      this.changePassword({
        email: this.email,
        code: this.code,
        password: this.password
      }).then(
        () => {
          VanillaToasts.create({
            title: 'Mot de passe',
            text: 'Votre mot de passe a été modifié avec succès.',
            type: 'success',
            timeout: 5000, // hide after 5000ms, // optional paremter
          });
          this.$router.push({path: '/connexion/' + encodeURI(this.email)})
          this.loadingEnds();
        }
      )
    },
    hidePassword() {
      this.passwordHidden = true
    },
    showPassword() {
      this.passwordHidden = false
    },
    afterRecaptchaValidation(result) {
      this.isRecaptchaValidated = result
    }
  },
}
</script>

<style scoped>
  .recaptcha {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  /*.input-group {*/
  /*  z-index: -1 !important;*/
  /*}*/
</style>