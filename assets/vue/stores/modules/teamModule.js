import teamService from '../../api/services/team'

export default {
  state: {
    teams: [],
    team: {
      id: '',
      description: '',
      businesses: [],
      managers: [],
    }
  },
  getters: {
    teams(state) {
      return state.teams
    },
    team(state) {
      return state.team
    }
  },
  mutations: {
    resetTeams(state) {
      state.teams = null
    },
    updateTeam(state, teamAndIndex) {
      state.teams[teamAndIndex.index] = teamAndIndex.team
    },
    setTeams(state, teams) {
      state.teams = teams
    },
    setTeam(state, team) {
      state.team = team
    },
    removeTeam(state, id) {
      state.teams.splice(
        state.teams.findIndex(item => {
          return item.id === id
        }),
        1
      )
    },
    resetTeam(state) {
      state.teams = []
      state.team = {
        id: '',
        description: '',
        businesses: [],
        managers: [],
      }
    }
  },
  actions: {
    fetchTeams({commit}) {
      return teamService.teams()
        .then(teams => {
          commit('setTeams', teams)
        })
    },
    fetchTeam({commit},id) {
      return teamService.team(id)
        .then(team => {
          commit('setTeam', team)
          return team
        })
    },
    // eslint-disable-next-line no-unused-vars
    createTeam({commit},description) {
      return teamService.create(description)
        .then(team => {
          return team
        })
    },
    // eslint-disable-next-line no-unused-vars
    updateTeam({commit},team) {
      return teamService.update(team)
        .then(updatedTeam => {
          return updatedTeam
        })
    },
    // eslint-disable-next-line no-unused-vars
    addManager({commit},teamManager) {
      return teamService.addManager(teamManager.teamId, teamManager.managerId)
    },
    // eslint-disable-next-line no-unused-vars
    addBusiness({commit},teamBusiness) {
      return teamService.addBusiness(teamBusiness.teamId, teamBusiness.businessId)
    },
    // eslint-disable-next-line no-unused-vars
    removeManager({commit},teamManager) {
      return teamService.deleteManager(teamManager.teamId, teamManager.managerId)
    },
    // eslint-disable-next-line no-unused-vars
    removeBusiness({commit},teamBusiness) {
      return teamService.deleteBusiness(teamBusiness.teamId, teamBusiness.businessId)
    },
    deleteTeam({commit},id) {
      return teamService.delete(id)
        .then(() => {
          commit('removeTeam', id)
        })
    },
  }
}