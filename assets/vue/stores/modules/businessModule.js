import businessService from '../../api/services/business'

export default {
  state: {
    businesses: [],
    business: {
      id: '',
      email: '',
    }
  },
  getters: {
    businesses(state) {
      return state.businesses
    },
    business(state) {
      return state.business
    }
  },
  mutations: {
    resetBusinesses(state) {
      state.businesses = []
    },
    updateBusiness(state, business) {
      state.businesses.splice(
        state.businesses.findIndex(item => {
          return item.id === business.id
        }),
        1,
        business
      )
    },
    setBusinesses(state, businesses) {
      state.businesses = businesses
    },
    setBusiness(state, business) {
      state.business = business
    },
    resetBusiness(state) {
      state.businesses = []
      state.business = {
        id: '',
        email: '',
      }
    }
  },
  actions: {
    fetchBusinesses({commit}) {
      return businessService.businesses()
        .then(businesses => {
          commit('setBusinesses', businesses)
        })
    },
    createBusiness({commit}) {
      return businessService.create()
        .then(businessCreated => {
          commit('setBusiness', businessCreated)
          return businessCreated
        })
    },
    validBusinessPayment({commit}, business) {
      return businessService.valid(business.id)
        .then(business => {
          commit('updateBusiness', business)
          return business
        })
    },
    invalidBusinessPayment({commit}, business) {
      return businessService.invalid(business.id)
        .then(business => {
          commit('updateBusiness', business)
          return business
        })
    },
    lockBusiness({commit}, business) {
      return businessService.lock(business.id)
        .then(business => {
          commit('updateBusiness', business)
        })
    },
    unlockBusiness({commit}, business) {
      return businessService.unlock(business.id)
        .then(business => {
          commit('updateBusiness', business)
        })
    },
  }
}