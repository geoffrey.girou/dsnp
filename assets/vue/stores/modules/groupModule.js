import groupService from '../../api/services/group'

export default {
  state: {
    groups: [],
    group: {
      name: ''
    }
  },
  getters: {
    groups(state) {
      return state.groups
    },
    group(state) {
      return state.group
    }
  },
  mutations: {
    setGroups(state, groups) {
      state.groups = groups
    },
    setGroup(state, group) {
      state.group = group
    },
    removeGroup(state, id) {
      state.groups.splice(
        state.groups.findIndex(item => {
          return item.id === id
        }),
        1
      )
    },
    resetGroup(state) {
      state.groups = []
      state.group = {
        name: ''
      }
    },
    updateGroups(state, groupAndIndex) {
      state.groups[groupAndIndex.index] = groupAndIndex.group
    }
  },
  actions: {
    fetchGroups({commit}) {
      return groupService.all()
        .then(groups => {
          commit('setGroups', groups);
        })
    },
    fetchGroup({commit}, groupIdentifier) {
      return groupService.one(groupIdentifier)
        .then(group => {
          commit('setGroup', group);
        })
    },
    // eslint-disable-next-line no-unused-vars
    createGroup({commit}, group) {
      return groupService.create(group)
    },
    deleteGroup({commit}, id) {
      commit('setLoading', true)
      return groupService.delete(id)
        .then(() => {
          commit('removeGroup', id)
          commit('setLoading', false)
        })
    },
    setGroup({commit},group) {
      commit('setGroup', group)
    },
    // eslint-disable-next-line no-unused-vars
    updateGroupName({commit},group) {
      return groupService.update({
        id: group.id,
        name: group.name
      })
    },
    // eslint-disable-next-line no-unused-vars
    updateGroup({commit},group) {
      return groupService.update(group)
    }
  }
}