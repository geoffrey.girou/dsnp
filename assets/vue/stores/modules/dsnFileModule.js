import dsnFileService from '../../api/services/dsnFile'

export default {
  state: {
    hasError: false,
    dsnFiles: [],
    dsnFile: {
      id: '',
    },
    dsnFilesUpdated: false
  },
  getters: {
    hasError(state) {
      return state.hasError
    },
    dsnFilesUpdated(state) {
      return state.dsnFilesUpdated
    },
    dsnFiles(state) {
      return state.dsnFiles
    },
    dsnFile(state) {
      return state.dsnFile
    },
    dsnFilesToRefresh(state) {
      if( state.dsnFiles!==[] ) {
        return state.dsnFiles.filter((dsnFile) => {
          if(dsnFile.status === 'ok' || dsnFile.status.startsWith('Erreur de traitement') || dsnFile.status.startsWith('INFO')) {
            return false
          }else if (dsnFile.status === 'en_cours_de_transmission' || dsnFile.status === 'en_cours') {
            return true
          }
        })
      }
    },
    dsnFilesWithOkStatus(state) {
      if(state.dsnFiles===[]) {
        return false
      }else {
        let dsnFilesWithOkStatus = state.dsnFiles.filter((dsnFile) => {
          return (dsnFile.status === "ok")
        })
        return dsnFilesWithOkStatus.length >=1
      }
    }
  },
  mutations: {
    setHasError(state, bool) {
      state.hasError = bool
    },
    setDsnFiles(state, dsnFiles) {
      state.dsnFiles = dsnFiles
    },
    addDsnFiles(state, dsnFiles) {
      dsnFiles.forEach(dsnFile => {
        state.dsnFiles.unshift(dsnFile)
        if(dsnFile.status.startsWith('Erreur')) {
          state.hasError = true
        }
      })
    },
    resetDsnFile(state) {
      state.dsnFiles = []
      state.hasError = false
      state.dsnFilesUpdated = false
    },
    removeDsnFile(state, id) {
      state.dsnFiles.splice(
        state.dsnFiles.findIndex(item => {
          return item.id === id
        }),
        1
      )
    },
    setDsnFileStatus(state,dsnFileRefreshed) {
      const dsnFound = state.dsnFiles.find(dsnFile => {
        return dsnFile.id === dsnFileRefreshed.id && dsnFile.status !== dsnFileRefreshed.status;
      });

      if(dsnFound) {
        dsnFound.status = dsnFileRefreshed.status
        dsnFound.declarations = dsnFileRefreshed.declarations
        state.dsnFilesUpdated = true
      }
    },
    setDsnFilesUpdated(state,flag) {
      state.dsnFilesUpdated = flag
    }
  },
  actions: {
    sendFiles({commit},formData) {
      commit('setHasError',false)
      return dsnFileService.sendFiles(formData)
        .then((dsnFiles)=>{
          commit('addDsnFiles',dsnFiles)
        });
    },
    fetchDsnFiles({commit}) {
      return dsnFileService.dsnFiles()
        .then((dsnFiles)=>commit('setDsnFiles',dsnFiles));
    },
    refreshDsnFilesStatus({commit},dsnFilesToRefresh) {
      commit("setDsnFilesUpdated",false)
      return dsnFileService.refreshStatus(dsnFilesToRefresh)
        .then((dsnFilesRefreshed)=>{
          dsnFilesRefreshed.forEach(dsnFileRefreshed => {
            commit('setDsnFileStatus',dsnFileRefreshed)
          })
        })
    },
    deleteDsnFile({commit}, payload) {
      commit('setLoading', true)
      return dsnFileService.deleteDsnFile(payload.dsnFile.id, payload.choice)
        .then(() => {
          commit('removeDsnFile', payload.dsnFile.id)
          commit('setLoading', false)
        })
    }
  }
}