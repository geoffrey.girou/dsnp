import logoService from '../../api/services/logo'

export default {
  state: {
    logoHasError: false,
  },
  getters: {
    logoHasError(state) {
      return state.logoHasError
    },
  },
  mutations: {
    setLogoHasError(state, bool) {
      state.logoHasError = bool
    },
    resetLogo(state) {
      state.logoHasError = false
    }
  },
  actions: {
    sendLogo({commit},formData) {
      commit('setLogoHasError',false)
      return logoService.sendLogo(formData)
        .then((response)=>{
          if(response.errors != undefined) {
            commit('setLogoHasError',true)
          }else {
            commit('setLogo',response.logo)
          }
        });
    },
    removeLogo({commit}) {
      return logoService.removeLogo()
        .then(()=>{
          commit('resetLogo')
        })
    }
  }
}