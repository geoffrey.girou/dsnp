import wizardService from '../../api/services/wizard';

export default {
  state: {
    id:0,
    isFirstSirenAdded:false,
    isFirstDsnFileAddedAndStatusOk:false,
    isWelcomeOk:false,
  },
  getters: {
    isOn(state) {
      return (state.isFirstSirenAdded === false ||
          state.isFirstDsnFileAddedAndStatusOk === false ||
          state.isWelcomeOk === false)
    },
    wizard(state) {
      return state
    },
    isWelcomeOk(state) {
      return state.isWelcomeOk
    },
    isFirstSirenAdded(state) {
      return state.isFirstSirenAdded
    },
    isFirstDsnFileAddedAndStatusOk(state) {
      return state.isFirstDsnFileAddedAndStatusOk
    }
  },
  mutations: {
    setActive(state, bool) {
      state.active = bool
    },
    setWizard(state, wizard) {
      state.id = (wizard!=null) ? wizard.id : 0
      state.isFirstSirenAdded = (wizard!=null) ? wizard.isFirstSirenAdded : false
      state.isFirstDsnFileAddedAndStatusOk = (wizard!=null) ? wizard.isFirstDsnFileAddedAndStatusOk : false
      state.isWelcomeOk = (wizard!=null) ? wizard.isWelcomeOk : false
    },
    resetWizard(state) {
      state.id = 0
      state.isFirstSirenAdded = false
      state.isFirstDsnFileAddedAndStatusOk = false
      state.isWelcomeOk = false
    }
  },
  actions: {
    welcomeOk({commit}) {
      return wizardService.welcomeOk()
        .then(wizard => {
          commit('setWizard', wizard)
        })
    },
    firstSirenAdded({commit}) {
      return wizardService.firstSirenAdded()
        .then(wizard => {
          commit('setWizard', wizard)
        })
    },
    firstDsnFileAddedAndStatusOk({commit}) {
      return wizardService.firstDsnFileAddedAndStatusOk()
        .then(wizard => {
          commit('setWizard', wizard)
        })
    }
  }
}