import registrationService from '../../api/services/registration';

export default {
  actions: {
    // eslint-disable-next-line no-unused-vars
    register({commit},payload) {
      return registrationService.register(
        payload.email,
        payload.password,
        payload.companyName,
        payload.firstname,
        payload.lastname,
        payload.addressLine1,
        payload.addressLine2,
        payload.postalCode,
        payload.city,
        payload.phoneNumber
      ).then((response) => {
        return response;
      });
    },
    // eslint-disable-next-line no-unused-vars
    oauthRegister({commit},payload) {
      return registrationService.oauthRegister(
        payload.companyName,
        payload.firstname,
        payload.lastname,
        payload.addressLine1,
        payload.addressLine2,
        payload.postalCode,
        payload.city,
        payload.phoneNumber
      ).then((response) => {
        return response;
      });
    },
  }
}