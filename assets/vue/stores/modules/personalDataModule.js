import personalDataService from '../../api/services/personalData'

export default {
  state: {
    personalData: {
      userId: null,
      companyName: '',
      firstname: '',
      lastname: '',
      phoneNumber: '',
      addressLine1: '',
      addressLine2: '',
      postalCode: '',
      city: '',
    }
  },
  getters: {
    personalData(state) {
      return state.personalData
    },
  },
  mutations: {
    setPersonalData(state, personalData) {
      state.personalData = personalData
    },
    resetPersonalData(state) {
      state.personalData = {
        userId: null,
        companyName: '',
        firstname: '',
        lastname: '',
        phoneNumber: '',
        addressLine1: '',
        addressLine2: '',
        postalCode: '',
        city: '',
      }
    }
  },
  actions: {
    fetchPersonalData({commit},id) {
      return personalDataService.personalData(id)
        .then(personalData => {
          commit('setPersonalData', personalData)
          return personalData
        })
    },
    createPersonalData({commit},personalData) {
      return personalDataService.create(personalData)
        .then(personalDataCreated => {
          commit('setPersonalData', personalDataCreated)
          return personalDataCreated
        })
    },
    updatePersonalData({commit},personalData) {
      return personalDataService.update(personalData)
        .then(personalDataUpdated => {
          commit('setPersonalData', personalDataUpdated)
          return personalDataUpdated
        })
    },
  }
}