import securityService from '../../api/services/security';
import router from '../../router/router';

export default {
  state: {
    logged: false,
    csrfToken: null,
    isImpersonating: false,
  },
  getters: {
    logged(state) {
      return state.logged
    },
    csrfToken(state) {
      return state.csrfToken
    },
    isImpersonating(state) {
      return state.isImpersonating;
    }
  },
  mutations: {
    logged(state) {
      state.logged = true
    },
    setToken(state, token) {
      state.csrfToken = token
    },
    setIsImpersonating(state, isImpersonating) {
      state.isImpersonating = isImpersonating
    },
    logout(state) {
      state.logged = false
    },
    refresh(state, payload) {
      state.logged = payload.logged
    },
    resetSecurity(state) {
      state.logged = false
      state.csrfToken = null
      state.isImpersonating = false
    }
  },
  actions: {
    login({commit}, payload) {
      return securityService.login(payload.email, payload.password)
        .then((response) => {
          commit('logged')
          commit('setUser', response)
          commit('setWizard', response.wizard)
          commit('setLogo', response.logo)
          // no neeed to return response
          return response;
        })
        .catch((error) => {
          return Promise.reject(error.response.data)
        });
    },
    connect({commit},service) {
      return securityService.connect(service)
        .then((response) => {
          commit('logged')
          return response;
        })
        .catch((error) => {
          return Promise.reject(error.response.data)
        });
    },
    logout({commit}) {
      return securityService.logout().then((response) => {
        commit('resetBusiness')
        commit('resetDsnArchive')
        commit('resetDsnFile')
        commit('resetGroup')
        commit('resetLogo')
        commit('resetManager')
        commit('resetPassword')
        commit('resetPersonalData')
        commit('resetSecurity')
        commit('resetSiren')
        commit('resetSiret')
        commit('resetTeam')
        commit('resetUser')
        commit('resetWizard')
        return response;
      });
    },
    onRefresh({commit}) {
      return securityService.refresh().then((response) => {
        if(response.data.logged!=null) {
          commit('refresh', response.data)
        }
        if(response.data.user!=null) {
          commit('setIsImpersonating', response.data.isImpersonating)
          commit('setUser', response.data.user)
          commit('setWizard', response.data.user.wizard)
          commit('setToken',response.data.token)
          commit('setLogo', response.data.user.logo)
        }
      });
    },
    setToken({commit}, token) {
      commit('setToken', token);
    },
    // eslint-disable-next-line no-unused-vars
    redirectAfterLogin({commit, rootState, rootGetters}, redirect) {
      if (redirect !== undefined) {
        router.push({path: redirect});
      } else {
        if(rootGetters.isAdmin) {
          router.push({path: "/admin/accueil"})
        }else if(rootGetters.isManager) {
          router.push({path: "/gestionnaire/societes"})
        } else {
          if(rootGetters.isOn===false) {
            if(rootState.user.user.canAccessData === true) {
              router.push({path: "/tableau-de-bord"})
            }else if (rootState.user.user.canUploadDsnFile === true) {
              router.push({path: "/fichiers-dsn"})
            }else if(rootState.user.user.canCreateUser) {
              router.push({path: "/utilisateurs"})
            }
          }else {
            if(rootState.wizard.isFirstSirenAdded === false) {
              router.push({path: "/sirens"})
            }else if(rootState.wizard.isFirstDsnFileAddedAndStatusOk === false) {
              router.push({path: "/fichiers-dsn"})
            }
          }
        }
      }
    },
    impersonate({commit}, email) {
      commit('setLoading', true)
      return securityService.impersonate(email)
        .then((response) => {
          commit('setIsImpersonating', response)
          return response;
        }).then(() => {
          return this.dispatch('onRefresh')
            .then(() => {
              commit('setLoading', false)
              return this.dispatch('redirectAfterLogin',undefined)
            })
        })
    },
    exitImpersonate({commit}) {
      commit('setLoading', true)
      return securityService.exitImpersonate()
        .then((response) => {
          commit('setIsImpersonating', response)
          return response;
        }).then(() => {
          return this.dispatch('onRefresh')
            .then(() => {
              commit('setLoading', false)
              return this.dispatch('redirectAfterLogin',undefined)
            })
        })
    },
  }
}