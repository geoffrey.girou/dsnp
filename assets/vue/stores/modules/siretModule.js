import siretService from '../../api/services/siret'

export default {
  state: {
    sirets: null
  },
  getters: {
    sirets(state) {
      return state.sirets
    },
  },
  mutations: {
    setSirets(state, sirets) {
      state.sirets = sirets
    },
    resetSiret(state) {
      state.sirets = null
    },
  },
  actions: {
    fetchSirets({commit}) {
      return siretService.all()
        .then(sirets => {
          commit('setSirets', sirets);
        })
    }
  }
}