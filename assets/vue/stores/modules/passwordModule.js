import passwordService from '../../api/services/password'

export default {
  state: {
    code: '',
    isCodeValid: false,
    password: '',
    passwordConfirmation: '',
    passwordCodeSent: false
  },
  getters: {
    code(state) {
      return state.code
    },
    isCodeValid(state) {
      return state.isCodeValid
    },
    password(state) {
      return state.password
    },
    passwordConfirmation(state) {
      return state.passwordConfirmation
    },
    passwordCodeSent(state) {
      return state.passwordCodeSent
    }
  },
  mutations: {
    setCode(state, code) {
      state.code = code
    },
    setIsCodeValid(state, isCodeValid) {
      state.isCodeValid = isCodeValid
    },
    setPassword(state, password) {
      state.password = password
    },
    setPasswordConfirmation(state, passwordConfirmation) {
      state.passwordConfirmation = passwordConfirmation
    },
    setPasswordCodeSent(state, passwordCodeSent) {
      state.passwordCodeSent = passwordCodeSent
    },
    resetPassword(state) {
      state.code= ''
      state.isCodeValid = false
      state.password= ''
      state.passwordConfirmation= ''
      state.passwordCodeSent= false
    }
  },
  actions: {
    passwordCode({commit}, email) {
      return passwordService.passwordCode(email)
        .then(() => commit('setPasswordCodeSent',true))
    },
    checkCode({commit}, emailAndCode) {
      return passwordService.checkCode(emailAndCode)
        .then(() => commit('setIsCodeValid',true))
    },
    changePassword({commit}, emailCodeAndPassword) {
      return passwordService.changePassword(emailCodeAndPassword)
        .then(() => commit('resetPassword'))
    }
  }
}
