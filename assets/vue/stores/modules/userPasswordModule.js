import userPasswordService from '../../api/services/userPassword'

export default {
  actions: {
    passwordUserCode({commit}) {
      return userPasswordService.passwordCode()
        .then(() => commit('setPasswordCodeSent',true))
    },
    checkUserCode({commit}, code) {
      return userPasswordService.checkCode(code)
        .then(() => commit('setIsCodeValid',true))
    },
    changeUserPassword({commit}, codeAndPassword) {
      return userPasswordService.changePassword(codeAndPassword)
        .then(() => commit('resetPassword'))
    }
  }
}
