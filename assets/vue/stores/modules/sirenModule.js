import sirenService from '../../api/services/siren'

export default {
  state: {
    sirens: [],
    siren: {
      id: '',
      number: '',
    },
    countDsnFileToDelete: 0,
    groupToDeleteMessages: []
  },
  getters: {
    siren(state) {
      return state.siren
    },
    sirens(state) {
      return state.sirens
    },
    countDsnFileToDelete(state) {
      return state.countDsnFileToDelete
    },
    groupToDeleteMessages(state) {
      return state.groupToDeleteMessages
    }
  },
  mutations: {
    setSirens(state, sirens) {
      state.sirens = sirens
    },
    setSiren(state, siren) {
      state.siren = siren
    },
    addSiren(state, siren) {
      state.sirens.push(siren)
    },
    removeSiren(state, id) {
      state.sirens.splice(
        state.sirens.findIndex(item => {
          return item.id === id
        }),
        1
      )
    },
    updateSiren(state, siren) {
      state.sirens.splice(
        state.sirens.findIndex(item => {
          return item.id === siren.id
        }),
        1,
        siren
      )
    },
    setSirenToDelete(state, details) {
      state.countDsnFileToDelete = details.count
      state.groupToDeleteMessages = details.messages
    },
    resetSiren(state) {
      state.countDsnFileToDelete = 0
      state.groupToDeleteMessages = []
      state.sirens = []
    }
  },
  actions: {
    fetchSirens({commit}) {
      return sirenService.all()
        .then(sirenList => {
          commit('setSirens', sirenList.data);
        })
    },
    fetchSirensIntegrated({commit}) {
      return sirenService.allIntegrated()
        .then(result => {
          commit('setSirens', result.data);
        })
    },
    createSiren({commit}, siren) {
      return sirenService.create(siren)
        .then(siren => commit('addSiren', siren))
    },
    updateSiren({commit},siren) {
      commit('setLoading', true)
      return sirenService.update(siren)
        .then((siren)=> {
          commit('updateSiren', siren)
          commit('setLoading', false)
        });
    },
    deleteSiren({commit}, id) {
      commit('setLoading', true)
      return sirenService.delete(id)
        .then(() => {
          commit('removeSiren', id)
          commit('setLoading', false)
        })
    },
    sirenToDelete({commit}, siren) {
      commit('setLoading', true)
      commit('setSiren',siren)
      return sirenService.toDelete(siren.id)
        .then((result) => {
          commit('setSirenToDelete',result)
          commit('setLoading', false)
        })
    }
  }
}