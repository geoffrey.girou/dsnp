import managerService from '../../api/services/manager'

export default {
  state: {
    managers: [],
    manager: {
      id: '',
      email: '',
      teams: []
    }
  },
  getters: {
    managers(state) {
      return state.managers
    },
    manager(state) {
      return state.manager
    }
  },
  mutations: {
    resetManager(state) {
      state.managers = []
      state.manager = {
        id: '',
        email: '',
        teams: []
      }
    },
    updateManagers(state, manager) {
      state.managers.splice(
        state.managers.findIndex(item => {
          return item.id === manager.id
        }),
        1,
        manager
      )
    },
    setManagers(state, managers) {
      state.managers = managers
    },
    setManager(state, manager) {
      state.manager = manager
    },
    addManager(state, manager) {
      state.managers.splice(
        0,
        0,
        manager
      )
    },
    removeManager(state, id) {
      state.managers.splice(
        state.managers.findIndex(item => {
          return item.id === id
        }),
        1
      )
    }
  },
  actions: {
    fetchManagers({commit}) {
      return managerService.managers()
        .then(managers => {
          commit('setManagers', managers)
          return managers
        })
    },
    fetchManager({commit},id) {
      return managerService.manager(id)
        .then(manager => {
          commit('setManager', manager)
          return manager
        })
    },
    // eslint-disable-next-line no-unused-vars
    createManager({commit},email) {
      return managerService.create(email)
        .then(manager => {
          return manager
        })
    },
    // eslint-disable-next-line no-unused-vars
    updateManager({commit},manager) {
      return managerService.update(manager)
        .then(updatedManager => {
          return updatedManager
        })
    },
    deleteManager({commit},id) {
      return managerService.delete(id)
        .then(() => {
          commit('removeManager', id)
        })
    },
  }
}