import userService from '../../api/services/user';

export default {
  state: {
    user: null,
    logo: null,
    children: [],
    profile: {
      companyName: '',
      firstname: '',
      lastname: '',
      email: '',
      phoneNumber: '',
      addressLine1: '',
      addressLine2: '',
      postalCode: '',
      city: '',
    },
    passwordResetLinkSent: false,
    checkPasswordResetSuccess: false,
    passwordChangedSuccess: false
  },
  getters: {
    isAdmin(state) {
      return (state.user!=null) ? ((state.user.roles!=null) ? state.user.roles[0]==='ROLE_ADMIN' : false): false
    },
    isBusiness(state) {
      return (state.user!=null) ? ((state.user.roles!=null) ? state.user.roles[0]==='ROLE_BUSINESS' : false): false
    },
    isManager(state) {
      return (state.user!=null) ? ((state.user.roles!=null) ? state.user.roles[0]==='ROLE_MANAGER' : false): false
    },
    user(state) {
      return state.user
    },
    logo(state) {
      return state.logo
    },
    children(state) {
      return state.children
    },
    profile(state) {
      return state.profile;
    },
    passwordResetLinkSent(state) {
      return state.passwordResetLinkSent;
    },
    checkPasswordResetSuccess(state) {
      return state.checkPasswordResetSuccess;
    },
    passwordChangedSuccess(state) {
      return state.passwordChangedSuccess;
    }
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },
    resetUser(state) {
      state.user = null
      state.children= []
      state.profile = {
        companyName: '',
        firstname: '',
        lastname: '',
        email: '',
        phoneNumber: '',
        addressLine1: '',
        addressLine2: '',
        postalCode: '',
        city: '',
      }
      state.passwordResetLinkSent = false
      state.checkPasswordResetSuccess = false
      state.passwordChangedSuccess = false
    },
    setProfile(state, profile) {
      state.profile = profile
    },
    setChildren(state, children) {
      state.children = children
    },
    setLogo(state, logo) {
      state.logo = logo
    },
    setChild(state, child) {
      state.children.push(child)
    },
    setUpdatedChild(state, payload) {
      state.children[payload.index] = payload.child
    },
    removeChild(state, index) {
      state.children.splice(index,1)
    },
    passwordResetLinkSent(state) {
      state.passwordResetLinkSent = true
    },
    checkPasswordResetSuccess(state) {
      state.checkPasswordResetSuccess = true
    },
    passwordChangedSuccess(state) {
      state.passwordChangedSuccess = true
    },
    resetLogo(state) {
      state.logo = null
    },
  },
  actions: {
    fetchProfile({commit}) {
      userService.profile()
        .then(profile => {
          commit('setProfile', profile)
        })
    },
    // eslint-disable-next-line no-unused-vars
    updateProfile({commit}, profile) {
      return userService.updateProfile(profile)
    },
    fetchChildren({commit}) {
      return userService.fetchChildren()
        .then((children)=>commit('setChildren', children))
    },
    // eslint-disable-next-line no-unused-vars
    createChild({commit},child) {
      return userService.createChild(child)
    },
    // eslint-disable-next-line no-unused-vars
    updateChild({commit},child) {
      return userService.updateChild(child)
    },
    deleteChild({commit}, payload) {
      commit('setLoading', true)
      return userService.deleteChild(payload.child.id)
        .then(() => {
          commit('removeChild', payload.index)
          commit('setLoading', false)
        })
    },
    // eslint-disable-next-line no-unused-vars
    fetchChild({commit}, id) {
      return userService.fetchChild(id)
    },
  },
}