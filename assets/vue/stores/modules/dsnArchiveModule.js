import dsnArchiveService from '../../api/services/dsnArchive'

export default {
  state: {
    archiveHasError: false,
  },
  getters: {
    archiveHasError(state) {
      return state.archiveHasError
    },
  },
  mutations: {
    setArchiveHasError(state, bool) {
      state.archiveHasError = bool
    },
    resetDsnArchive(state) {
      state.archiveHasError = false
    }
  },
  actions: {
    sendArchive({commit},formData) {
      commit('setArchiveHasError',false)
      return dsnArchiveService.sendArchive(formData)
        .then((archive)=>{
          if(archive.errors != undefined) {
            commit('setArchiveHasError',true)
          }
        });
    }
  }
}