import dsnDashboardService from '../../api/services/dsnDashboard'

export default {
  actions: {
    // eslint-disable-next-line no-unused-vars
    accessDashboard({commit}) {
      return dsnDashboardService.access()
    }
  }
}