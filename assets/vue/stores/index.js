import Vue from 'vue'
import Vuex from 'vuex'
import business from './modules/businessModule'
import dsnArchive from './modules/dsnArchiveModule'
import dsnDashboard from './modules/dsnDashboard'
import dsnFile from './modules/dsnFileModule'
import group from './modules/groupModule'
import logo from './modules/logoModule'
import manager from './modules/managerModule'
import password from './modules/passwordModule'
import personalData from './modules/personalDataModule'
import registration from './modules/registrationModule'
import security from './modules/securityModule'
import siren from './modules/sirenModule'
import siret from './modules/siretModule'
import team from './modules/teamModule'
import user from './modules/userModule'
import userPassword from './modules/userPasswordModule'
import wizard from './modules/wizardModule'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoading: false
  },
  getters: {
    isLoading(state) {
      return state.isLoading
    }
  },
  mutations: {
    setLoading(state, boolean) {
      state.isLoading = boolean;
    }
  },
  actions: {
    loadingStarts({commit}) {
      commit('setLoading', true)
    },
    loadingEnds({commit}) {
      commit('setLoading', false)
    }
  },
  modules: {
    business,
    dsnArchive,
    dsnDashboard,
    dsnFile,
    group,
    logo,
    manager,
    password,
    personalData,
    registration,
    security,
    siren,
    siret,
    team,
    user,
    userPassword,
    wizard,
  }
});