import Vue from 'vue'
import store from './stores'
import Router from './router/router'
import App from './App'
import api from './api/api'
import moment from 'moment'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/fr'

new Vue({
  el: '#app',
  store,
  api,
  router: Router,
  components: {
    App
  },
  template: '<App/>'
})

Vue.use(ElementUI, { locale })

Vue.filter('beautifulDate', function (value) {
  return  moment(value).format('DD/MM/YYYY');
});

Vue.filter('beautifulDateWithHour', function (value) {
  return  moment(value).format('DD/MM/YYYY HH:mm');
});

Vue.filter('dsnFileStatusLabelFromCode', function (value) {
  if(value === "ok") {
    return "TRAITEMENT TERMINÉ"
  }else if(value === "en_cours") {
    return "EN COURS DE TRAITEMENT"
  }else {
    return value
  }
})

Vue.filter('dsnRoleLabelFromCode', function (value) {
  if(value === "admindsnp") {
    return "Administrateur"
  }else if(value === "grouperh") {
    return "Responsable des ressources humaines"
  }else if(value === "partenaire") {
    return "Membre du comité sociale de l’entreprise"
  }else {
    return value
  }
})

Vue.filter('yesNoText', function (value) {
  return (value == true) ? 'Oui' : 'Non'
})

Vue.filter('padWithZero', function (value) {
  return String(value).padStart(4, '0')
})
