<?php

exec("mysqldump --host=[dbhost].mysql.db --user=[dbuser] --port=[dbport] --password=[dbpassword] --no-tablespaces [dbname] > /home/[sshuser]/db_backup/backup_day_$(date +'%m_%d_%Y-%H').sql");
exec("find /home/[sshuser]/db_backup -type f -name 'backup_day_*' -mtime +7 -exec rm {} \;");