<?php

exec("mysqldump --host=[dbhost].mysql.db --user=[dbuser] --port=[dbport] --password=[dbpassword] --no-tablespaces [dbname] > /home/[sshuser]/db_backup/backup_week_$(date +'%m_%d_%Y-%H').sql");
exec("find /home/[sshuser]/db_backup -type f -name 'backup_week_*' -mtime +30 -exec rm {} \;");