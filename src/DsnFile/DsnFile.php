<?php

namespace App\DsnFile;

use App\Enum\DsnFileEnum;
use Symfony\Component\HttpFoundation\File\File;

class DsnFile
{
    private bool $isExtensionValid;
    private bool $isMimeTypeValid;
    private string $name;
    private string $folder;
    private File $file;

    public function __construct()
    {
    }

    private function validate()
    {
        $extension = pathinfo($this->file->getFilename(), \PATHINFO_EXTENSION);
        if(!in_array($extension,DsnFileEnum::EXTENSIONS)) {
            $this->isExtensionValid = false;
        }else {
            $this->isExtensionValid = true;
        }

        $mimeType = $this->file->getMimeType();
        if(!in_array($mimeType,DsnFileEnum::MIMETYPES)) {
            $this->isMimeTypeValid = false;
        }else {
            $this->isMimeTypeValid = true;
        }
    }

    public function createFromPath($path)
    {
        $this->file = new File($path,true);
        $this->validate();
    }

    public function isValid()
    {
        return $this->isExtensionValid && $this->isMimeTypeValid;
    }
}