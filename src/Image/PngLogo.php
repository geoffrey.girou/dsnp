<?php

namespace App\Image;

use App\Enum\PngLogoEnum;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PngLogo
{
    private UploadedFile $uploadedFile;
    private bool $isExtensionValid;
    private bool $isMimeTypeValid;
    private string $extractFolder;
    private string $name;

    public function __construct()
    {
    }

    public function createFromUploadedFile(UploadedFile $uploadedFile)
    {
        $extension = $uploadedFile->getClientOriginalExtension();
        if (!in_array($extension, PngLogoEnum::EXTENSIONS)) {
            $this->isExtensionValid = false;
        } else {
            $this->isExtensionValid = true;
        }

        $mimeType = $uploadedFile->getMimeType();
        if (!in_array($mimeType, PngLogoEnum::MIMETYPES)) {
            $this->isMimeTypeValid = false;
        } else {
            $this->isMimeTypeValid = true;
        }
        $this->uploadedFile = $uploadedFile;
    }

    public function createFromFile($folder, $name)
    {
        $this->extractFolder = $folder;
        $this->name = $name;
    }

    public function move($folder,$name)
    {
        $this->extractFolder = $folder;
        $this->name = $name;
        $this->uploadedFile->move($folder,$name);
    }

    public function isValid()
    {
        return $this->isExtensionValid && $this->isMimeTypeValid;
    }

    public function b64()
    {
        return base64_encode(file_get_contents($this->uploadedFile->getPathname()));
    }
}