<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\OneToMany(targetEntity=Siren::class, mappedBy="user", cascade={"persist"})
     */
    private $sirens;

    /**
     * @ORM\OneToMany(targetEntity=DsnFile::class, mappedBy="user", cascade={"persist"})
     */
    private $dsnFiles;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isResetPassword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $resetPasswordAskedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $passwordUpdatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $resetPasswordCodeSentAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resetPasswordCode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $reportedAt;

    /**
     * @ORM\OneToOne(targetEntity=PersonalData::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $personalData;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $mailConfirmationSentAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $paymentReportedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFrozen;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity=DsnpData::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $dsnpData;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="children")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\Column(type="boolean")
     */
    private $canCreateUser;

    /**
     * @ORM\Column(type="boolean")
     */
    private $canAccessData;

    /**
     * @ORM\Column(type="boolean")
     */
    private $canUploadDsnFile;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $dsnpRoles = [];

    /**
     * @ORM\OneToMany(targetEntity=UserAction::class, mappedBy="user")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $userActions;

    /**
     * @ORM\OneToMany(targetEntity=SirenAccess::class, mappedBy="user", cascade={"persist"})
     */
    private $sirenAccesses;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activationCode;

    /**
     * @ORM\OneToOne(targetEntity=Wizard::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $wizard;

    /**
     * @ORM\OneToMany(targetEntity=GroupAccess::class, mappedBy="user", cascade={"persist"})
     */
    private $groupAccesses;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $githubId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @ORM\ManyToMany(targetEntity=Team::class, mappedBy="businesses")
     * @ORM\JoinTable(name="businessteam_user",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="businessteam_id", referencedColumnName="id")}
     *      )
     */
    private $businessTeams;

    /**
     * @ORM\ManyToMany(targetEntity=Team::class, mappedBy="managers")
     * @ORM\JoinTable(name="managerteam_user",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="managerteam_id", referencedColumnName="id")}
     *      )
     */
    private $managerTeams;

    public function __construct()
    {
        $this->sirens = new ArrayCollection();
        $this->dsnFiles = new ArrayCollection();
        $this->userActions = new ArrayCollection();
        $this->sirenAccesses = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->groupAccesses = new ArrayCollection();
        $this->businessTeams = new ArrayCollection();
        $this->managerTeams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection|Siren[]
     */
    public function getSirens(): Collection
    {
        return $this->sirens;
    }

    /**
     * @return Collection|DsnFile[]
     */
    public function getDsnFiles(): Collection
    {
        return $this->dsnFiles;
    }

    public function addSiren(Siren $siren): self
    {
        if (!$this->sirens->contains($siren)) {
            $this->sirens[] = $siren;
            $siren->setUser($this);
        }

        return $this;
    }

    public function addDsnFiles(DsnFile $dsnFile): self
    {
        if (!$this->dsnFiles->contains($dsnFile)) {
            $this->dsnFiles[] = $dsnFile;
            $dsnFile->setUser($this);
        }

        return $this;
    }

    public function removeSiren(Siren $siren): self
    {
        if ($this->sirens->removeElement($siren)) {
            // set the owning side to null (unless already changed)
            if ($siren->getUser() === $this) {
                $siren->setUser(null);
            }
        }

        return $this;
    }

    public function removeDsnFiles(DsnFile $dsnFile): self
    {
        if ($this->dsnFiles->removeElement($dsnFile)) {
            // set the owning side to null (unless already changed)
            if ($dsnFile->getUser() === $this) {
                $dsnFile->setUser(null);
            }
        }

        return $this;
    }

    public function getIsResetPassword(): ?bool
    {
        return $this->isResetPassword;
    }

    public function setIsResetPassword(?bool $isResetPassword): self
    {
        $this->isResetPassword = $isResetPassword;

        return $this;
    }

    public function getResetPasswordAskedAt(): ?\DateTimeInterface
    {
        return $this->resetPasswordAskedAt;
    }

    public function setResetPasswordAskedAt(?\DateTimeInterface $resetPasswordAskedAt): self
    {
        $this->resetPasswordAskedAt = $resetPasswordAskedAt;

        return $this;
    }

    public function getPasswordUpdatedAt(): ?\DateTimeInterface
    {
        return $this->passwordUpdatedAt;
    }

    public function setPasswordUpdatedAt(?\DateTimeInterface $passwordUpdatedAt): self
    {
        $this->passwordUpdatedAt = $passwordUpdatedAt;

        return $this;
    }

    public function getResetPasswordCodeSentAt(): ?\DateTimeInterface
    {
        return $this->resetPasswordCodeSentAt;
    }

    public function setResetPasswordCodeSentAt(?\DateTimeInterface $resetPasswordCodeSentAt): self
    {
        $this->resetPasswordCodeSentAt = $resetPasswordCodeSentAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getResetPasswordCode(): ?string
    {
        return $this->resetPasswordCode;
    }

    public function setResetPasswordCode(?string $resetPasswordCode): self
    {
        $this->resetPasswordCode = $resetPasswordCode;

        return $this;
    }

    public function getReportedAt(): ?\DateTimeInterface
    {
        return $this->reportedAt;
    }

    public function setReportedAt(?\DateTimeInterface $reportedAt): self
    {
        $this->reportedAt = $reportedAt;

        return $this;
    }

    public function getPersonalData(): ?PersonalData
    {
        return $this->personalData;
    }

    public function setPersonalData(?PersonalData $personalData): self
    {
        // unset the owning side of the relation if necessary
        if ($personalData === null && $this->personalData !== null) {
            $this->personalData->setUser(null);
        }

        // set the owning side of the relation if necessary
        if ($personalData !== null && $personalData->getUser() !== $this) {
            $personalData->setUser($this);
        }

        $this->personalData = $personalData;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getMailConfirmationSentAt(): ?\DateTimeInterface
    {
        return $this->mailConfirmationSentAt;
    }

    public function setMailConfirmationSentAt(?\DateTimeInterface $mailConfirmationSentAt): self
    {
        $this->mailConfirmationSentAt = $mailConfirmationSentAt;

        return $this;
    }

    public function getPaymentReportedAt(): ?\DateTimeInterface
    {
        return $this->paymentReportedAt;
    }

    public function setPaymentReportedAt(?\DateTimeInterface $paymentReportedAt): self
    {
        $this->paymentReportedAt = $paymentReportedAt;

        return $this;
    }

    public function getIsFrozen(): ?bool
    {
        return $this->isFrozen;
    }

    public function setIsFrozen(bool $isFrozen): self
    {
        $this->isFrozen = $isFrozen;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDsnpData(): ?DsnpData
    {
        return $this->dsnpData;
    }

    public function setDsnpData(DsnpData $dsnpData): self
    {
        // set the owning side of the relation if necessary
        if ($dsnpData->getUser() !== $this) {
            $dsnpData->setUser($this);
        }

        $this->dsnpData = $dsnpData;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function addParent(self $parent): self
    {
        if (!$this->parent->contains($parent)) {
            $this->parent[] = $parent;
            $parent->setParent($this);
        }

        return $this;
    }

    public function removeParent(self $parent): self
    {
        if ($this->parent->removeElement($parent)) {
            // set the owning side to null (unless already changed)
            if ($parent->getParent() === $this) {
                $parent->setParent(null);
            }
        }

        return $this;
    }

    public function getCanCreateUser(): ?bool
    {
        return $this->canCreateUser;
    }

    public function setCanCreateUser(bool $canCreateUser): self
    {
        $this->canCreateUser = $canCreateUser;

        return $this;
    }

    public function getCanAccessData(): ?bool
    {
        return $this->canAccessData;
    }

    public function setCanAccessData(bool $canAccessData): self
    {
        $this->canAccessData = $canAccessData;

        return $this;
    }

    public function getCanUploadDsnFile(): ?bool
    {
        return $this->canUploadDsnFile;
    }

    public function setCanUploadDsnFile(bool $canUploadDsnFile): self
    {
        $this->canUploadDsnFile = $canUploadDsnFile;

        return $this;
    }

    public function getDsnpRoles(): ?array
    {
        return $this->dsnpRoles;
    }

    public function setDsnpRoles(?array $dsnpRoles): self
    {
        $this->dsnpRoles = $dsnpRoles;

        return $this;
    }

    public function hasRole(string $role): bool
    {
        foreach ($this->roles as $currentUserRole) {
            if($currentUserRole===$role) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Collection|UserAction[]
     */
    public function getUserActions(): Collection
    {
        return $this->userActions;
    }

    public function addUserAction(UserAction $userAction): self
    {
        if (!$this->userActions->contains($userAction)) {
            $this->userActions[] = $userAction;
            $userAction->setUser($this);
        }

        return $this;
    }

    public function removeUserAction(UserAction $userAction): self
    {
        if ($this->userActions->removeElement($userAction)) {
            // set the owning side to null (unless already changed)
            if ($userAction->getUser() === $this) {
                $userAction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SirenAccess[]
     */
    public function getSirenAccesses(): Collection
    {
        return $this->sirenAccesses;
    }

    public function addSirenAccess(SirenAccess $sirenAccess): self
    {
        if (!$this->sirenAccesses->contains($sirenAccess)) {
            $this->sirenAccesses[] = $sirenAccess;
            $sirenAccess->setUser($this);
        }

        return $this;
    }

    public function removeSirenAccess(SirenAccess $sirenAccess): self
    {
        if ($this->sirenAccesses->removeElement($sirenAccess)) {
            // set the owning side to null (unless already changed)
            if ($sirenAccess->getUser() === $this) {
                $sirenAccess->setUser(null);
            }
        }

        return $this;
    }

    public function getActivationCode(): ?string
    {
        return $this->activationCode;
    }

    public function setActivationCode(?string $activationCode): self
    {
        $this->activationCode = $activationCode;

        return $this;
    }

    public function getWizard(): ?Wizard
    {
        return $this->wizard;
    }

    public function setWizard(Wizard $wizard): self
    {
        // set the owning side of the relation if necessary
        if ($wizard->getUser() !== $this) {
            $wizard->setUser($this);
        }

        $this->wizard = $wizard;

        return $this;
    }

    /*
     * CUSTOM MODEL ADDED BEHAVIOUR
     */

    /**
     * @return bool
     */
    public function isAChild(): bool
    {
        return $this->getParent()!==null;
    }

    /**
     * @return bool
     */
    public function isNotAChild(): bool
    {
        return !$this->isAChild();
    }

    /**
     * @return $this
     */
    public function getFinalParent(): User
    {
        if($this->getParent() == null) {
            return $this;
        }else {
            return $this->getParent()->getFinalParent();
        }
    }

    /**
     * @return Collection|Siren[]
     */
    public function sirensFromSirenAccesses()
    {
        $sirens = [];
        foreach ($this->getSirenAccesses() as $sirenAccess) {
            array_push($sirens, $sirenAccess->getSiren());
        }

        return $sirens;
    }

    /**
     * @return Siren[]|Collection
     */
    public function getAvailableSiren()
    {
        return ($this->isNotAChild() && $this->hasRole('ROLE_BUSINESS')) ? $this->getSirens() : $this->sirensFromSirenAccesses();
    }

    /**
     * @return Collection|GroupAccess[]
     */
    public function getGroupAccesses(): Collection
    {
        return $this->groupAccesses;
    }

    public function addGroupAccess(GroupAccess $groupAccess): self
    {
        if (!$this->groupAccesses->contains($groupAccess)) {
            $this->groupAccesses[] = $groupAccess;
            $groupAccess->setUser($this);
        }

        return $this;
    }

    public function removeGroupAccess(GroupAccess $groupAccess): self
    {
        if ($this->groupAccesses->removeElement($groupAccess)) {
            // set the owning side to null (unless already changed)
            if ($groupAccess->getUser() === $this) {
                $groupAccess->setUser(null);
            }
        }

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getGithubId(): ?string
    {
        return $this->githubId;
    }

    public function setGithubId(?string $githubId): self
    {
        $this->githubId = $githubId;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getBusinessTeams(): Collection
    {
        return $this->businessTeams;
    }

    public function addBusinessTeam(Team $businessTeam): self
    {
        if (!$this->businessTeams->contains($businessTeam)) {
            $this->businessTeams[] = $businessTeam;
            $businessTeam->addBusiness($this);
        }

        return $this;
    }

    public function removeBusinessTeam(Team $businessTeam): self
    {
        if ($this->businessTeams->removeElement($businessTeam)) {
            $businessTeam->removeBusiness($this);
        }

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getManagerTeams(): Collection
    {
        return $this->managerTeams;
    }

    public function addManagerTeam(Team $managerTeam): self
    {
        if (!$this->managerTeams->contains($managerTeam)) {
            $this->managerTeams[] = $managerTeam;
            $managerTeam->addManager($this);
        }

        return $this;
    }

    public function removeManagerTeam(Team $managerTeam): self
    {
        if ($this->managerTeams->removeElement($managerTeam)) {
            $managerTeam->removeManager($this);
        }

        return $this;
    }
}
