<?php

namespace App\Entity;

use App\Repository\DsnFileDeclarationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DsnFileDeclarationRepository::class)
 */
class DsnFileDeclaration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siret;

    /**
     * @ORM\Column(type="datetime")
     */
    private $month;

    /**
     * @ORM\Column(type="integer")
     */
    private $personnelNumbersCount;

    /**
     * @ORM\ManyToOne(targetEntity=DsnFile::class, inversedBy="dsnFileDeclarations")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $dsnFile;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getMonth(): ?\DateTimeInterface
    {
        return $this->month;
    }

    public function setMonth(\DateTimeInterface $month): self
    {
        $this->month = $month;

        return $this;
    }

    public function getPersonnelNumbersCount(): ?int
    {
        return $this->personnelNumbersCount;
    }

    public function setPersonnelNumbersCount(int $personnelNumbersCount): self
    {
        $this->personnelNumbersCount = $personnelNumbersCount;

        return $this;
    }

    public function getDsnFile(): ?DsnFile
    {
        return $this->dsnFile;
    }

    public function setDsnFile(?DsnFile $dsnFile): self
    {
        $this->dsnFile = $dsnFile;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
