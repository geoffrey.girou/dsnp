<?php

namespace App\Entity;

use App\Repository\DsnFileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DsnFileRepository::class)
 */
class DsnFile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="dsnFiles", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dsnpapiId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $storageName;

    /**
     * @ORM\OneToMany(targetEntity=DsnFileDeclaration::class, mappedBy="dsnFile")
     */
    private $dsnFileDeclarations;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->dsnFileDeclarations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDsnpapiId(): ?int
    {
        return $this->dsnpapiId;
    }

    public function setDsnpapiId(?int $dsnpapiId): self
    {
        $this->dsnpapiId = $dsnpapiId;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getStorageName(): ?string
    {
        return $this->storageName;
    }

    public function setStorageName(string $storageName): self
    {
        $this->storageName = $storageName;

        return $this;
    }

    /**
     * @return Collection|DsnFileDeclaration[]
     */
    public function getDsnFileDeclarations(): Collection
    {
        return $this->dsnFileDeclarations;
    }

    public function addDsnFileDeclaration(DsnFileDeclaration $dsnFileDeclaration): self
    {
        if (!$this->dsnFileDeclarations->contains($dsnFileDeclaration)) {
            $this->dsnFileDeclarations[] = $dsnFileDeclaration;
            $dsnFileDeclaration->setDsnFile($this);
        }

        return $this;
    }

    public function removeDsnFileDeclaration(DsnFileDeclaration $dsnFileDeclaration): self
    {
        if ($this->dsnFileDeclarations->removeElement($dsnFileDeclaration)) {
            // set the owning side to null (unless already changed)
            if ($dsnFileDeclaration->getDsnFile() === $this) {
                $dsnFileDeclaration->setDsnFile(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
