<?php

namespace App\Entity;

use App\Repository\WizardRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WizardRepository::class)
 */
class Wizard
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFirstSirenAdded;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFirstDsnFileAddedAndStatusOk;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="wizard", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWelcomeOk;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsFirstSirenAdded(): ?bool
    {
        return $this->isFirstSirenAdded;
    }

    public function setIsFirstSirenAdded(bool $isFirstSirenAdded): self
    {
        $this->setUpdatedAt(new \DateTime());
        $this->isFirstSirenAdded = $isFirstSirenAdded;

        return $this;
    }

    public function getIsFirstDsnFileAddedAndStatusOk(): ?bool
    {
        return $this->isFirstDsnFileAddedAndStatusOk;
    }

    public function setIsFirstDsnFileAddedAndStatusOk(bool $isFirstDsnFileAddedAndStatusOk): self
    {
        $this->setUpdatedAt(new \DateTime());
        $this->isFirstDsnFileAddedAndStatusOk = $isFirstDsnFileAddedAndStatusOk;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsWelcomeOk(): ?bool
    {
        return $this->isWelcomeOk;
    }

    public function setIsWelcomeOk(bool $isWelcomeOk): self
    {
        $this->setUpdatedAt(new \DateTime());
        $this->isWelcomeOk = $isWelcomeOk;

        return $this;
    }

    public function complete()
    {
        $this->isWelcomeOk = true;
        $this->isFirstSirenAdded = true;
        $this->isFirstDsnFileAddedAndStatusOk = true;
    }
}
