<?php

namespace App\Entity;

use App\Repository\SirenRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SirenRepository::class)
 */
class Siren
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="sirens")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxPersonnelNumbers;

    /**
     * @ORM\OneToMany(targetEntity=SirenAccess::class, mappedBy="siren")
     */
    private $sirenAccesses;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->sirenAccesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getMaxPersonnelNumbers(): ?int
    {
        return $this->maxPersonnelNumbers;
    }

    public function setMaxPersonnelNumbers(int $maxPersonnelNumbers): self
    {
        $this->maxPersonnelNumbers = $maxPersonnelNumbers;

        return $this;
    }

    /**
     * @return Collection|SirenAccess[]
     */
    public function getSirenAccesses(): Collection
    {
        return $this->sirenAccesses;
    }

    public function addSirenAccess(SirenAccess $sirenAccess): self
    {
        if (!$this->sirenAccesses->contains($sirenAccess)) {
            $this->sirenAccesses[] = $sirenAccess;
            $sirenAccess->setSiren($this);
        }

        return $this;
    }

    public function removeSirenAccess(SirenAccess $sirenAccess): self
    {
        if ($this->sirenAccesses->removeElement($sirenAccess)) {
            // set the owning side to null (unless already changed)
            if ($sirenAccess->getSiren() === $this) {
                $sirenAccess->setSiren(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
