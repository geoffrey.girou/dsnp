<?php


namespace App\Mailer;

use App\Service\UserService;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class Mailer
{
    private MailerInterface $mailerInterface;
    private LoggerInterface $loggerInterface;
    private UserService $userService;

    public function __construct(MailerInterface $mailer, LoggerInterface $logger, UserService $service)
    {
        $this->mailerInterface = $mailer;
        $this->loggerInterface = $logger;
        $this->userService = $service;
    }

    public function sendEmail(string $subject, string $to, string $template, array $data = [])
    {
        $user = $this->userService->findByEmail($to);
        $customLogo = $user->getFinalParent()->getLogo();

        $data['logo'] = ($customLogo !== null) ? $customLogo : null;

        $email = (new TemplatedEmail())
            ->from(new Address($_SERVER['APP_MAIL_FROM_ADDRESS'], $_SERVER['APP_MAIL_FROM_LABEL']))
            ->to($to)
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject($subject)
            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)
            // pass variables (name => value) to the template
            ->context($data);

        $this->mailerInterface->send($email);
        $this->loggerInterface->info('Mail sent with subject [ '.
        $subject.' ] to [ '.
        $to.' ]');

        return true;
    }
}