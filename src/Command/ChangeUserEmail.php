<?php


namespace App\Command;

use App\Service\UserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ChangeUserEmail extends Command
{
    protected static $defaultName = 'app:change-user-email';
    private UserService $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Given a valid user\'s email address provide a new one to apply changes.
            Beware this is a highly risked operation with consequences'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
            'Beware this is a highly risked operation with consequences !!!',
            'So far use cases for this are either user migration or email change with material proof.'
        ]);

        $helper = $this->getHelper('question');
        $question = new Question('Please enter the actual user\'s email : ', 'email');

        $email = $helper->ask($input, $output, $question);

        $user = $this->userService->findByEmail($email);
        if(!$user) {
            $output->writeln([
                'No user found.',
                'Command ended...',
            ]);
            return 0;
        }

        $question = new Question('Please enter the new user\'s email : ', 'newEmail');

        $newEmail = $helper->ask($input, $output, $question);

        if (!filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {
            $output->writeln([
                'Email seems not valid to me.',
                'Command ended...',
            ]);
            return 0;
        }

        $user->setEmail($newEmail);
        $this->userService->save($user);

        $output->writeln([
            'User with email '.$email.' has now this email '.$newEmail.'.',
            'Command ended successfully ...',
        ]);

        return 0;
    }
}