<?php


namespace App\Command;


use App\Entity\User;
use App\Mailer\Mailer;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendPasswordCode extends Command
{
    protected static $defaultName = 'app:send-password-code';

    private UserRepository $repository;
    private Mailer $mailer;

    public function __construct(
        UserRepository $repository,
        Mailer $mailer)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Send mail to users when reset password was asked'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);
        $users = $this->repository->findResetPasswordAsked();
        $output->writeln([
            sizeof($users),
        ]);

        $code = uniqid();

        foreach ($users as $user) {
            $now = new \DateTime();
            $resetPasswordCodeSentAt = $user->getResetPasswordCodeSentAt();
            if($resetPasswordCodeSentAt!=null) {
                $interval = date_diff($resetPasswordCodeSentAt,$now);
                if($interval->format("%H")!=="00") {
                    $this->process($user,$code);
                }
            }else {
                $this->process($user,$code);
            }
        }

        return 0;
    }

    private function process(User $user, string $code) {
        $this->mailer->sendEmail(
            'Changement de mot de passe',
            $user->getEmail(),
            'code.reset.password.html.twig',
            [
                'code'=>$code
            ]
        );

        $this->repository->resetPasswordCodeSent($user,$code);
    }
}