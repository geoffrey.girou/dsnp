<?php


namespace App\Command;

use App\Service\UserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class UnsetUserAdmin extends Command
{
    protected static $defaultName = 'app:unset-user-admin';
    private UserService $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Remove admin right to a user.'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);

        $helper = $this->getHelper('question');
        $question = new Question('Please enter the user email : ', 'email');

        $email = $helper->ask($input, $output, $question);

        $user = $this->userService->findByEmail($email);
        if(!$user) {
            $output->writeln([
                'No user found.',
                'Command ended...',
            ]);
            return 0;
        }
        $user->setRoles(['ROLE_BUSINESS','ROLE_USER']);
        $user->setCanUploadDsnFile(true);
        $user->setCanCreateUser(true);
        $user->setCanAccessData(true);
        $user->setUpdatedAt(new \DateTime());
        $user->getWizard()->setIsWelcomeOk(false);
        $user->getWizard()->setIsFirstDsnFileAddedAndStatusOk(false);
        $user->getWizard()->setIsFirstSirenAdded(false);
        $user->getWizard()->setUpdatedAt(new \DateTime());
        $this->userService->save($user);

        $output->writeln([
            'User is not ADMIN anymore.',
            'Command ended...',
        ]);

        return 0;
    }
}