<?php


namespace App\Command;

use App\Archive\DsnArchive;
use App\DsnFile\DsnFile;
use App\Entity\DsnFile as EntityDsnFile;
use App\Repository\DsnFileRepository;
use App\Service\Dsnfile\DsnFileUpdateStatusService;
use App\Service\Dsnpapi\DsnpapiDsnFileService;
use App\Service\DsnpDataService;
use App\Service\UserService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class ProcessDsnArchive extends Command
{
    protected static $defaultName = 'app:process-dsn-archive';
    private LoggerInterface $logger;
    private UserService $userService;
    private DsnFileUpdateStatusService $dsnFileService;
    private DsnpDataService $dsnpDataService;
    private DsnpapiDsnFileService $dsnpapiDsnFileService;
    private DsnFileRepository $dsnFileRepository;

    public function __construct(UserService $userService,
    LoggerInterface $logger,
    DsnFileUpdateStatusService $dsnFileService,
    DsnpDataService $dsnpDataService,
    DsnpapiDsnFileService $dsnpapiDsnFileService,
        DsnFileRepository $dsnFileRepository) {
        $this->logger = $logger;
        $this->userService = $userService;
        $this->dsnFileService = $dsnFileService;
        $this->dsnpDataService = $dsnpDataService;
        $this->dsnpapiDsnFileService = $dsnpapiDsnFileService;
        $this->dsnFileRepository = $dsnFileRepository;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Extract a DsnArchive, check for DsnFiles and process'
        )
        ->addArgument('userId', InputArgument::REQUIRED, 'User identifier')
        ->addArgument('archiveName', InputArgument::REQUIRED, 'Name of the archive')
        ->addArgument('isAnonymized', InputArgument::REQUIRED, 'Anonymisation status');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $output->writeln([
            'Command started...',
        ]);
        $this->logger->info('Command started...');

        $userId = $input->getArgument('userId');
        $user = $this->userService->findById($userId);
        if($user==null) {
            $this->logger->error('User with id :'.$userId.' not found. Cannot process archive.');
            $output->writeln([
                'Command ended with error ...',
            ]);
            return 1;
        }
        $archiveName = $input->getArgument('archiveName');
        $isAnonymized = $input->getArgument('isAnonymized');

        $this->logger->info('User id : '.$userId);
        $this->logger->info('Archive name : '.$archiveName);
        $this->logger->info('Is anonymised : '.$isAnonymized);
        
        $dsnArchive = new DsnArchive();
        $dsnArchive->createFromFile(__DIR__.'/../../tmp/archive/'.$userId,$archiveName);

        $dsnArchive->extract();
        $dsnArchive->deleteArchive();
        $this->logger->info('Archive deleted');

        $finder = new Finder();
        $finder->files()->in($dsnArchive->getExtractFolder());
        
        $dsnFiles = [];

        foreach ($finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $this->logger->info('File found : '.$absoluteFilePath);
            $dsnFile = new DsnFile();
            $dsnFile->createFromPath($absoluteFilePath);
            if($dsnFile->isValid()) {
                $this->logger->info('Accepted as DSN file');
                $dsnFileEntity = $this->processDsnFile($user,$file->getFilename());
                $dsnFiles[] = $dsnFileEntity;
            }else {
                $this->logger->info('Not as DSN file');
            }
        }

        // duplicate code from dsnfile controller

        $finalParent = $user->getFinalParent();
        if($finalParent->getDsnpData()===null) {
            // Create Siren collection
            $dsnpData = $this->dsnpDataService->createSirenCollection($finalParent);
            $finalParent->setDsnpData($dsnpData);
            // Add Siren to collection
            $this->dsnpDataService->addSirensToCollection($finalParent);
        }

        foreach ($dsnFiles as $dsnFile) {
            $dsnFile = $this->dsnpapiDsnFileService->sendDsnFile(
                __DIR__.'/../../tmp/archive/'.$userId.'/'.$dsnFile->getName(),
                $dsnFile,
                $finalParent->getDsnpData(),
                $isAnonymized=="1"
            );
            $this->dsnFileRepository->save($dsnFile);
        }

        $dsnArchive->deleteExtractFolder();
        $this->logger->info('Extract folder deleted');

        $output->writeln([
            'Command ended successfully ...',
        ]);
        $this->logger->info('Command ended successfully ...');

        return 0;
    }

    /**
     * @param $user
     * @param $filename
     * @return EntityDsnFile
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function processDsnFile($user,$filename)
    {
        return $this->dsnFileRepository->create(
            $user,
            $filename,
            uniqid().'.dsn'
        );
    }
}