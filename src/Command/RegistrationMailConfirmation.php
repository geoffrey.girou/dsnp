<?php


namespace App\Command;

use App\Entity\User;
use App\Mailer\Mailer;
use App\Repository\UserRepository;
use App\Service\PasswordGeneratorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationMailConfirmation extends Command
{
    protected static $defaultName = 'app:registration-mail-confirmation';

    private Mailer $mailer;
    private UserRepository $repository;
    private PasswordGeneratorService $passwordGeneratorService;
    private UserPasswordEncoderInterface $encoder;

    public function __construct(
        UserRepository $repository,
        Mailer $mailer,
        PasswordGeneratorService $passwordGeneratorService,
        UserPasswordEncoderInterface $encoder)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
        $this->passwordGeneratorService = $passwordGeneratorService;
        $this->encoder = $encoder;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Ask new user registered to confirm email'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);
        $users = $this->repository->findNotActiveClient();
        $output->writeln([
            sizeof($users),
        ]);

        if(sizeof($users)===0) {
            $output->writeln([
                'Command ended...',
            ]);

            return 0;
        }

        foreach ($users as $user) {
            $password = null;
            if($user->isAChild()) {
                // Generate password
                $password = $this->passwordGeneratorService->generate();
                $user->setPassword($this->encoder->encodePassword(new User(), $password));
                $user->setUpdatedAt(new \DateTime());
                $this->repository->save($user);
            }

            $user->setActivationCode(uniqid());
            $this->mailer->sendEmail(
                'DSN Plus vous souhaite la bienvenue',
                $user->getEmail(),
                'mail.confirmation.html.twig',
                [
                    'link'=>$_SERVER['APP_MAIL_CONFIRMATION_URL'].'?code='.$user->getActivationCode().'&email='.urlencode($user->getEmail()),
                    'password'=>$password
                ]
            );

            $this->repository->mailConfirmationLinkSent($user);
        }

        return 0;
    }
}