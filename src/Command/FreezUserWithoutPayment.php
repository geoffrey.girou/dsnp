<?php


namespace App\Command;


use App\Mailer\Mailer;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FreezUserWithoutPayment extends Command
{
    protected static $defaultName = 'app:freez-user';

    private Mailer $mailer;
    private UserRepository $repository;

    public function __construct(UserRepository $repository,
        Mailer $mailer)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Freez any user with payment_reported_at null
            two weeks after registration'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);
        $users = $this->repository->findClientToFreez();

        $output->writeln([
            sizeof($users),
        ]);

        if (sizeof($users) === 0) {
            $output->writeln([
                'Command ended...',
            ]);

            return 0;
        }

        foreach ($users as $user) {
            if(!$user->hasRole('ROLE_ADMIN')) {
                $this->repository->freezUser($user);
                $output->writeln([
                    $user->getEmail().' is now frozen.',
                ]);

                $this->mailer->sendEmail(
                    'Fin de votre période d’essai',
                    $user->getEmail(),
                    'no.payment.after.registration.html.twig',
                    []
                );

                $admins = $this->repository->findByRole('ROLE_ADMIN');

                foreach ($admins as $admin) {
                    $this->mailer->sendEmail(
                        'Fin de période d’essai',
                        $admin->getEmail(),
                        'admin.notification.no.payment.after.registration.html.twig',
                        ['user'=>$user]
                    );
                }
            }
        }

        return 0;
    }
}