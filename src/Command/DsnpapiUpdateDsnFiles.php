<?php


namespace App\Command;


use App\Entity\DsnFile;
use App\Repository\DsnFileRepository;
use App\Service\DsnFileDeclarationService;
use App\Service\Dsnpapi\DsnpapiDsnFileService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DsnpapiUpdateDsnFiles extends Command
{
    protected static $defaultName = 'app:dsnpapi-status';

    private DsnFileRepository $repository;
    private DsnpapiDsnFileService $dsnpapiDsnFileService;
    private DsnFileDeclarationService $dsnFileDeclarationService;

    public function __construct(
        DsnFileRepository $repository,
        DsnpapiDsnFileService $dsnpapiDsnFileService,
        DsnFileDeclarationService $dsnFileDeclarationService
    )
    {
        $this->repository = $repository;
        $this->dsnpapiDsnFileService = $dsnpapiDsnFileService;
        $this->dsnFileDeclarationService = $dsnFileDeclarationService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Update status of dsn files from dsnpapi'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);
        /** @var DsnFile[] $dsnFiles */
        $dsnFiles = $this->repository->findDsnFilesToUpdate();

        if (!is_array($dsnFiles) || sizeof($dsnFiles) === 0) {
            $output->writeln([
                'Command ended...',
            ]);

            return 0;
        }

        $output->writeln([
            sizeof($dsnFiles),
        ]);

        foreach ($dsnFiles as $dsnFile) {
            $output->writeln([
                'Updating file',
                $dsnFile->getName(),
            ]);
            $response = $this->dsnpapiDsnFileService->dsnFileDetails(
                $dsnFile->getUser()->getDsnpData()->getUuid(),
                $dsnFile->getDsnpapiId()
            );

            $status = $response['statut_traitement'];
            $declarations = $response['declarations'];

            if(sizeof($dsnFile->getDsnFileDeclarations())!==sizeof($declarations)) {
                foreach ($declarations as $declaration) {
                    $this->dsnFileDeclarationService->add(
                        $dsnFile,
                        $declaration['nombre_individus'],
                        $declaration['siret'],
                        $declaration['mois']
                    );
                }
            }
            $dsnFile->setStatus($status);
            $dsnFile->setUpdatedAt(new \DateTime());
            $this->repository->save($dsnFile);
        }

        $output->writeln([
            'Command ended...',
        ]);

        return 0;
    }
}