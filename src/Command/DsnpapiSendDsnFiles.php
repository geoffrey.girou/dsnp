<?php


namespace App\Command;


use App\Entity\DsnFile;
use App\Repository\DsnFileRepository;
use App\Service\Dsnpapi\DsnpapiDsnFileService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class DsnpapiSendDsnFiles extends Command
{
    protected static $defaultName = 'app:dsnpapi-send';

    private DsnFileRepository $repository;
    private DsnpapiDsnFileService $dsnpapiDsnFileService;

    public function __construct(
        DsnFileRepository $repository,
        DsnpapiDsnFileService $dsnpapiDsnFileService
    )
    {
        $this->repository = $repository;
        $this->dsnpapiDsnFileService = $dsnpapiDsnFileService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Send waiting dsn files'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);
        /** @var DsnFile[] $dsnFiles */
        $dsnFiles = $this->repository->findDsnFilesToSend();

        if (!is_array($dsnFiles) || sizeof($dsnFiles) === 0) {
            $output->writeln([
                'Command ended...',
            ]);

            return 0;
        }

        $output->writeln([
            sizeof($dsnFiles),
        ]);

        foreach ($dsnFiles as $dsnFile) {
            $output->writeln([
                'Sending file',
                $dsnFile->getStorageName(),
            ]);
            // $this->dsnpapiDsnFileService->sendDsnFile($dsnFile);
        }

        $output->writeln([
            'Command ended...',
        ]);

        return 0;
    }
}