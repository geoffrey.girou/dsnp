<?php


namespace App\Command;

use App\Service\UserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class SetUserAdmin extends Command
{
    protected static $defaultName = 'app:set-user-admin';
    private UserService $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Give admin right to a user. !!! Be very careful while granting this access to a user !!!'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);

        $helper = $this->getHelper('question');
        $question = new Question('Please enter the user email : ', 'email');

        $email = $helper->ask($input, $output, $question);

        $user = $this->userService->findByEmail($email);
        if(!$user) {
            $output->writeln([
                'No user found.',
                'Command ended...',
            ]);
            return 0;
        }
        $user->setRoles(['ROLE_ADMIN','ROLE_USER']);
        $user->setCanUploadDsnFile(false);
        $user->setCanCreateUser(false);
        $user->setCanAccessData(false);
        $user->setUpdatedAt(new \DateTime());
        $user->setPaymentReportedAt(new \DateTime());
        $user->getWizard()->setIsWelcomeOk(true);
        $user->getWizard()->setIsFirstDsnFileAddedAndStatusOk(true);
        $user->getWizard()->setIsFirstSirenAdded(true);
        $user->getWizard()->setUpdatedAt(new \DateTime());
        $this->userService->save($user);

        $output->writeln([
            'User just became an ADMIN.',
            'Command ended...',
        ]);

        return 0;
    }
}