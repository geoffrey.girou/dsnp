<?php


namespace App\Command;


use App\Mailer\Mailer;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegistrationNotifier extends Command
{
    protected static $defaultName = 'app:registration-notifier';

    private Mailer $mailer;
    private UserRepository $repository;

    public function __construct(
        UserRepository $repository,
        Mailer $mailer)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription(
            'Alert mail for each new user registered'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command started...',
        ]);
        $users = $this->repository->findNewRegisteredClient();
        $output->writeln([
            sizeof($users),
        ]);

        if(sizeof($users)===0) {
            $output->writeln([
                'Command ended...',
            ]);

            return 0;
        }
        // Fetch all admin
        $admins = $this->repository->findByRole('ROLE_ADMIN');

        foreach ($admins as $admin) {
            $this->mailer->sendEmail(
                'Rapport de nouvelle(s) inscription(s)',
                $admin->getEmail(),
                'registration.notifier.html.twig',
                ['users'=>$users]
            );
        }

        foreach ($users as $user) {
            $user->setReportedAt(new \DateTime());
            $this->repository->save($user);
        }

        $output->writeln([
            'Command ended...',
        ]);

        return 0;
    }
}