<?php


namespace App\Action;


use App\Entity\User;
use App\Service\Dsnpapi\DsnpapiSirenCollectionService;

class RemoveSirensAction
{
    private DsnpapiSirenCollectionService $dsnpapiSirenCollectionService;

    public function __construct(DsnpapiSirenCollectionService $dsnpapiSirenCollectionService)
    {
        $this->dsnpapiSirenCollectionService = $dsnpapiSirenCollectionService;
    }

    public function process(
        User $user,
        $sirens
    ) {
        if ($user->getDsnpData() !== null) {
            $this->dsnpapiSirenCollectionService->removeSirens(
                $user->getDsnpData()->getUuid(),
                $sirens
            );
        }
    }
}