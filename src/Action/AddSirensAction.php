<?php


namespace App\Action;


use App\Entity\Siren;
use App\Entity\SirenAccess;
use App\Entity\User;
use App\Service\Dsnpapi\DsnpapiSirenCollectionService;
use App\Service\SirenAccessService;

class AddSirensAction
{
    public function process(User $user,
        DsnpapiSirenCollectionService $dsnpapiSirenCollectionService,
        SirenAccessService $sirenAccessService,
        Siren $newSiren,
        Siren $existingSiren)
    {
        if($user->getDsnpData()!=null) {
            $dsnpapiSirenCollectionService->addSirens($user->getDsnpData()->getUuid(),[$newSiren]);
        }
        /** @var SirenAccess[] $sirenAccesses */
        $sirenAccesses = $sirenAccessService->findBySirenAndUserIds($existingSiren,$user->getChildren());

        if($user->isAChild()) {
            $sirenAccessService->add(
                $user,
                $newSiren
            );
        }

        foreach ($sirenAccesses as $sirenAccess) {
            $this->process($sirenAccess->getUser(),
                $dsnpapiSirenCollectionService,
                $sirenAccessService,
                $newSiren,
                $existingSiren
            );
        }
    }
}