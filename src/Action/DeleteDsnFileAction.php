<?php

namespace App\Action;

use App\Entity\DsnFile;
use App\Entity\User;
use App\Repository\DsnFileRepository;
use App\Service\Dsnpapi\DsnpapiDsnFileService;

class DeleteDsnFileAction
{
    private DsnpapiDsnFileService $dsnpapiDsnFileService;
    private DsnFileRepository $repository;

    public function __construct(DsnpapiDsnFileService $dsnpapiDsnFileService, DsnFileRepository $repository)
    {
        $this->dsnpapiDsnFileService = $dsnpapiDsnFileService;
        $this->repository = $repository;
    }

    public function process(User $user, DsnFile $dsnfile, bool $deleteData)
    {
        if ($dsnfile->getDsnpapiId() !== null) {
            $this->dsnpapiDsnFileService->delete(
                $user->getFinalParent()->getDsnpData()->getUuid(),
                $dsnfile->getDsnpapiId(),
                $deleteData
            );
        }
        $this->repository->delete($dsnfile);
    }
}