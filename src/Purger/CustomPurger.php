<?php

namespace App\Purger;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Purger\ORMPurgerInterface;
use Doctrine\ORM\EntityManagerInterface;

class CustomPurger extends ORMPurger implements ORMPurgerInterface
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->setEntityManager($em);
    }

    /**
     * {@inheritDoc}
     */
    function purge()
    {
        $connection = $this->getObjectManager()->getConnection();
        $this->setPurgeMode(parent::PURGE_MODE_TRUNCATE);

        try {
            $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0');
            parent::purge();
        } finally {
            $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1');
        }
    }
}