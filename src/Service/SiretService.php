<?php


namespace App\Service;


use App\Entity\Siren;
use App\Entity\Siret;
use App\Entity\User;
use App\Repository\SiretRepository;

class SiretService
{
    private SiretRepository $siretRepository;

    public function __construct(SiretRepository $siretRepository)
    {
        $this->siretRepository = $siretRepository;
    }

    public function findForUser(User $user)
    {
        $sirets = [];
        foreach ($user->getSirens() as $siren) {
            $sirets = array_merge($sirets,$this->siretRepository->findBy(['siren'=>$siren]));
        }
        return $sirets;
    }

    public function findBySiren(Siren $siren)
    {
        return $this->siretRepository->findBy(['siren'=>$siren]);
    }

    public function findBySirenAndNic(Siren $siren, int $nic)
    {
        return $this->siretRepository->findOneBy(['siren'=>$siren, 'nic'=>$nic]);
    }

    public function exists(Siren $siren, string $nic): bool
    {
        return $this->siretRepository->findOneBy([
            'siren'=>$siren,
            'nic'=>$nic
        ])!==null;
    }

    public function existsAndReturns(int $id)
    {
        $siret = $this->siretRepository->findOneBy([
            'id'=>$id
        ]);

        return ($siret===null) ? false : $siret;
    }

    public function create(Siren $siren, string $nic, string $name)
    {
        return $this->siretRepository->create($siren, $nic, $name);
    }

    public function delete(Siret $siret): void
    {
        $this->siretRepository->delete($siret);
    }

    public function update(int $id, Siren $siren, string $nic, string $name)
    {
        $siret = $this->siretRepository->findOneBy(['id'=>$id]);
        if($siret===null) {
            return false;
        }
        return $this->siretRepository->update($siret,$siren,$nic,$name);
    }
}