<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\Wizard;
use App\Repository\WizardRepository;

class WizardService
{
    private WizardRepository $repository;

    public function __construct(WizardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(User $user)
    {
        $wizard = new Wizard();
        $wizard->setCreatedAt(new \DateTime());
        $wizard->setIsWelcomeOk(false);
        $wizard->setIsFirstDsnFileAddedAndStatusOk(false);
        $wizard->setIsFirstSirenAdded(false);
        $wizard->setUser($user);

        return $wizard;
    }

    public function save(Wizard $wizard)
    {
        $this->repository->save($wizard);
    }

    public function welcomeOk(Wizard $wizard)
    {
        $wizard->setIsWelcomeOk(true);
        return $this->repository->save($wizard);
    }

    public function firstSirenAdded(Wizard $wizard)
    {
        $wizard->setIsFirstSirenAdded(true);
        return $this->repository->save($wizard);
    }

    public function firstDsnFileAddedAndStatusOk(Wizard $wizard)
    {
        $wizard->setIsFirstDsnFileAddedAndStatusOk(true);
        return $this->repository->save($wizard);
    }
}