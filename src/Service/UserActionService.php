<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\UserAction;
use App\Enum\UserActionEnum;
use App\Repository\UserActionRepository;

class UserActionService
{
    private UserActionRepository $repository;

    public function __construct(UserActionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function add(User $user, string $type)
    {
        $userAction = new UserAction();
        $userAction->setUser($user);
        $userAction->setType($type);
        return $this->repository->add($userAction,$type);
    }
}