<?php


namespace App\Service;


use App\Entity\Siren;
use App\Entity\SirenAccess;
use App\Entity\User;
use App\Repository\SirenAccessRepository;

class SirenAccessService
{
    private SirenAccessRepository $sirenAccessRepository;

    public function __construct(SirenAccessRepository $sirenAccessRepository)
    {
        $this->sirenAccessRepository = $sirenAccessRepository;
    }

    public function add(User $user,Siren $siren)
    {
        $sirenAccess = new SirenAccess();
        $sirenAccess->setCreatedAt(new \DateTime());
        $sirenAccess->setUser($user);
        $sirenAccess->setSiren($siren);

        return $this->sirenAccessRepository->save($sirenAccess);
    }

    /**
     * @param Siren $siren
     * @param $users
     * @return int|mixed|string
     */
    public function findBySirenAndUserIds(Siren $siren, $users)
    {
        $userIds = [];
        foreach ($users as $user) {
            $userIds[] = $user->getId();
        }
        return $this->sirenAccessRepository->findBySirenAndUserIds($siren,$userIds);
    }
}