<?php


namespace App\Service;


use App\Entity\DsnpData;
use App\Entity\User;
use App\Exception\NoDsnpapiSirenCollectionException;
use App\Helper\UuidGenerator;
use App\Repository\DsnpDataRepository;
use App\Service\Dsnpapi\DsnpapiSirenCollectionService;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class DsnpDataService
{
    private DsnpDataRepository $repository;
    private DsnpapiSirenCollectionService $dsnpapiSirenCollectionService;

    public function __construct(
        DsnpDataRepository $repository,
        DsnpapiSirenCollectionService $dsnpapiSirenCollectionService
    ) {
        $this->repository = $repository;
        $this->dsnpapiSirenCollectionService = $dsnpapiSirenCollectionService;
    }

    /**
     * @param User $user
     * @return DsnpData
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function createSirenCollection(User $user): DsnpData
    {
        $generator = new UuidGenerator();
        $generator->generate();

        $this->dsnpapiSirenCollectionService->createSirenCollection(
            $generator->toString()
        );

        return $this->repository->create($user, $generator->toString());
    }

    public function addSirensToCollection(User $user)
    {
        if ($user->getDsnpData() == null) {
            throw new NoDsnpapiSirenCollectionException("dsnpData attribute of user " . $user->getId() . " is null");
        }
        $this->dsnpapiSirenCollectionService->addSirens($user->getDsnpData()->getUuid(), $user->getAvailableSiren());
    }

    public function save(DsnpData $dsnpData): DsnpData
    {
        return $this->repository->save($dsnpData);
    }
}