<?php


namespace App\Service;


use App\Entity\Siren;
use App\Entity\User;
use App\Repository\SirenRepository;

class SirenService
{
    private SirenRepository $sirenRepository;

    public function __construct(
        SirenRepository $sirenRepository)
    {
        $this->sirenRepository = $sirenRepository;
    }

    /**
     * @param $id
     * @return \App\Entity\Siren|null
     */
    public function one($id)
    {
        return $this->sirenRepository->findOneBy(['id'=>$id]);
    }

    public function delete($id): bool
    {
        $siren = $this->one($id);
        if($siren===null) {
            return false;
        }
        $this->sirenRepository->delete($this->one($id));
        return true;
    }

    public function add(User $user,int $number,
        int $maxPersonnelNumbers, string $name)
    {
        $siren = new Siren();
        $siren->setCreatedAt(new \DateTime());
        $siren->setName($name);
        $siren->setUser($user);
        $siren->setNumber($number);
        $siren->setMaxPersonnelNumbers($maxPersonnelNumbers);
        return $this->sirenRepository->save($siren);
    }

    public function update(Siren $siren,int $number,
        int $maxPersonnelNumbers, string $name)
    {
        $siren->setUpdatedAt(new \DateTime());
        $siren->setName($name);
        $siren->setNumber($number);
        $siren->setMaxPersonnelNumbers($maxPersonnelNumbers);
        return $this->sirenRepository->save($siren);
    }

    /**
     * @param int $number
     * @return \App\Entity\Siren|null
     */
    public function oneByNumber(int $number)
    {
        return $this->sirenRepository->findOneBy(['number'=>$number]);
    }
}