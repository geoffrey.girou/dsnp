<?php


namespace App\Service;


class EmailGeneratorService
{
    public function generate($startingWith = '', $endingWith = '.com')
    {
        return $startingWith . $this->generateRandomString(20) . '@' . $this->generateRandomString() . $endingWith;
    }

    private function generateRandomString($length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}