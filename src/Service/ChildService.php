<?php


namespace App\Service;


use App\Entity\User;
use App\Repository\SirenRepository;
use App\Repository\UserRepository;

class ChildService
{
    private UserRepository $userRepository;
    private SirenRepository $sirenRepository;

    /**
     * ChildService constructor.
     * @param UserRepository $userRepository
     * @param SirenRepository $sirenRepository
     */
    public function __construct(UserRepository $userRepository, SirenRepository $sirenRepository)
    {
        $this->userRepository = $userRepository;
        $this->sirenRepository = $sirenRepository;
    }

    /**
     * @param $id
     * @return User|null
     */
    public function one($id)
    {
        return $this->userRepository->findOneBy(['id'=>$id]);
    }

    /**
     * @param User $child
     * @return bool
     */
    public function delete(User $child)
    {
        foreach ($child->getSirenAccesses() as $sirenAccess) {
            $child->removeSirenAccess($sirenAccess);
        }
        $this->userRepository->delete($child);
        return true;
    }
}