<?php


namespace App\Service\Dsnfile;


use App\Entity\DsnFile;
use App\Repository\DsnFileRepository;
use App\Service\DsnFileDeclarationService;
use App\Service\Dsnpapi\DsnpapiDsnFileService;

class DsnFileUpdateStatusService
{
    private DsnFileRepository $repository;
    private DsnFileDeclarationService $dsnFileDeclarationService;
    private DsnpapiDsnFileService $dsnpapiDsnFileService;
    public function __construct(DsnFileRepository $repository,
        DsnFileDeclarationService $dsnFileDeclarationService,
        DsnpapiDsnFileService $dsnpapiDsnFileService)
    {
        $this->repository = $repository;
        $this->dsnFileDeclarationService = $dsnFileDeclarationService;
        $this->dsnpapiDsnFileService = $dsnpapiDsnFileService;
    }

    /**
     * @param $id
     * @return DsnFile
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateStatus($id): DsnFile
    {
        $dsnFile = $this->repository->findOneBy(['id'=>$id]);
        $finalParent = $dsnFile->getUser()->getFinalParent();
        $response = $this->dsnpapiDsnFileService->dsnFileDetails(
            $finalParent->getDsnpData()->getUuid(),
            $dsnFile->getDsnpapiId()
        );
        $status = $response['statut_traitement'];
        $declarations = $response['declarations'];

        if(sizeof($dsnFile->getDsnFileDeclarations())!==sizeof($declarations)) {
            foreach ($declarations as $declaration) {
                $this->dsnFileDeclarationService->add(
                    $dsnFile,
                    $declaration['nombre_individus'],
                    $declaration['siret'],
                    $declaration['mois']
                );
            }
        }
        $dsnFile->setStatus($status);
        $dsnFile->setUpdatedAt(new \DateTime());
        $this->repository->save($dsnFile);

        return $dsnFile;
    }
}