<?php

namespace App\Service;

use App\Repository\GroupAccessRepository;

class GroupAccessService {
    
    private GroupAccessRepository $groupAccessRepository;

    public function __construct(GroupAccessRepository $groupAccessRepository) 
    {
        $this->groupAccessRepository = $groupAccessRepository;
    }

    public function deleleFromGroupApiId(string $uuid, string $groupApiId)
    {
        $groupsAccesses = $this->groupAccessRepository->findBy(['dsnpapiGroupId'=>$groupApiId]);
        foreach($groupsAccesses as $groupsAccess) {
            if($groupsAccess->getUser()->getFinalParent()->getDsnpData()->getUuid()===$uuid) {
                $this->groupAccessRepository->delete($groupsAccess);
            }
        }
    }
}