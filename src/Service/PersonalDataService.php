<?php


namespace App\Service;


use App\Entity\User;
use App\Entity\PersonalData;
use App\Repository\PersonalDataRepository;

class PersonalDataService
{
    private PersonalDataRepository $personalDataRepository;

    public function __construct(PersonalDataRepository $personalDataRepository)
    {
        $this->personalDataRepository = $personalDataRepository;
    }

    public function addPersonalData(User $user, string $companyName,
    string $firstname, string $lastname,
    string $addressLine1, string $addressLine2, string $postalCode,
        string $phoneNumber, string $city) : User
    {
        $personalData = new PersonalData();
        $personalData->setCreatedAt(new \DateTime());
        $personalData->setCompanyName($companyName);
        $personalData->setFirstname($firstname);
        $personalData->setLastname($lastname);
        $personalData->setAddressLine1($addressLine1);
        $personalData->setAddressLine2($addressLine2);
        $personalData->setPostalCode($postalCode);
        $personalData->setCity($city);
        $personalData->setPhoneNumber($phoneNumber);

        $user->setPersonalData($personalData);

        return $user;
    }

    public function update(
        User $user,
        string $companyName,
        string $firstname,
        string $lastname,
        string $addressLine1,
        string $addressLine2,
        string $postalCode,
        string $city,
        string $phoneNumber
    )
    {
        $personalData = $user->getPersonalData();
        $personalData->setCompanyName($companyName);
        $personalData->setFirstname($firstname);
        $personalData->setLastname($lastname);
        $personalData->setAddressLine1($addressLine1);
        $personalData->setAddressLine2($addressLine2);
        $personalData->setPostalCode($postalCode);
        $personalData->setCity($city);
        $personalData->setPhoneNumber($phoneNumber);
        return $this->save($personalData);
    }

    public function save(PersonalData $personalData): PersonalData
    {
        return $this->personalDataRepository->save($personalData);
    }
}