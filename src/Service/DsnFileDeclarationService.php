<?php


namespace App\Service;


use App\Entity\DsnFile;
use App\Entity\Siren;
use App\Entity\User;
use App\Repository\DsnFileDeclarationRepository;

class DsnFileDeclarationService
{
    private DsnFileDeclarationRepository $repository;

    public function __construct(DsnFileDeclarationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function add(
        DsnFile $dsnFile,
        int $personnelNumbersCount,
        int $siret,
        string $mois
    )
    {
        $firstOfMonthDatetime = \DateTime::createFromFormat('Y-m-d\TH:i:s',$mois);
        $dsnFileDeclaration = $this->repository->create(
            $dsnFile,
            $personnelNumbersCount,
            $siret,
            $firstOfMonthDatetime
        );

        return $this->repository->save($dsnFileDeclaration);
    }

    /**
     * @param array<Siren> $sirens
     * @return array
     */
    public function getSirensIntegrated(array $sirens)
    {
        $sirensIntegrated = [];
        foreach ($sirens as $siren) {
            $sirenDeclarations = $this->repository->findBySiren(strval($siren->getNumber()));
            if($sirenDeclarations!=null&&sizeof($sirenDeclarations)!=0) {
                $sirensIntegrated[] = $siren;
            }
        }
        return $sirensIntegrated;
    }

    public function getGrouped(User $user)
    {
        $dsnFiles = $user->getDsnFiles();

        $children = $user->getChildren();

        $dsnFileIds = [];
        foreach ($dsnFiles as $dsnFile) {
            $dsnFileIds[] = $dsnFile->getId();
        }

        foreach ($children as $child) {
            foreach ($child->getDsnFiles() as $dsnFile) {
                $dsnFileIds[] = $dsnFile->getId();
            }
        }

        return $this->repository->getGrouped($dsnFileIds);
    }
}