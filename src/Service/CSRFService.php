<?php


namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class CSRFService extends AbstractController
{
    const CSRF_KEY = 'dsnplus';

    public function generate()
    {
        return  $this->container->get('security.csrf.token_manager')->getToken(self::CSRF_KEY)->getValue();
    }

    public function verify($csrfToken)
    {
        if(!$this->isCsrfTokenValid(self::CSRF_KEY,$csrfToken)) {
            return new JsonResponse([
                'title' => 'Sécurité',
                'message' => 'Identité non vérifiée.'
            ], 403);
        }
    }
}