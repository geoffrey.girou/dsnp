<?php

namespace App\Service\String;

class PolyfillPhp8 {
    public static function polyfill_str_contains($haystack, $needle) {
        // based on original work from the PHP Laravel framework
        if (!function_exists('str_contains')) {
            function str_contains($haystack, $needle) {
                return $needle !== '' && mb_strpos($haystack, $needle) !== false;
            }
        }else {
            return str_contains($haystack, $needle);
        }
    }
}