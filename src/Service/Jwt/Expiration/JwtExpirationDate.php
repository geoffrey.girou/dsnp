<?php


namespace App\Service\Jwt\Expiration;

use DateInterval;
use DateTime;

class JwtExpirationDate
{
    private $expireAt;

    public function __construct()
    {
        $this->expireAt = new DateTime();
        $this->expireAt->add(new DateInterval(('PT1H')));
    }

    public function expirationDateStringForPayload()
    {
        // ex : 2022-12-30T09:25:33Z
        return $this->expireAt->format("Y-m-d\TH:m:s\Z");
    }
}