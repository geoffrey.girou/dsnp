<?php

namespace App\Service\Group;

use App\Entity\Siren;
use App\Model\Group;

class SirenInGroupService
{
    /**
     * Returns groups containing the siren
     * sort if siren is the only remaining
     * @param array<Group> $groupModels
     * @return array
     */
    public function verify(array $groupModels, Siren $siren)
    {
        $groupsWithMultipleSiren = [];
        $groupsWithOneSiren = [];
        foreach ($groupModels as $groupModel) {
            $countSirets = sizeof($groupModel->getSirets());
            $sirenFound = 0;
            foreach ($groupModel->getSirets() as $siret) {
                if (intval($siret->siren()) === $siren->getNumber()) {
                    $sirenFound++;
                }
            }
            if ($sirenFound !== 0) {
                if ($sirenFound === $countSirets) {
                    $groupsWithOneSiren[] = $groupModel;
                } else {
                    $groupsWithMultipleSiren[] = $groupModel;
                }
            }
        }
        return [
            'multiple' => $groupsWithMultipleSiren,
            'one' => $groupsWithOneSiren,
        ];
    }
}