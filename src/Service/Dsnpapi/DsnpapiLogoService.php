<?php


namespace App\Service\Dsnpapi;

use App\Enum\HTTPMethod;
use App\Exception\DsnpapiUnexpectedErrorCodeException;
use App\Service\Dsnpapi\Client\DsnpapiHttpClient;
use App\Service\Dsnpapi\Request\DsnpapiClientRequest;

class DsnpapiLogoService
{
    private DsnpapiHttpClient $client;
    
    public function __construct(DsnpapiHttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $uuid
     * @param string $filePath
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function add(string $uuid, string $filePath)
    {
        $request = new DsnpapiClientRequest();
        $request->setMethod(HTTPMethod::PUT);
        $request->setPath($uuid.'/properties/logo_110x42');
        $request->setBody(file_get_contents($filePath));

        $response =  $this->client->execute($request);

        if($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function delete(string $uuid)
    {
        $request = new DsnpapiClientRequest();
        $request->setMethod(HTTPMethod::DELETE);
        $request->setPath($uuid.'/properties/logo_110x42');

        $response =  $this->client->execute($request);

        if($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }
}