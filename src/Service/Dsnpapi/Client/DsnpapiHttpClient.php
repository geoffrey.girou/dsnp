<?php


namespace App\Service\Dsnpapi\Client;


use App\Exception\DsnpapiUnauthorizedHttpException;
use App\Service\Dsnpapi\Request\IDsnpapiRequest;
use App\Service\Dsnpapi\Response\DsnpapiResponse;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DsnpapiHttpClient implements LoggerAwareInterface
{
    /**
     * @var HttpClientInterface $client
     */
    private HttpClientInterface $client;
    /**
     * @var LoggerInterface $logger
     */
    private LoggerInterface $logger;

    public function __construct()
    {
        $this->client = HttpClient::create(['verify_peer' => false, 'verify_host' => false]);
    }

    /**
     * @param IDsnpapiRequest $request
     * @return DsnpapiResponse
     */
    public function execute(IDsnpapiRequest $request)
    {
        $options = $request->getOptions();
        if (!empty($request->getBody())) {
            $options['body'] = $request->getBody();
        }
        $this->logger->info('Send HTTP Request to : ' . $request->getPath());

        $response = $this->client->request(
            $request->getMethod(),
            $request->getPath(),
            $options);

        // getContent(false) won't throw exception for 4XX 5XX 3XX + transport
        // mandatory cause api is not properly based on http codes
        $this->logger->info('Response HTTP code : ' . $response->getStatusCode());

        if($response->getStatusCode() === 401) {
            throw new DsnpapiUnauthorizedHttpException("Cannot connect to dsnpapi check out credentials",500);
        }

        $dsnpapiResponse = new DsnpapiResponse($this->logger);
        $dsnpapiResponse->initWithArray(json_decode($response->getContent(false),true));

        return $dsnpapiResponse;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}