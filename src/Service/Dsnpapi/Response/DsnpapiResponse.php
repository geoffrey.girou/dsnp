<?php

namespace App\Service\Dsnpapi\Response;

use App\Enum\ApiErrorMessageFromCode;
use App\Exception\DsnpapiNoErrorCodeException;
use Psr\Log\LoggerInterface;


class DsnpapiResponse
{
    private bool $isError;

    private string $errorMessage;

    private array $data;
    /**
     * @var LoggerInterface $logger
     */
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array|null $data
     * @return $this
     * @throws DsnpapiNoErrorCodeException
     */
    public function initWithArray(array $data= null): DsnpapiResponse
    {
        $this->data = $data;
        $this->isError = false;

        if (!isset($data['error_code'])) {
            throw new DsnpapiNoErrorCodeException();
        } else {
            $apiErrorCode = $data['error_code'];
            if ($apiErrorCode !== 0) {
                $this->logger->info('Dsnp API Error with code : '.$apiErrorCode);
                $this->logger->info($data["message_details"]);
                $this->error($apiErrorCode);
            }

            return $this;
        }
    }

    public function error($apiErrorCode)
    {
        $this->errorMessage = ApiErrorMessageFromCode::$message[$apiErrorCode];
        $this->isError = true;
    }

    public function hasError(): bool
    {
        return $this->isError;
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage(string $errorMessage): void
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}