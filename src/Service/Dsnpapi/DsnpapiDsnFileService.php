<?php


namespace App\Service\Dsnpapi;


use App\Entity\DsnFile;
use App\Entity\DsnpData;
use App\Enum\HTTPMethod;
use App\Repository\DsnFileRepository;
use App\Service\Dsnpapi\Client\DsnpapiHttpClient;
use App\Service\Dsnpapi\Request\DsnpapiRequest;

class DsnpapiDsnFileService
{
    private DsnpapiHttpClient $client;

    public function __construct(DsnpapiHttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $uuid
     * @param int $dsnpapiId
     * @param bool $deleteData
     * @return array
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function delete(string $uuid, int $dsnpapiId, bool $deleteData = false)
    {
        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::DELETE);
        $path = $uuid . '/dsn/' . $dsnpapiId . '?fileonly=';
        $path .= ($deleteData) ? 'false' : 'true';
        $request->setPath($path);

        $response = $this->client->execute($request);
        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param int $dsnpapiId
     * @return array
     */
    public function dsnFileDetails(string $uuid, int $dsnpapiId)
    {
        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::GET);
        $request->setPath($uuid . '/dsn/' . $dsnpapiId);

        $response = $this->client->execute($request);
        return $response->getData();
    }

    /**
     * @param string $filePath
     * @param DsnFile $dsnFile
     * @param DsnpData $dsnpData
     * @param bool $isAnonymized
     * @return DsnFile
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function sendDsnFile(
        string $filePath,
        DsnFile $dsnFile,
        DsnpData $dsnpData,
        bool $isAnonymized = false
    ): DsnFile {
        $uuid = $dsnpData->getUuid();

        $path = ($isAnonymized) ? $uuid . '/dsn?anonymize=true' : $uuid . '/dsn';

        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::POST);
        $request->setPath($path);
        $request->setBody(file_get_contents($filePath));
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            $dsnFile->setStatus('Erreur de traitement : ' . $response->getErrorMessage());
        } else {
            $dsnId = $response->getData()['id'];
            $dsnFile->setStatus($response->getData()['statut_traitement']);
            $dsnFile->setDsnpapiId($dsnId);
        }

        $dsnFile->setUpdatedAt(new \DateTime());

        unlink($filePath);
        return $dsnFile;
    }
}