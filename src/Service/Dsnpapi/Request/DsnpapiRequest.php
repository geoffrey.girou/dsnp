<?php

namespace App\Service\Dsnpapi\Request;

class DsnpapiRequest implements IDsnpapiRequest
{
    protected string $method;

    protected string $apiUrl;

    protected string $path;

    protected string $body;

    private array $options;

    public function __construct()
    {
        $this->apiUrl = $_SERVER['APP_DSNPAPI_URL'];
        $this->options = [
            'auth_basic' => $_SERVER['APP_DSNPAPI_CLIENT_ID'] . ':' . $_SERVER['APP_DSNPAPI_SECRET'],
            'headers' => [
                'Content-Type: application/json'
            ]
        ];
        $this->body = '';
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setBody($body): void
    {
        $this->body = $body;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath($path): void
    {
        $this->path = ($path != null && $path != '') ? $this->apiUrl . '/' . $path : $this->apiUrl;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}