<?php

namespace App\Service\Dsnpapi\Request;

class DsnpapiClientRequest extends DsnpapiRequest implements IDsnpapiRequest
{
    public function __construct()
    {
        parent::__construct();
        $this->apiUrl = $_SERVER['APP_DSNPAPI_CLIENT_URL'];
    }
}