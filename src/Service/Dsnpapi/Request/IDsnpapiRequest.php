<?php

namespace App\Service\Dsnpapi\Request;

interface IDsnpapiRequest
{
    public function setMethod(string $method): void;
    public function getMethod();
    public function setPath($path): void;
    public function getPath();
    public function setBody($body): void;
    public function getBody();
    public function getOptions();
}