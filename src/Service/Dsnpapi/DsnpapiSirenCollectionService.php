<?php


namespace App\Service\Dsnpapi;

use App\Enum\HTTPMethod;
use App\Exception\DsnpapiUnexpectedErrorCodeException;
use App\Service\Dsnpapi\Client\DsnpapiHttpClient;
use App\Service\Dsnpapi\Request\DsnpapiRequest;

class DsnpapiSirenCollectionService
{
    private DsnpapiHttpClient $client;

    public function __construct(DsnpapiHttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $uuid
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function createSirenCollection(string $uuid): array
    {
        $body = array(
            "customer_uuid" => $uuid,
            "uuid" => $uuid,
            "comment" => "SAAS " . $uuid,
            "sirens" => [],
        );

        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::POST);
        $request->setPath('');
        $request->setBody(json_encode($body));

        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param $sirens
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function addSirens(string $uuid, $sirens): array
    {
        $formattedSirens = [];

        foreach ($sirens as $siren) {
            $formattedSirens[] = [
                "siren" => strval($siren->getNumber()),
                "nombre_salaries" => $siren->getMaxPersonnelNumbers(),
                "nombre_heures_par_jour" => 7
            ];
        }

        $body = array(
            "sirens" => $formattedSirens,
        );

        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::PUT);
        $request->setPath($uuid);
        $request->setBody(json_encode($body));

        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param array $sirens
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function removeSirens(string $uuid, array $sirens): array
    {
        $formattedSirens = [];

        foreach ($sirens as $siren) {
            $formattedSirens[] = strval($siren->getNumber());
        }

        $body = array(
            "sirens" => $formattedSirens,
        );

        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::DELETE);
        $request->setPath($uuid);
        $request->setBody(json_encode($body));

        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }
}