<?php


namespace App\Service;


use App\Entity\User;
use App\Repository\UserRepository;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use League\OAuth2\Client\Provider\GoogleUser;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private UserPasswordEncoderInterface $encoder;
    private UserRepository $repository;
    private bool $isResetPasswordCodeValid = false;
    private User $user;

    public function __construct(UserPasswordEncoderInterface $encoder, UserRepository $repository)
    {
        $this->encoder = $encoder;
        $this->repository = $repository;
    }

    public function createClient(string $email, string $password) : User
    {
        return $this->repository->createClient($email,$this->encoder->encodePassword(new User(),$password));
    }

    public function createChild(User $user, string $email, string $password,
        bool $canAccessData, bool $canCreateUser, bool $canUploadDsnFile
    , string $dsnpRole) : User
    {
        return $this->repository->createChild($user, $email,
            $this->encoder->encodePassword(new User(), $password),
            $canAccessData, $canCreateUser, $canUploadDsnFile,
            $dsnpRole);
    }

    public function findOrCreateFromOauthGithub(GithubResourceOwner $owner, string $password): User
    {
        /** @var User|null $user */
        $user = $this->repository->findFromGithub($owner);

        if($user) {
            if($user->getGithubId()===null) {
                $user->setGithubId($owner->getId());
                $this->repository->save($user);
            }
            return $user;
        }

        return $this->repository->createFromGithub($owner,$password);
    }

    public function findOrCreateFromOauthGoogle(GoogleUser $owner, string $password): User
    {
        /** @var User|null $user */
        $user = $this->repository->findFromGoogle($owner);

        if($user) {
            if($user->getGoogleId()===null) {
                $user->setGoogleId($owner->getId());
                $this->repository->save($user);
            }
            return $user;
        }

        return $this->repository->createFromGoogle($owner,$password);
    }

    public function updateChild(User $user,
        bool $canAccessData, bool $canCreateUser,
        bool $canUploadDsnFile, string $dsnpRole) : User
    {

        return $this->repository->updateChild($user,
            $canAccessData,
            $canCreateUser, $canUploadDsnFile, $dsnpRole);
    }

    public function updateChildSirenAccess(
        User $user,
        array $sirenAccesses
    )
    {
        return $this->repository->updateChildSirenAccess(
            $user,
            $sirenAccesses
        );
    }

    public function updateChildGroupAccess(
        User $user,
        array $groupAccesses
    )
    {
        return $this->repository->updateChildGroupAccess(
            $user,
            $groupAccesses
        );
    }

    public function save(User $user) : User
    {
        return $this->repository->save($user);
    }

    public function findByEmail($email)
    {
        return $this->repository->findOneBy(['email'=>$email]);
    }

    public function findById($id)
    {
        return $this->repository->findOneBy(['id'=>$id]);
    }

    public function askPasswordReset(User $user) : void
    {
        $user->setResetPasswordAskedAt(new \DateTime());
        $user->setIsResetPassword(true);
        $user->setUpdatedAt(new \DateTime());
        $this->repository->save($user);
    }

    public function checkPasswordResetCode(User $user, string $code) : bool
    {
        $this->user = $user;
        $codeSentAt = $user->getResetPasswordCodeSentAt();
        $now = new \DateTime();
        $interval = $codeSentAt->diff($now);
        $numericHourInterval = $interval->format('%h');

        if($user->getResetPasswordCode()===$code && $numericHourInterval<=1) {
            $this->isResetPasswordCodeValid = true;
            return true;
        }

        return false;
    }

    public function changePassword(string $password, UserPasswordEncoderInterface $encoder) : User
    {
        $this->user->setPassword($this->encoder->encodePassword($this->user,$password));
        $this->user->setPasswordUpdatedAt(new \DateTime());
        $this->user->setUpdatedAt(new \DateTime());

        return $this->repository->save($this->user);
    }

    public function usedCodeForPasswordChange()
    {
        $this->user->setIsResetPassword(false);
        $this->user->setResetPasswordCodeSentAt(null);
        $this->user->setResetPasswordAskedAt(null);

        return $this->repository->save($this->user);
    }

    /**
     * @return bool
     */
    public function isResetPasswordCodeValid(): bool
    {
        return $this->isResetPasswordCodeValid;
    }

    /**
     * @param $user
     * @return User[]
     */
    public function findChildren($user)
    {
        return $this->repository->findBy(['parent'=>$user]);
    }

    /**
     * @return User[]
     */
    public function findBusinesses(): array
    {
        return $this->repository->findByRole('ROLE_BUSINESS');
    }
    /**
     * @return User[]
     */
    public function allManagers(): array
    {
        return $this->repository->findByRole('ROLE_MANAGER');
    }

    public function lockUser(User $user)
    {
        $user->setIsActive(false);
        return $this->repository->save($user);
    }

    public function unlockUser(User $user)
    {
        $user->setIsActive(true);
        return $this->repository->save($user);
    }

    public function validPayment(User $client)
    {
        $client->setPaymentReportedAt(new \DateTime());
        $client->setIsFrozen(false);
        return $this->repository->save($client);
    }

    public function invalidPayment(User $client)
    {
        $client->setPaymentReportedAt(null);
        return $this->repository->save($client);
    }

    public function active(User $user): void
    {
        $this->repository->active($user);
    }
}