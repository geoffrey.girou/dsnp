<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JsonRequestAcceptor
{
    private array $data;
    private JsonResponse $jsonResponse;

    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function accept(Request $request): bool
    {
        if (false === $this->supports($request)) {
            $this->jsonResponse = new JsonResponse([
                'message' => 'Unable to handle the request',
            ], 500);
            return false;
        }
        try {
            $this->data = json_decode((string)$request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            return true;
        } catch (\JsonException $exception) {
            $this->jsonResponse = new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
            return false;
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function  supports(Request $request): bool
    {
        return 'json' === $request->getContentType() && $request->getContent();
    }


    public function data()
    {
        return $this->data;
    }

    /**
     * @return JsonResponse
     */
    public function jsonResponse(): JsonResponse
    {
        return $this->jsonResponse;
    }
}