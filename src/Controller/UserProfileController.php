<?php


namespace App\Controller;


use App\Entity\User;
use App\Enum\UserActionEnum;
use App\Presenter\ChildPresenter;
use App\Presenter\ClientPresenter;
use App\Presenter\ProfilePresenter;
use App\Service\CSRFService;
use App\Service\JsonRequestAcceptor;
use App\Service\PersonalDataService;
use App\Service\UserActionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class UserProfileController extends AbstractController
{
    /**
     * @Route("/api/user/profile", name="profile", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function profile(): JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();

        $presenter = $user->isNotAChild() ? new ProfilePresenter($user)  : new ChildPresenter($user);

        return new JsonResponse($presenter->present(),200);
    }

    /**
     * @Route("/api/user/profile", name="update-profile", methods={"PUT"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param PersonalDataService $personalDataService
     * @param UserActionService $userActionService
     * @param CSRFService $cSRFService
     * @return JsonResponse
     */
    public function updateProfile(Request $request,
        JsonRequestAcceptor $acceptor,
        PersonalDataService $personalDataService,
        UserActionService $userActionService,
        CSRFService $cSRFService): JsonResponse
    {
        if(!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }

        $data = $acceptor->data();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        /** @var User $client */
        $client=$this->getUser();

        $personalData = $personalDataService->update(
            $client,
            $data['companyName'],
            $data['firstname'],
            $data['lastname'],
            $data['addressLine1'],
            $data['addressLine2'],
            $data['postalCode'],
            $data['city'],
            $data['phoneNumber']
        );
        $client->setPersonalData($personalData);

        $userActionService->add($client,UserActionEnum::PROFILE);
        $presenter = new ProfilePresenter($client);
        return new JsonResponse($presenter->present(),200);
    }
}