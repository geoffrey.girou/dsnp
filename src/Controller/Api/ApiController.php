<?php

namespace App\Controller\Api;

use App\Presenter\CollectionPresenter;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var int
     */
    public $page;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $offset;

    /**
     * @var Request
     */
    public $request;

    /**
     * ApiController constructor.
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $em
     */
    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em
    ) {
        $this->logger = $logger;
        $this->em = $em;
    }

    /**
     * @param mixed $data
     * @param int $code
     * @return Response
     */
    protected function response($data, int $code = Response::HTTP_OK)
    {
        return new JsonResponse($data,$code);
    }

    /**
     * @param Request $request
     * @return void
     */
    public function init(Request $request)
    {
        $this->request = $request;
        $this->limit = (int) $request->query->get('limit', '50');
        $this->page = (int) $request->query->get('page', '1');
        $this->offset = $this->limit * ($this->page - 1);
    }

    /**
     * @param array $defaultFilters
     * @return array
     */
    public function getFilters($defaultFilters = [])
    {
        $filters = $this->request->query->all();

        return array_merge($defaultFilters, $filters);
    }


    /**
     * Finds entities by a set of criteria.
     *
     * @param ObjectRepository<mixed> $repository
     * @param string $presenterName
     * @param array  $criteria
     * @param array|null $orderBy
     *
     * @return Response The objects.
     */
    public function list($repository, $presenterName, $criteria = [], $orderBy = [])
    {

        $data = $repository->findBy($criteria, $orderBy, $this->limit, $this->offset);
        $count = count($repository->findBy($criteria));

        return $this->response([
            'data'     => (new CollectionPresenter($data,$presenterName))->present(),
            'count'    =>  $count,
            'page_max' => ceil($count/$this->limit),
            'limit'    => $this->limit
        ]);
    }

    /**
     * Find one entity by a set of criteria.
     *
     * @param class-string<mixed> $className
     * @param string $presenterName
     * @param array  $criteria
     *
     * @return Response The objects.
     */
    public function view($className, $presenterName, $criteria = []): Response
    {
        $entity = $this->em->getRepository($className)->findOneBy($criteria);

        if (empty($entity)) {
            throw new NotFoundHttpException((new \ReflectionClass($className))->getShortName().' not found');
        }

        return $this->response((new $presenterName($entity))->present());
    }
}
