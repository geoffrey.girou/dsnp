<?php

namespace App\Controller\Api\PersonalData;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\PersonalData;
use App\Entity\User;
use App\JSONHttpRequest\PersonalData\CreatePersonalDataRequest;
use App\Presenter\PersonalDataPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PersonalDataCreateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/users/{userId}/personal-datas", name="create-user-personal-datas", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function create(
        CreatePersonalDataRequest $request
    ) {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['id'=>$request->userId]);
        if (empty($user)) {
            throw new NotFoundHttpException("L'utilisateur n'a pas été trouvé.");
        }

        $personalData = new PersonalData();
        $personalData->setCreatedAt(new \DateTime());
        $personalData->setCompanyName($request->companyName);
        $personalData->setFirstname($request->firstname);
        $personalData->setLastname($request->lastname);
        $personalData->setAddressLine1($request->addressLine1);
        $personalData->setAddressLine2($request->addressLine2);
        $personalData->setPostalCode($request->postalCode);
        $personalData->setCity($request->city);
        $personalData->setPhoneNumber($request->phoneNumber);
        $personalData->setUser($user);

        $this->em->persist($personalData);
        $this->em->flush();

        $presenter = new PersonalDataPresenter($personalData);
        return $this->response($presenter->present());
    }
}