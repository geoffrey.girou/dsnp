<?php

namespace App\Controller\Api\Manager;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Presenter\CollectionPresenter;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ManagerListController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/managers", name="all-managers", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @param UserService $userService
     * @return JsonResponse
     */
    public function all(UserService $userService): JsonResponse
    {
        $presenter = new CollectionPresenter(
            $userService->allManagers(),
            '\\App\\Presenter\\ManagerPresenter'
        );
        return new JsonResponse($presenter->present(),200);
    }
}