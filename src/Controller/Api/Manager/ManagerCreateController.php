<?php

namespace App\Controller\Api\Manager;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\JSONHttpRequest\Manager\CreateManagerRequest;
use App\Presenter\ManagerPresenter;
use App\Service\PasswordGeneratorService;
use App\Service\WizardService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ManagerCreateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/managers", name="create-manager", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function create(
        CreateManagerRequest $request,
        UserPasswordEncoderInterface $encoder,
        PasswordGeneratorService $passwordGeneratorService,
        WizardService $wizardService
    ) {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['email'=>$request->email]);
        if (!empty($user)) {
            throw new ConflictHttpException("Un gestionnaire avec cet email existe déjà.");
        }
        $manager = new User();
        $manager->setEmail($request->email);
        $manager->setRoles(['ROLE_MANAGER','ROLE_USER','ROLE_ALLOWED_TO_SWITCH']);
        $manager->setCreatedAt(new \DateTime());
        $manager->setUpdatedAt(new \DateTime());
        $manager->setCanUploadDsnFile(true);
        $manager->setCanCreateUser(true);
        $manager->setCanAccessData(true);
        $manager->setIsActive(true);
        $manager->setIsFrozen(false);
        $manager->setPassword($encoder->encodePassword(new User(), $passwordGeneratorService->generate()));

        $wizard = $wizardService->create($manager);
        $wizard->complete();

        $manager->setWizard($wizard);

        $this->em->persist($manager);
        $this->em->flush();

        $presenter = new ManagerPresenter($manager);
        return $this->response($presenter->present());
    }
}