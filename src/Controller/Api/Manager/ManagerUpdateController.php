<?php

namespace App\Controller\Api\Manager;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\JSONHttpRequest\Manager\UpdateManagerRequest;
use App\Presenter\ManagerPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ManagerUpdateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/managers/{id}", name="update-manager", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function update(UpdateManagerRequest $request)
    {
        /** @var User $manager */
        $manager = $this->em->getRepository(User::class)->findOneBy(['id'=>$request->id]);
        if (empty($manager)) {
            throw new NotFoundHttpException("Le gestionnaire n'a pas été trouvé.");
        }
        $manager->setEmail($request->email);
        $manager->setUpdatedAt(new \DateTime());
        $this->em->persist($manager);
        $this->em->flush();
        $presenter = new ManagerPresenter($manager);
        return new JsonResponse($presenter->present(),200);
    }
}