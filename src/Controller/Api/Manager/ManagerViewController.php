<?php

namespace App\Controller\Api\Manager;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\JSONHttpRequest\Manager\ViewManagerRequest;
use App\Presenter\ManagerPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ManagerViewController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/managers/{id}", name="one-manager", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @param ViewManagerRequest $request
     * @return JsonResponse
     */
    public function one(ViewManagerRequest $request): JsonResponse
    {
        $manager = $this->em->getRepository(User::class)->findOneBy(['id'=>$request->id]);
        if (empty($manager)) {
            throw new NotFoundHttpException("Le gestionnaire n'a pas été trouvé.");
        }
        $presenter = new ManagerPresenter($manager);
        return new JsonResponse($presenter->present(),200);
    }
}