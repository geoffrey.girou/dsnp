<?php

namespace App\Controller\Api\Manager;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\JSONHttpRequest\Manager\DeleteManagerRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ManagerDeleteController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/managers/{id}", name="delete-manager", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(DeleteManagerRequest $request)
    {
        $manager = $this->em->getRepository(User::class)->findOneBy(['id'=>$request->id]);
        if (empty($manager)) {
            throw new NotFoundHttpException("Le gestionnaire n'a pas été trouvé.");
        }
        $this->em->remove($manager);
        $this->em->flush();

        return $this->response([]);
    }
}