<?php

namespace App\Controller\Api\Business;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\PersonalData;
use App\Entity\User;
use App\JSONHttpRequest\Business\CreateBusinessRequest;
use App\Presenter\BusinessPresenter;
use App\Service\CSRFService;
use App\Service\EmailGeneratorService;
use App\Service\PasswordGeneratorService;
use App\Service\WizardService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BusinessCreateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/businesses", name="create-business", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function create(
        UserPasswordEncoderInterface $encoder,
        PasswordGeneratorService $passwordGeneratorService,
        EmailGeneratorService $emailGeneratorService,
        WizardService $wizardService
    ) {
        $email = $emailGeneratorService->generate();

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['email'=>$email]);
        if (!empty($user)) {
            throw new ConflictHttpException("Un domaine avec cet email existe déjà.");
        }
        $business = new User();
        $business->setEmail($email);
        $business->setRoles(['ROLE_BUSINESS','ROLE_USER']);
        $business->setCreatedAt(new \DateTime());
        $business->setUpdatedAt(new \DateTime());
        $business->setCanUploadDsnFile(true);
        $business->setCanCreateUser(true);
        $business->setCanAccessData(true);
        $business->setIsActive(true);
        $business->setIsFrozen(false);
        $business->setPassword($encoder->encodePassword(new User(), $passwordGeneratorService->generate()));

        $wizard = $wizardService->create($business);
        $wizard->complete();

        $business->setWizard($wizard);

        $this->em->persist($business);
        $this->em->flush();

        $presenter = new BusinessPresenter($business);
        return $this->response($presenter->present());
    }
}