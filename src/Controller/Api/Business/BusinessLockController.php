<?php

namespace App\Controller\Api\Business;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Presenter\BusinessPresenter;
use App\Service\CSRFService;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BusinessLockController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/businesses/{id}/lock", name="lock-business", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @param UserService $userService
     * @return JsonResponse
     */
    public function lockBusiness(Request $request,
        CSRFService $cSRFService,
        UserService $userService): JsonResponse
    {
        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $business = $userService->findById($request->get('id'));
        if ($business === null) {
            return new JsonResponse([
                'title' => 'Modification de la société',
                'message' => 'Impossible de trouver la société'
            ], 404);
        }

        $business = $userService->lockUser($business);
        $presenter = new BusinessPresenter($business);
        return new JsonResponse($presenter->present(),200);
    }
}