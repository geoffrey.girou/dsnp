<?php

namespace App\Controller\Api\Business;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\Presenter\CollectionPresenter;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BusinessListController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/businesses", name="list-businesses", methods={"GET"})
     * @IsGranted("ROLE_ADMIN","ROLE_MANAGER")
     * @param UserService $userService
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if($user->hasRole("ROLE_ADMIN")) {
        $presenter = new CollectionPresenter(
            $this->em->getRepository(User::class)->findByRole('ROLE_BUSINESS'),
            '\\App\\Presenter\\BusinessPresenter'
        );
        return new JsonResponse($presenter->present(),200);
        }else if($user->hasRole("ROLE_MANAGER")) {
            $presenter = new CollectionPresenter(
                $this->em->getRepository(User::class)->findWithManager($user),
                '\\App\\Presenter\\BusinessPresenter'
            );
        return new JsonResponse($presenter->present(),200);
        }

        throw new NotFoundHttpException();
    }
}