<?php

namespace App\Controller\Api\Business;

use App\Presenter\BusinessPresenter;
use App\Presenter\ClientPresenter;
use App\Service\CSRFService;
use App\Service\JsonRequestAcceptor;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ValidBusinessController
{
    /**
     * @Route("/api/businesses/{id}/valid", name="valid-business", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @return JsonResponse
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function validBusiness(Request $request,
        CSRFService $cSRFService,
        UserService $userService): JsonResponse
    {
        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $business = $userService->findById($request->get('id'));
        if ($business === null) {
            return new JsonResponse([
                'title' => 'Modification de la société',
                'message' => 'Impossible de trouver la société'
            ], 404);
        }

        $business = $userService->validPayment($business);
        $presenter = new BusinessPresenter($business);
        return new JsonResponse($presenter->present(),200);
    }
}