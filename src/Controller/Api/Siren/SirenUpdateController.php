<?php


namespace App\Controller\Api\Siren;


use App\Action\AddSirensAction;
use App\Action\RemoveSirensAction;
use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Siren;
use App\Entity\User;
use App\JSONHttpRequest\Siren\SirenUpdateRequest;
use App\Presenter\SirenPresenter;
use App\Repository\SirenRepository;
use App\Service\Dsnpapi\DsnpapiSirenCollectionService;
use App\Service\SirenAccessService;
use App\Service\SirenService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SirenUpdateController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/sirens", name="update-sirens", methods={"PUT"})
     * @IsGranted("ROLE_BUSINESS")
     * @param SirenUpdateRequest $request
     * @param SirenService $sirenService
     * @param DsnpapiSirenCollectionService $dsnpapiSirenCollectionService
     * @param SirenAccessService $sirenAccessService
     * @return Response
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function update(SirenUpdateRequest $request,
        SirenService $sirenService,
        DsnpapiSirenCollectionService $dsnpapiSirenCollectionService,
        SirenAccessService $sirenAccessService
    ): JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();

        /** @var Siren $sirenToUpdate */
        $sirenToUpdate = $this->em->getRepository(Siren::class)->findOneBy(['id'=>$request->id]);

        $messages = ['message' => 'validation_failed', 'errors' => []];

        if ($sirenToUpdate === null) {
            $messages['errors'][] = [
                'property' => 'id',
                'value' => $request->id,
                'message' => "L'entreprise avec l'identifiant ".$request->id." n'a pas été trouvée.",
            ];
            $response = new JsonResponse($messages, 409);
            return $response->send();
        }

        if($sirenToUpdate->getNumber()===$request->number) {
            // Update siren first
            $updatedSiren = $sirenService->update(
                $sirenToUpdate,
                $request->number,
                $request->maxPersonnelNumbers,
                $request->name
            );

            // Update api
            if($user->getDsnpData()!=null) {
                $dsnpapiSirenCollectionService->addSirens($user->getDsnpData()->getUuid(),[$updatedSiren]);
            }

            $presenter = new SirenPresenter($updatedSiren);
            return new JsonResponse($presenter->present(), 200);
        }else {
            $alreadyExistsSirenForNewNumber = $sirenService->oneByNumber($request->number);
            if ($alreadyExistsSirenForNewNumber != null) {
                $messages['errors'][] = [
                    'property' => 'number',
                    'value' => $request->number,
                    'message' => 'Le numéro SIREN ' . $request->number . ' est déjà utilisé.',
                ];

                $response = new JsonResponse($messages, 409);
                return $response->send();
            }

            // Add siren first
            $newSiren = $sirenService->add(
                $user,
                $request->number,
                $request->maxPersonnelNumbers,
                $request->name
            );

            // Add siren in dsnpapi for user and children and sirenaccess for children only
            if($user->getDsnpData()!==null) {
                $action = new AddSirensAction();
                $action->process($user,
                    $dsnpapiSirenCollectionService,
                    $sirenAccessService,
                    $newSiren,
                    $sirenToUpdate
                );
            }

            // Remove siren in db will remove children siren access by cascade
            $sirenService->delete($sirenToUpdate->getId());

            // Remove siren in dsnpapi for current user and all children
            if($user->getDsnpData()!==null) {
                $action = new RemoveSirensAction($dsnpapiSirenCollectionService);
                $action->process($user,
                    [$sirenToUpdate]);
            }

            $response = $this->view(Siren::class,SirenPresenter::class,['id'=> $newSiren->getId()]);
            return $response->send();
        }
    }
}