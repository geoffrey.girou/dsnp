<?php

namespace App\Controller\Api\Siren;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Siren;
use App\Entity\User;
use App\Presenter\CollectionPresenter;
use App\Presenter\SirenPresenter;
use App\Service\DsnFileDeclarationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SirenListController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/sirens", name="all-sirens", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function all(Request $request, DsnFileDeclarationService $dsnFileDeclarationService): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $limit = 1000;
        if ($user->hasRole('ROLE_BUSINESS')) {
            $integrated =
                $request->query->get('integrated') !== null &&
                $request->get('integrated') === "true";
            $sirens = $this->em->getRepository(Siren::class)->findBy(['user' => $user], ['number' => 'DESC'],
                $limit);
            if ($integrated === true) {
                $sirens = $dsnFileDeclarationService->getSirensIntegrated($sirens);
            }
        } else {
            $sirens = $this->em->getRepository(Siren::class)->findWithAccess(['user' => $user], ['number' => 'DESC'],
                $limit);
        }

        return $this->response([
            'data' => (new CollectionPresenter($sirens, SirenPresenter::class))->present(),
            'count' => sizeof($sirens),
            'page_max' => ceil(sizeof($sirens) / $limit),
            'limit' => $limit
        ]);
    }
}