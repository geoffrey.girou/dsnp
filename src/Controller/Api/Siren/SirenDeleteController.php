<?php

namespace App\Controller\Api\Siren;

use App\Action\DeleteDsnFileAction;
use App\Action\RemoveSirensAction;
use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\DsnFile;
use App\Entity\GroupAccess;
use App\Entity\Siren;
use App\Entity\User;
use App\Model\Group;
use App\Repository\Dsnpapi\GroupDsnpapiRepository;
use App\Service\CSRFService;
use App\Service\Group\SirenInGroupService;
use App\Service\SirenService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SirenDeleteController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/sirens/{id}", name="delete-sirens", methods={"DELETE"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @param SirenService $sirenService
     * @param GroupDsnpapiRepository $groupRepository
     * @param SirenInGroupService $sirenInGroupService
     * @param RemoveSirensAction $removeSirensAction
     * @param DeleteDsnFileAction $deleteDsnFileAction
     * @return JsonResponse
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function delete(
        Request $request,
        CSRFService $cSRFService,
        SirenService $sirenService,
        GroupDsnpapiRepository $groupRepository,
        SirenInGroupService $sirenInGroupService,
        RemoveSirensAction $removeSirensAction,
        DeleteDsnFileAction $deleteDsnFileAction,
        GroupDsnpapiRepository $groupDsnpapiRepository
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));
        /** @var Siren $siren */
        $siren = $this->em->getRepository(Siren::class)->findOneBy(['id' => $request->get('id'), 'user' => $user]);

        if (empty($siren)) {
            throw new NotFoundHttpException((new \ReflectionClass(Siren::class))->getShortName() . ' not found');
        }

        $dsnfilesForDeletion = $this->em->getRepository(DsnFile::class)->findBySiren($siren->getNumber());
        $groupModels = $groupRepository->listWithDetails($user->getFinalParent()->getDsnpData()->getUuid());
        $groups = $sirenInGroupService->verify($groupModels, $siren);

        if ($user->getDsnpData() !== null) {
            foreach ($dsnfilesForDeletion as $dsnfileId) {
                $deleteDsnFileAction->process($user,
                    $this->em->getRepository(DsnFile::class)->findOneBy(['id' => $dsnfileId[1]]), true);
            }
            // UPDATE GROUPS
            /** @var Group $group */
            foreach ($groups['multiple'] as $group) {
                $groupDsnpapiRepository->deleteSirets(
                    $user->getDsnpData()->getUuid(),
                    $group->getId(),
                    [
                        [
                            'siren' => strval($siren->getNumber())
                        ]
                    ]
                );
            }
            // DELETE GROUP
            foreach ($groups['one'] as $group) {
                $groupDsnpapiRepository->deleteSirets(
                    $user->getDsnpData()->getUuid(),
                    $group->getId(),
                    [
                        [
                            'siren' => strval($siren->getNumber())
                        ]
                    ]
                );
                // REMOVE GROUPACCESS FOR CHILDS
                foreach ($user->getChildren() as $child) {
                    $groupAccessList = $this->em->getRepository(GroupAccess::class)->findBy([
                        'user' => $child,
                        'dsnpapiGroupId' => $group->getId(),
                        'name' => $group->getDescription()
                    ]);
                    foreach ($groupAccessList as $groupAccess) {
                        $this->em->getRepository(GroupAccess::class)->delete($groupAccess);
                    }
                }
            }

            // DELETE SIREN
            $removeSirensAction->process($user,
                [$siren]);
        }

        // siren is removed and sirenAccesses as well with
        // on cascade entity effect
        $sirenService->delete($siren->getId());

        return new JsonResponse([], 200);
    }
}