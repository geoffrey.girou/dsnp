<?php

namespace App\Controller\Api\Siren;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\DsnFile;
use App\Entity\Siren;
use App\Entity\User;
use App\Repository\Dsnpapi\GroupDsnpapiRepository;
use App\Service\Group\SirenInGroupService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SirenToDeleteController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/sirens/{id}/todelete", name="one-siren-todelete", methods={"GET"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param GroupDsnpapiRepository $groupRepository
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function toDelete(Request $request, GroupDsnpapiRepository $groupRepository, SirenInGroupService $sirenInGroupService)
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Siren $siren */
        $siren = $this->em->getRepository(Siren::class)->findOneBy(['user' => $user, 'id' => $request->get('id')]);

        if (empty($siren)) {
            throw new NotFoundHttpException((new \ReflectionClass(Siren::class))->getShortName() . ' not found');
        }

        $dsnFileCountForDeletion = $this->em->getRepository(DsnFile::class)->countBySiren($siren->getNumber());

        $groupModels = $groupRepository->listWithDetails($user->getFinalParent()->getDsnpData()->getUuid());

        $groups = $sirenInGroupService->verify($groupModels,$siren);
        $groupMessagesForDeletion = [];
        foreach ($groups['one'] as $group) {
            $groupMessagesForDeletion[] = 'Le groupement ' . $group->getDescription() . ' sera supprimé.';
        }
        foreach ($groups['multiple'] as $group) {
            $groupMessagesForDeletion[] = 'Le groupement ' . $group->getDescription() . ' sera mis à jour.';
        }

        return $this->response(['count' => intval($dsnFileCountForDeletion), 'messages' => $groupMessagesForDeletion]);
    }
}