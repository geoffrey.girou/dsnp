<?php

namespace App\Controller\Api\Siren;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Siren;
use App\Entity\User;
use App\JSONHttpRequest\Siren\SirenCreateRequest;
use App\Presenter\SirenPresenter;
use App\Service\CSRFService;
use App\Service\Dsnpapi\DsnpapiSirenCollectionService;
use App\Service\SirenService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SirenCreateController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/sirens", name="create-sirens", methods={"POST"})
     * @IsGranted("ROLE_BUSINESS")
     * @param SirenCreateRequest $request
     * @param CSRFService $cSRFService
     * @param SirenService $sirenService
     * @param DsnpapiSirenCollectionService $dsnpapiSirenCollectionService
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function create(SirenCreateRequest $request,
        SirenService $sirenService,
        DsnpapiSirenCollectionService $dsnpapiSirenCollectionService
    )
    {
        /** @var Siren $sirenToCreate */
        $sirenToCreate = $this->em->getRepository(Siren::class)->findOneBy(['number'=>$request->number]);

        $messages = ['message' => 'validation_failed', 'errors' => []];
        if ($sirenToCreate != null) {
            $messages['errors'][] = [
                'property' => 'number',
                'value' => $request->number,
                'message' => 'Le numéro SIREN ' . $request->number . ' est déjà utilisé.',
            ];

            $response = new JsonResponse($messages, 409);
            return $response->send();
        }

        /** @var User $user */
        $user=$this->getUser();

        $siren = $sirenService->add(
            $user,
            $request->number,
            $request->maxPersonnelNumbers,
            $request->name
        );

        if($user->getDsnpData()!==null) {
            $dsnpapiSirenCollectionService->addSirens(
                $user->getDsnpData()->getUuid(),
                [$siren]
            );
        }

        $response = $this->view(Siren::class,SirenPresenter::class,['id'=> $siren->getId()]);
        return $response->send();
    }
}