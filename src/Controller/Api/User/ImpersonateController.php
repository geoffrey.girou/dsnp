<?php

namespace App\Controller\Api\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ImpersonateController extends AbstractController
{
    /**
     * @Route("/api/impersonate", name="impersonate", methods={"GET"})
     * @IsGranted("ROLE_ADMIN", "ROLE_MANAGER", "ROLE_BUSINESS")
     */
    public function impersonate(Security $security): JsonResponse
    {
        // switch is triggered automatically by the security component
        return new JsonResponse($security->isGranted('ROLE_PREVIOUS_ADMIN'));
    }
}