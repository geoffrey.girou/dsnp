<?php

namespace App\Controller\Api\Group;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\Repository\Dsnpapi\GroupDsnpapiRepository;
use App\Service\CSRFService;
use App\Service\GroupAccessService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GroupDeleteController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/groups/{id}", name="delete-group", methods={"DELETE"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @param GroupDsnpapiRepository $dsnpapiGroupService
     * @param GroupAccessService $groupAccessService
     * @return JsonResponse
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function delete(Request $request,
        CSRFService $cSRFService,
        GroupDsnpapiRepository $dsnpapiGroupService,
        GroupAccessService $groupAccessService
    ): JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        //TODO verifier que le group appartient à l'utilisateur
        $dsnpapiGroupService->delete(
            $user->getDsnpData()->getUuid(),
            $request->get('id')
        );

        $groupAccessService->deleleFromGroupApiId(
            $user->getDsnpData()->getUuid(),
            $request->get('id')
        );

        return new JsonResponse([],200);
    }
}