<?php

namespace App\Controller\Api\Group;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\Model\Group;
use App\Model\SiretFactory;
use App\Presenter\GroupPresenter;
use App\Repository\Dsnpapi\GroupDsnpapiRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GroupOneController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/group/{id}", name="one-group", methods={"GET"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param GroupDsnpapiRepository $dsnpapiGroupService
     * @return JsonResponse
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function one(Request $request, GroupDsnpapiRepository $dsnpapiGroupService): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $uuid = $user->getFinalParent()->getDsnpData()->getUuid();

        $groupsResponse = $dsnpapiGroupService->one($uuid,$request->get('id'));

        $groupModel = new Group($groupsResponse['id'],$groupsResponse['description'], $groupsResponse['time']);
        foreach ($groupsResponse['sirets'] as $siret) {
            $groupModel->addSiret(SiretFactory::create($siret['nic'],$siret['siren']));
        }

        $presenter = new GroupPresenter($groupModel);

        return new JsonResponse($presenter->present(),200);
    }
}