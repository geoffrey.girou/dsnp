<?php


namespace App\Controller\Api\Group;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\Model\Group;
use App\Model\SiretFactory;
use App\Presenter\GroupPresenter;
use App\Repository\Dsnpapi\GroupDsnpapiRepository;
use App\Service\CSRFService;
use App\Service\DsnFileDeclarationService;
use App\Service\JsonRequestAcceptor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GroupUpdateController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/groups", name="update-group", methods={"PUT"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @param JsonRequestAcceptor $acceptor
     * @param GroupDsnpapiRepository $dsnpapiGroupService
     * @param DsnFileDeclarationService $dsnFileDeclarationService
     * @return JsonResponse
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function update(Request $request,
        CSRFService $cSRFService,
        JsonRequestAcceptor $acceptor,
        GroupDsnpapiRepository $dsnpapiGroupService,
        DsnFileDeclarationService $dsnFileDeclarationService
    ): JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();

        if(!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $data = $acceptor->data();

        $uuid = $user->getDsnpData()->getUuid();
        $groupsResponse = $dsnpapiGroupService->one($uuid,$data['id']);

        $groupModel = new Group($groupsResponse['id'],$groupsResponse['description'], $groupsResponse['time']);
        foreach ($groupsResponse['sirets'] as $siret) {
            $groupModel->addSiret(SiretFactory::create($siret['nic'],$siret['siren']));
        }

        $sirets = [];

        /**
         * Si la données siren contient "Toutes les entreprises" alors
         * le groupement contient toutes les entreprises et tous les sirets
         * 1 - si le siret est vide alors
         *     - chercher tous les siret du siren
         * 2 - si le siret n est pas vide alors
         *    - chercher le siret du siren
         * 3 - ajouter au tableau le siret s il n est pas deja dedans
         */
        foreach ($data['sirets'] as $siret) {
            if($siret['siren']=="Toutes les entreprises") {
                foreach ($dsnFileDeclarationService->getGrouped($user) as $declaration) {
                    $siretsObject =  SiretFactory::createFromNumber($declaration->getSiret());
                    $sirets[] = [
                        'nic' => $siretsObject->nic(),
                        'siren' => $siretsObject->siren()
                    ];
                }
            }else {
                if($siret['nic']==="Tous les établissements") {
                    foreach ($siret['nics'] as $nicsAvailable) {
                        $sirets[] = [
                            'nic' => $nicsAvailable['nic'],
                            'siren' => $nicsAvailable['siren']
                        ];
                    }
                }else {
                    $sirets[] = [
                        'nic' => $siret['nic'],
                        'siren' => $siret['siren']
                    ];
                }
            }
        }

        $siretToDelete = [];

        foreach ($groupModel->getSirets() as $oldSirets) {
            $found = false;
            foreach ($sirets as $siret) {
                if($siret['siren']==$oldSirets->siren() && $siret['nic'] == $oldSirets->nic()) {
                    $found = true;
                }
            }
            if(!$found) {
                $siretToDelete[] = [
                    'nic' => $oldSirets->nic(),
                    'siren' => $oldSirets->siren()
                ];
            }
        }

        $groupResponse = $dsnpapiGroupService->update(
            $uuid,
            $data['id'],
            $data['name'],
            $sirets
        );

        if(sizeof($siretToDelete)!=0) {
            $dsnpapiGroupService->deleteSirets(
                $uuid,
                $data['id'],
                $siretToDelete
            );
        }

        $group = new Group($groupResponse['ID'],$groupResponse['description'],$groupResponse['time']);
        foreach ($groupResponse['sirets'] as $siret) {
            $group->addSiret(SiretFactory::create($siret['nic'],$siret['siren']));
        }

        $presenter = new GroupPresenter($group);
        return new JsonResponse($presenter->present(), 200);
    }
}