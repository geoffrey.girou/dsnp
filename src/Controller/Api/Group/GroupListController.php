<?php

namespace App\Controller\Api\Group;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\User;
use App\Presenter\CollectionPresenter;
use App\Repository\Dsnpapi\GroupDsnpapiRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class GroupListController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/groups", name="all-groups", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @param GroupDsnpapiRepository $groupRepository
     * @return JsonResponse
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function all(GroupDsnpapiRepository $groupRepository): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if($user->getFinalParent()->getDsnpData()===null) {
            return new JsonResponse([],200);
        }

        $groups = $groupRepository->listWithDetails($user->getFinalParent()->getDsnpData()->getUuid());

        if($user->isAChild()) {
            $groupAccesses = $user->getGroupAccesses();
            $childGroups = [];

            foreach ($groups as $group) {
                $isGroupAutorized = false;
                foreach ($groupAccesses as $groupAccess) {
                    if($groupAccess->getDsnpapiGroupId()===$group->getId()) {
                        $isGroupAutorized = true;
                    }
                }
                if($isGroupAutorized) {
                    $childGroups[] = $group;
                }
            }

            $presenter = new CollectionPresenter(
                $childGroups,
                '\\App\\Presenter\\GroupPresenter'
            );

            return new JsonResponse($presenter->present(),200);
        }

        $presenter = new CollectionPresenter(
            $groups,
            '\\App\\Presenter\\GroupPresenter'
        );

        return new JsonResponse($presenter->present(),200);
    }
}