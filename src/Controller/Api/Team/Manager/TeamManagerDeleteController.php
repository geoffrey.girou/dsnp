<?php

namespace App\Controller\Api\Team\Manager;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\Entity\User;
use App\JSONHttpRequest\Team\Manager\DeleteTeamManagerRequest;
use App\Presenter\TeamPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TeamManagerDeleteController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/teams/{teamId}/managers/{managerId}", name="delete-team-manager", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(DeleteTeamManagerRequest $request)
    {
        /** @var Team $team */
        $team = $this->em->getRepository(Team::class)->findOneBy(['id'=>$request->teamId]);
        if (empty($team)) {
            throw new NotFoundHttpException("L'équipe n'a pas été trouvée.");
        }
        /** @var User $manager */
        $manager = $this->em->getRepository(User::class)->findOneBy(['id'=>$request->managerId]);
        if (empty($manager)) {
            throw new NotFoundHttpException("Le gestionnaire n'a pas été trouvée.");
        }

        $team->removeManager($manager);
        $this->em->persist($team);
        $this->em->flush();

        return $this->response("");
    }
}