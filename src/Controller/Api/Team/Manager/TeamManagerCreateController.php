<?php

namespace App\Controller\Api\Team\Manager;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\Entity\User;
use App\JSONHttpRequest\Team\Manager\CreateTeamManagerRequest;
use App\Presenter\TeamPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TeamManagerCreateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/teams/{teamId}/managers", name="create-team-manager", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function create(CreateTeamManagerRequest $request)
    {
        /** @var Team $team */
        $team = $this->em->getRepository(Team::class)->findOneBy(['id'=>$request->teamId]);
        if (empty($team)) {
            throw new NotFoundHttpException("L'équipe n'a pas été trouvée.");
        }
        /** @var User $manager */
        $manager = $this->em->getRepository(User::class)->findOneBy(['id'=>$request->managerId]);
        if (empty($manager)) {
            throw new NotFoundHttpException("Le gestionnaire n'a pas été trouvé.");
        }

        $team->addManager($manager);
        $this->em->persist($team);
        $this->em->flush();

        $presenter = new TeamPresenter($team);
        return $this->response($presenter->present());
    }
}