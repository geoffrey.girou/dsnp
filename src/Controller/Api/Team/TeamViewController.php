<?php

namespace App\Controller\Api\Team;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\JSONHttpRequest\Team\ViewTeamRequest;
use App\Presenter\TeamPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TeamViewController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/teams/{id}", name="one-team", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     * @param ViewTeamRequest $request
     * @return JsonResponse
     */
    public function one(ViewTeamRequest $request): JsonResponse
    {
        $team = $this->em->getRepository(Team::class)->findOneBy(['id'=>$request->id]);
        if (empty($team)) {
            throw new NotFoundHttpException("L'équipe n'a pas été trouvée.");
        }
        $presenter = new TeamPresenter($team);
        return new JsonResponse($presenter->present(),200);
    }
}