<?php

namespace App\Controller\Api\Team;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\Entity\User;
use App\Presenter\CollectionPresenter;
use App\Presenter\TeamPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TeamListController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/teams", name="all-admin-teams", methods={"GET"})
     * @IsGranted("ROLE_ADMIN","ROLE_MANAGER")
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if($user->hasRole("ROLE_ADMIN")) {
            $presenter = new CollectionPresenter(
                $this->em->getRepository(Team::class)->findAll(),
                '\\App\\Presenter\\TeamPresenter'
            );
            return new JsonResponse($presenter->present(),200);
        }else if($user->hasRole("ROLE_MANAGER")) {
            $presenter = new CollectionPresenter(
                $this->em->getRepository(Team::class)->findWithManager($user),
                '\\App\\Presenter\\TeamPresenter'
            );
            return new JsonResponse($presenter->present(),200);
        }

        throw new NotFoundHttpException();
    }
}