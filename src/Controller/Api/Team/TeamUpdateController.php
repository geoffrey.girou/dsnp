<?php

namespace App\Controller\Api\Team;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\JSONHttpRequest\Team\UpdateTeamRequest;
use App\Presenter\TeamPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TeamUpdateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/teams/{id}", name="update-team", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function update(UpdateTeamRequest $request)
    {
        $team = $this->em->getRepository(Team::class)->findOneBy(['id'=>$request->id]);
        if (empty($team)) {
            throw new NotFoundHttpException("L'équipe n'a pas été trouvée.");
        }
        $team->setDescription($request->description);
        $team->setUpdatedAt(new \DateTime());

        $this->em->persist($team);
        $this->em->flush();

        $presenter = new TeamPresenter($team);
        return $this->response($presenter->present());
    }
}