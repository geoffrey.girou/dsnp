<?php

namespace App\Controller\Api\Team;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\JSONHttpRequest\Team\DeleteTeamRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TeamDeleteController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/teams/{id}", name="delete-team", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(DeleteTeamRequest $request)
    {
        $team = $this->em->getRepository(Team::class)->findOneBy(['id'=>$request->id]);
        if (empty($team)) {
            throw new NotFoundHttpException("L'équipe n'a pas été trouvée.");
        }
        $this->em->remove($team);
        $this->em->flush();

        return $this->response(null);
    }
}