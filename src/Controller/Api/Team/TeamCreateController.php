<?php

namespace App\Controller\Api\Team;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\JSONHttpRequest\Team\CreateTeamRequest;
use App\Presenter\TeamPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

class TeamCreateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/teams", name="create-team", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function create(CreateTeamRequest $request)
    {
        $team = new Team();
        $team->setDescription($request->description);
        $team->setCreatedAt(new \DateTime());
        $team->setUpdatedAt(new \DateTime());

        $this->em->persist($team);
        $this->em->flush();

        $presenter = new TeamPresenter($team);
        return $this->response($presenter->present());
    }
}