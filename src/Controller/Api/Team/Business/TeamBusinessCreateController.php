<?php

namespace App\Controller\Api\Team\Business;

use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\Team;
use App\Entity\User;
use App\JSONHttpRequest\Team\Business\CreateTeamBusinessRequest;
use App\Presenter\TeamPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TeamBusinessCreateController extends ApiController implements ApiAccessController
{
    /**
     * @Route(path="/api/teams/{teamId}/businesses", name="create-team-business", methods={"POST"})
     * @IsGranted("ROLE_ADMIN","ROLE_MANAGER")
     */
    public function create(CreateTeamBusinessRequest $request)
    {
        /** @var Team $team */
        $team = $this->em->getRepository(Team::class)->findOneBy(['id'=>$request->teamId]);
        if (empty($team)) {
            throw new NotFoundHttpException("L'équipe n'a pas été trouvée.");
        }
        /** @var User $business */
        $business = $this->em->getRepository(User::class)->findOneBy(['id'=>$request->businessId]);
        if (empty($business)) {
            throw new NotFoundHttpException("Le domaine n'a pas été trouvé.");
        }

        $team->addBusiness($business);
        $this->em->persist($team);
        $this->em->flush();

        $presenter = new TeamPresenter($team);
        return $this->response($presenter->present());
    }
}