<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;

interface ApiAccessController
{
    /**
     * @param Request $request
     * @return void
     */
    public function init(Request $request);
}
