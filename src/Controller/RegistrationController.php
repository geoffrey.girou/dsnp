<?php


namespace App\Controller;

use App\Entity\User;
use App\JSONHttpRequest\POSTOauthRegister;
use App\JSONHttpRequest\POSTRegister;
use App\Service\CommandLauncherService;
use App\Service\PersonalDataService;
use App\Service\UserService;
use App\Service\WizardService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", methods={"POST"})
     * @param POSTRegister $request
     * @param UserService $userService
     * @param PersonalDataService $personalDataService
     * @param WizardService $wizardService
     * @param CommandLauncherService $commandLauncherService
     * @return JsonResponse
     */
    public function register(
        POSTRegister $request,
        UserService $userService,
        PersonalDataService $personalDataService,
        WizardService $wizardService,
        CommandLauncherService $commandLauncherService
    ) : JsonResponse
    {
        $request->verifyMailExists($userService);
        $user = $userService->createClient($request->email, $request->password);

        $user = $personalDataService->addPersonalData($user,
            $request->companyName,
            $request->firstname,
            $request->lastname,
            $request->addressLine1,
            $request->addressLine2,
            $request->postalCode,
            $request->phoneNumber,
            $request->city
        );

        $user = $userService->save($user);
        $wizard = $wizardService->create($user);
        $wizardService->save($wizard);

        $commandLauncherService->executeWithoutOutput('app:registration-mail-confirmation');
        return new JsonResponse($user->getEmail(), 200);
    }

    /**
     * @Route("/oauth/register", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param POSTOauthRegister $request
     * @param UserService $userService
     * @param PersonalDataService $personalDataService
     * @param WizardService $wizardService
     * @return JsonResponse
     */
    public function oauthRegister(
        POSTOauthRegister $request,
        UserService $userService,
        PersonalDataService $personalDataService,
        WizardService $wizardService
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $user = $personalDataService->addPersonalData($user,
            $request->companyName,
            $request->firstname,
            $request->lastname,
            $request->addressLine1,
            $request->addressLine2,
            $request->postalCode,
            $request->phoneNumber,
            $request->city
        );

        $user->setIsActive(1);

        $user = $userService->save($user);
        $wizard = $wizardService->create($user);
        $wizardService->save($wizard);

        return new JsonResponse(null, 200);
    }
}