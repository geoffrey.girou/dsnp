<?php


namespace App\Controller;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountActivationController extends AbstractController
{
    /**
     * @Route("/activation", name="activation", methods={"GET"})
     * @param Request $request
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function activation(
        Request $request,
        UserService $userService
    )
    {
        $code = $request->get('code');
        $email = $request->get('email');
        $user = $userService->findByEmail($email);
        if($user===null) {
            return new Response("Utilisateur introuvable.",404);
        }

        if($user->getActivationCode()!==$code) {
            return new Response("Code incorrect.",400);
        }

        $userService->active($user);

        return $this->redirect($_SERVER['APP_LOGIN_URL'].'/'.$user->getEmail());
    }
}