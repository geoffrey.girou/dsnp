<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="index")
 * @return Response
 */
class RenderController extends AbstractController
{
    /**
     * @Route("/{vueRouting}", requirements={"vueRouting"="^(?!api|login|logout|refresh|connect|oauth|activation|_(profiler|wdt)).*"}, name="action")
     * @return Response
     */
    public function action(): Response
    {
        return $this->render('base.html.twig');
    }
}