<?php

namespace App\Controller;

use App\Entity\User;
use App\Image\PngLogo;
use App\Service\CSRFService;
use App\Service\Dsnpapi\DsnpapiLogoService;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LogoController extends AbstractController
{

    /**
     * @Route("/api/checklogo", name="check-logo", methods={"POST"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @return JsonResponse
     */
    public function check(Request $request,
        CSRFService $cSRFService): JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->all()['file'];

        $logo = new PngLogo();
        $logo->createFromUploadedFile($uploadedFile);

        if(!$logo->isValid()) {
            return new JsonResponse([
                'title'=>'Transmission de logo',
                'message'=>'Opération annulée. Merci de vérifier l\'extension de votre fichier.'
            ],403);
        }

        return new JsonResponse(['ok'=>true],200);
    }

    /**
     * @Route("/api/logo", name="uploadlogo", methods={"POST"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @return JsonResponse
     */
    public function send(Request $request,
        CSRFService $cSRFService,
    UserService $userService,
    DsnpapiLogoService $logoService): JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->all()['files'][0];

        $logo = new PngLogo();
        $logo->createFromUploadedFile($uploadedFile);

        if(!$logo->isValid()) {
            return new JsonResponse([
                'title'=>'Transmission de logo',
                'message'=>'Opération annulée. Merci de vérifier l\'extension de votre fichier.'
            ],403);
        }

        $logoName = uniqid().'.png';
        $logo->move(__DIR__.'/../../public/logo',$logoName);

        $size = getimagesize(__DIR__.'/../../public/logo/'.$logoName);
        if($size[0]!=110&&$size[0]!=42) {
            unlink(__DIR__.'/../../public/logo/'.$logoName);
            return new JsonResponse([
                'title'=>'Transmission de logo',
                'message'=>'Opération annulée. Merci de transmettre une image aux dimensions 110x42 pixels.'
            ],403);
        }

        $logoService->add($user->getDsnpData()->getUuid(),__DIR__.'/../../public/logo/'.$logoName);
        $user->setLogo($logoName);
        $userService->save($user);

        return new JsonResponse([
            'logo'=>'../../../logo/'.$user->getLogo()
        ],200);
    }

    /**
     * @Route("/api/logo", name="deletelogo", methods={"DELETE"})
     * @IsGranted("ROLE_BUSINESS")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @return JsonResponse
     */
    public function delete(Request $request,
        CSRFService $cSRFService,
    UserService $userService,
        DsnpapiLogoService $logoService): JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        unlink(__DIR__.'/../../public/logo/'.$user->getLogo());

        $logoService->delete($user->getDsnpData()->getUuid());
        $user->setLogo(null);
        $userService->save($user);

        return new JsonResponse(null,200);
    }
}