<?php


namespace App\Controller;


use App\Entity\User;
use App\Mailer\Mailer;
use App\Repository\UserRepository;
use App\Service\CommandLauncherService;
use App\Service\CSRFService;
use App\Service\JsonRequestAcceptor;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserPasswordController extends AbstractController
{
    /**
     * @Route("/api/password/code", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @param CommandLauncherService $commandLauncherService
     * @param UserService $userService
     * @return JsonResponse
     */
    public function passwordCode(
        Mailer $mailer,
        UserService $userService,
        UserRepository $userRepository
    )
    {
        /** @var User $user */
        $user=$this->getUser();
        $userService->askPasswordReset($user);

        $code = uniqid();
        $now = new \DateTime();

        $resetPasswordCodeSentAt = $user->getResetPasswordCodeSentAt();
        if($resetPasswordCodeSentAt!=null) {
            $interval = date_diff($resetPasswordCodeSentAt,$now);
            if($interval->format("%H")!=="00") {
                $mailer->sendEmail(
                    'Changement de mot de passe',
                    $user->getEmail(),
                    'code.reset.password.html.twig',
                    [
                        'code'=>$code
                    ]
                );
        
                $userRepository->resetPasswordCodeSent($user,$code);
            }
        }else {
            $mailer->sendEmail(
                'Changement de mot de passe',
                $user->getEmail(),
                'code.reset.password.html.twig',
                [
                    'code'=>$code
                ]
            );
    
            $userRepository->resetPasswordCodeSent($user,$code);
        }

        return new JsonResponse(null,200);
    }

    /**
     * @Route("/api/password/code", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param UserService $userService
     * @return JsonResponse
     */
    public function checkCode(Request $request,
        CSRFService $cSRFService,
        JsonRequestAcceptor $acceptor,
        UserService $userService)
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        if(!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }
        $data = $acceptor->data();
        $isCodeValid = $userService->checkPasswordResetCode($user,$data['code']);

        if(!$isCodeValid) {
            return new JsonResponse([
                'title'=>'Mot de passe',
                'message'=>'Le code fourni n\est pas valide',
            ],403);
        }

        return new JsonResponse([],200);
    }
    /**
     * @Route("/api/password/change", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param UserService $userService
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function changePassword(Request $request, CSRFService $cSRFService, JsonRequestAcceptor $acceptor,
        UserService $userService, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        if(!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }
        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $response = $this->checkCode($request,$cSRFService,$acceptor,$userService);

        if(!$userService->isResetPasswordCodeValid()) {
            return $response;
        }

        $data = $acceptor->data();
        $userService->changePassword($data['password'],$encoder);
        $userService->usedCodeForPasswordChange();

        return new JsonResponse([],200);
    }
}