<?php


namespace App\Controller;


use App\Entity\User;
use App\Enum\UserActionEnum;
use App\Presenter\UserPresenter;
use App\Service\CSRFService;
use App\Service\UserActionService;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\GithubClient;
use KnpU\OAuth2ClientBundle\Client\Provider\GoogleClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class SessionController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     * @param Request $request
     * @param UserActionService $userActionService
     * @param CSRFService $cSRFService
     * @return JsonResponse|RedirectResponse
     */
    public function login(Request $request, UserActionService $userActionService,
    CSRFService $cSRFService)
    {
        /** @var User $user */
        $user=$this->getUser();
        if(empty($user)) {
            return new JsonResponse('no session',401);
        }

        if ($user->getIsFrozen()===true) {
            return new JsonResponse([
                'title'=>'Connexion',
                'message'=>'Ce compte a été bloqué.'
            ],403);
        }else if($user->getIsActive()===true) {
            $userActionService->add($user,UserActionEnum::CONNECTION);

            $presenter = new UserPresenter($user);
            $presentedData = $presenter->present();
            $presentedData['token'] = $cSRFService->generate();
            return new JsonResponse($presentedData);
        }else {
            return new JsonResponse([
                'title'=>'Connexion',
                'message'=>'Ce compte n\'est pas actif. Vérifiez votre boîte mail.'
            ],403);
        }
    }

    /**
     * @Route("/connect/{service}", name="connect")
     * @param ClientRegistry $clientRegistry
     * @return JsonResponse
     */
    public function connect(Request $request, ClientRegistry $clientRegistry): JsonResponse
    {
        $service = $request->get('service');

        switch ($service) {
            case 'github':
                /** @var GithubClient $githubClient */
                $githubClient = $clientRegistry->getClient('github');
                $response = $githubClient->redirect(['read:user','user:email']);
                break;
            case 'google':
                /** @var GoogleClient $googleClient */
                $googleClient = $clientRegistry->getClient('google');
                $response = $googleClient->redirect(['openid','email']);
                break;
        }

        return new JsonResponse($response->getTargetUrl(),200);
    }

    /**
     * @Route("/logout")
     */
    public function logout() : void
    {
        // Intercepted
    }

    /**
     * @Route("/refresh")
     */
    public function refresh(CSRFService $cSRFService, Security $security) : JsonResponse
    {
        /** @var User $user */
        $user=$this->getUser();
        if(empty($user)) {
            return new JsonResponse('no session',401);
        }

        if ($user->getIsFrozen()===true) {
            return new JsonResponse([
                'title'=>'Connexion',
                'message'=>'Ce compte a été bloqué.'
            ],403);
        }else if($user->getIsActive()===true) {
            $presenter = new UserPresenter($user);

            return new JsonResponse([
                'isImpersonating' => $security->isGranted('ROLE_PREVIOUS_ADMIN'),
                'logged' => !empty($user),
                'user' =>$presenter->present(),
                'token' => $cSRFService->generate()
            ]);
        }else {
            return new JsonResponse([
                'title'=>'Connexion',
                'message'=>'Ce compte n\'est pas actif. Vérifiez votre boîte mail.'
            ],403);
        }
    }
}