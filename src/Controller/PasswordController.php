<?php


namespace App\Controller;

use App\Mailer\Mailer;
use App\Repository\UserRepository;
use App\Service\CommandLauncherService;
use App\Service\JsonRequestAcceptor;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordController extends AbstractController
{
    /**
     * @Route("/password/code", methods={"GET"})
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param UserService $userService
     * @param CommandLauncherService $commandLauncherService
     * @return JsonResponse
     */
    public function passwordCode(
        Request $request,
        UserService $userService,
        Mailer $mailer,
        UserRepository $userRepository): JsonResponse
    {
        $email = $request->get('email');
        $user = $userService->findByEmail($email);
        if(!$user||$user->getIsFrozen()) {
            return new JsonResponse([
                'title'=>'Mot de passe',
                'message'=>'Impossible de trouver l\'utilisateur',
            ],404);
        }

        $userService->askPasswordReset($user);
        //send password code for only one user

        $code = uniqid();
        $now = new \DateTime();

        $resetPasswordCodeSentAt = $user->getResetPasswordCodeSentAt();
        if($resetPasswordCodeSentAt!=null) {
            $interval = date_diff($resetPasswordCodeSentAt,$now);
            if($interval->format("%H")!=="00") {
                $mailer->sendEmail(
                    'Changement de mot de passe',
                    $user->getEmail(),
                    'code.reset.password.html.twig',
                    [
                        'code'=>$code
                    ]
                );
        
                $userRepository->resetPasswordCodeSent($user,$code);
            }
        }else {
            $mailer->sendEmail(
                'Changement de mot de passe',
                $user->getEmail(),
                'code.reset.password.html.twig',
                [
                    'code'=>$code
                ]
            );
    
            $userRepository->resetPasswordCodeSent($user,$code);
        }

        return new JsonResponse(null,200);
    }

    /**
     * @Route("/password/code", methods={"POST"})
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param UserService $userService
     * @return JsonResponse
     */
    public function checkCode(Request $request,
        JsonRequestAcceptor $acceptor,
        UserService $userService)
    {
        if(!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }
        $data = $acceptor->data();
        $user = $userService->findByEmail($data['email']);
        if(!$user||$user->getIsFrozen()) {
            return new JsonResponse([
                'title'=>'Mot de passe',
                'message'=>'Impossible de trouver l\'utilisateur',
            ],404);
        }

        $isCodeValid = $userService->checkPasswordResetCode($user,$data['code']);

        if(!$isCodeValid) {
            return new JsonResponse([
                'title'=>'Mot de passe',
                'message'=>'Le code fourni n\'est pas valide',
            ],403);
        }

        return new JsonResponse([],200);
    }

    /**
     * @Route("/password/change", methods={"POST"})
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param UserService $userService
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function changePassword(Request $request, JsonRequestAcceptor $acceptor,
        UserService $userService, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        if(!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }

        $response = $this->checkCode($request,$acceptor,$userService);

        if(!$userService->isResetPasswordCodeValid()) {
            return $response;
        }

        $data = $acceptor->data();
        $userService->changePassword($data['password'],$encoder);
        $userService->usedCodeForPasswordChange();

        return new JsonResponse([],200);
    }
}