<?php


namespace App\Controller;

use App\Service\JsonRequestAcceptor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RecaptchaController extends AbstractController
{
    /**
     * @Route("/recaptcha/validate", name="recaptcha-validate", methods={"POST"})
     * @return JsonResponse
     */
    public function validate(Request $request,
    JsonRequestAcceptor $acceptor,
    HttpClientInterface $httpClient
    ): JsonResponse {

        if(!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }
        $data = $acceptor->data();

        $response = $httpClient->request(
            'POST',
            $_SERVER['RECAPTCHA_VALIDATION_URL'],
            [
                'body' => [
                    'secret' => $_SERVER['RECAPTCHA_KEY'],
                    'response' => $data['recaptcha']
                ],
            ]
        );

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $content = $response->toArray();
        // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]

        if($statusCode===200 && $contentType == "application/json; charset=utf-8"
        && $content["success"]==true) {
            return new JsonResponse([
                "success" => true
            ], 200);
        }else {
            return new JsonResponse([
                "success" => false
            ], 200);
        }

        
    }
}