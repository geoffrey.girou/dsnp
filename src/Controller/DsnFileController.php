<?php


namespace App\Controller;


use App\Action\DeleteDsnFileAction;
use App\Controller\Api\ApiAccessController;
use App\Controller\Api\ApiController;
use App\Entity\DsnFile;
use App\Entity\User;
use App\Enum\DsnFileEnum;
use App\Enum\UserActionEnum;
use App\Presenter\CollectionPresenter;
use App\Repository\DsnFileRepository;
use App\Service\CSRFService;
use App\Service\Dsnfile\DsnFileUpdateStatusService;
use App\Service\Dsnpapi\DsnpapiDsnFileService;
use App\Service\DsnpDataService;
use App\Service\JsonRequestAcceptor;
use App\Service\UserActionService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DsnFileController extends ApiController implements ApiAccessController
{
    /**
     * @Route("/api/dsnfiles/uploadfiles", name="uploadfiles", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param DsnFileUpdateStatusService $dsnFileService
     * @param UserActionService $userActionService
     * @param DsnpDataService $dsnpDataService
     * @param DsnpapiDsnFileService $dsnpapiDsnFileService
     * @return JsonResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function send(
        Request $request,
        CSRFService $cSRFService,
        DsnFileRepository $dsnFileRepository,
        UserActionService $userActionService,
        DsnpDataService $dsnpDataService,
        DsnpapiDsnFileService $dsnpapiDsnFileService
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        if (!$user->getCanUploadDsnFile()) {
            return new JsonResponse([
                'title' => 'Transmission de fichier(s)',
                'message' => 'Permission refusée'
            ], 403);
        }

        /** @var UploadedFile[] $dsnFiles */
        $dsnFilesUploaded = $request->files->all()['files'];

        // Verify file extensions
        foreach ($dsnFilesUploaded as $dsnFileUploaded) {
            $extension = pathinfo($dsnFileUploaded->getClientOriginalName(), PATHINFO_EXTENSION);
            if (!in_array($extension, DsnFileEnum::EXTENSIONS)) {
                return new JsonResponse([
                    'title' => 'Transmission de fichier(s)',
                    'message' => 'Opération annulée. Merci de vérifier l\'extension de votre fichier.'
                ], 403);
            }
        }

        $isAnonymized = $request->get('isAnonymized') === 'true';

        $dsnFiles = [];
        $fileDirectory = '/tmp/' . $user->getId();
        foreach ($dsnFilesUploaded as $dsnFileUploaded) {
            $storageName = uniqid() . '.dsn';
            $dsnFileUploaded->move($fileDirectory, $storageName);

            // Verify mime type
            $mimeType = mime_content_type($fileDirectory . '/' . $storageName);
            if ($mimeType !== 'text/plain') {
                unlink($fileDirectory . '/' . $storageName);
                return new JsonResponse([
                    'title' => 'Transmission de fichier(s)',
                    'message' => 'Merci de transmettre des fichiers au format DSN'
                ], 403);
            }

            $dsnFile = $dsnFileRepository->create(
                $user,
                $dsnFileUploaded->getClientOriginalName(),
                $storageName
            );
            $dsnFiles[] = $dsnFile;

            $userActionService->add($user, UserActionEnum::FILE);
        }

        $finalParent = $user->getFinalParent();
        if ($finalParent->getDsnpData() === null) {
            // Create Siren collection
            $dsnpData = $dsnpDataService->createSirenCollection($finalParent);
            $finalParent->setDsnpData($dsnpData);
            // Add Siren to collection
            $dsnpDataService->addSirensToCollection($finalParent);
        }

        foreach ($dsnFiles as $dsnFile) {
            $filePath = '/tmp/' . $dsnFile->getUser()->getId() . '/' . $dsnFile->getStorageName();
            $dsnpapiDsnFileService->sendDsnFile($filePath, $dsnFile, $finalParent->getDsnpData(), $isAnonymized);
            $this->em->getRepository(DsnFile::class)->save($dsnFile);
        }

        $presenter = new CollectionPresenter(
            $dsnFiles,
            '\\App\\Presenter\\DsnFilePresenter'
        );
        return new JsonResponse($presenter->present(), 200);
    }

    /**
     * @Route("/api/dsnfiles", name="files", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function search(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $presenter = new CollectionPresenter(
            $this->em->getRepository(DsnFile::class)->findBy(['user' => $user, 'deletedAt' => null],
                ['createdAt' => 'DESC'], 100),
            '\\App\\Presenter\\DsnFilePresenter'
        );

        return new JsonResponse($presenter->present(), 200);
    }

    /**
     * @Route("/api/dsnfiles/refresh", name="refresh-dsnfiles", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param DsnFileUpdateStatusService $dsnFileService
     * @return JsonResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function refresh(
        Request $request,
        CSRFService $cSRFService,
        JsonRequestAcceptor $acceptor,
        DsnFileUpdateStatusService $dsnFileUpdateStatusService
    ): JsonResponse {
        if (!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }

        $data = $acceptor->data();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $dsnFiles = [];
        foreach ($data as $dsnFile) {
            $dsnFile = $dsnFileUpdateStatusService->updateStatus($dsnFile['id']);
            array_push($dsnFiles, $dsnFile);
        }

        $presenter = new CollectionPresenter(
            $dsnFiles,
            '\\App\\Presenter\\DsnFilePresenter'
        );
        return new JsonResponse($presenter->present(), 200);
    }

    /**
     * @Route("/api/dsnfiles/{id}", name="delete-dsnfile", methods={"DELETE"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param CSRFService $cSRFService
     * @param DsnpapiDsnFileService $dsnpapiDsnFileService
     * @return JsonResponse
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function delete(
        Request $request,
        CSRFService $cSRFService,
        DeleteDsnFileAction $deleteDsnFileAction
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $deleteData = $request->get('withData') === "true";
        $dsnfile = $this->em->getRepository(DsnFile::class)->findOneBy(['user' => $user, 'id' => $request->get('id')]);

        if (empty($dsnfile)) {
            throw new NotFoundHttpException((new \ReflectionClass(DsnFile::class))->getShortName() . ' not found');
        }

        $deleteDsnFileAction->process($user, $dsnfile, $deleteData);

        return new JsonResponse([], 200);
    }

    /**
     * @Route("/api/checkfile", name="check-file", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkFile(Request $request)
    {
        return new JsonResponse(['ok' => true], 200);
    }
}