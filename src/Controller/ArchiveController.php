<?php


namespace App\Controller;

use App\Archive\DsnArchive;
use App\Entity\User;
use App\Service\CSRFService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Process\Process;

class ArchiveController extends AbstractController
{
    /**
     * @Route("/api/dsnarchive/uploadarchive", name="uploadarchive", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return JsonResponse
     */
    public function send(Request $request,
    CSRFService $cSRFService,
    LoggerInterface $logger)
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        if(!$user->getCanUploadDsnFile()) {
            return new JsonResponse([
                'title'=>'Transmission de fichier',
                'message'=>'Permission refusée'
            ],403);
        }

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->all()['files'][0];
        
        $dsnArchive = new DsnArchive();
        $dsnArchive->createFromUploadedFile($uploadedFile);

        if(!$dsnArchive->isValid()) {
            return new JsonResponse([
                'title'=>'Transmission d\'archive',
                'message'=>'Opération annulée. Merci de vérifier l\'extension de votre fichier.'
            ],403);
        }

        $dsnArchiveName = uniqid().'.zip';
        $dsnArchive->move(__DIR__.'/../../tmp/archive/'.$user->getId(),$dsnArchiveName);

        $isAnonymized = ($request->get('isAnonymized') === 'true') ? "1" : "0";

        $logger->info('Shell command to execute : '.
        $_SERVER['WHICH_PHP']
        .' '
        .$_SERVER['PWD_CONSOLE']
        .' app:process-dsn-archive '
        .$user->getId()
        .' '
        .$dsnArchiveName
        .' '
        .$isAnonymized);

        $process = Process::fromShellCommandline(
            $_SERVER['WHICH_PHP']
            .' '
            .$_SERVER['PWD_CONSOLE']
            .' app:process-dsn-archive '
            .$user->getId()
            .' '
            .$dsnArchiveName
            .' '
            .$isAnonymized
        );

        $process->start();

        while ($process->isRunning()) {  
        }

        return new JsonResponse([],200);
    }

    /**
     * @Route("/api/checkarchive", name="check-archive", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkArchive(Request $request)
    {
        return new JsonResponse(['ok'=>true],200);
    }
}