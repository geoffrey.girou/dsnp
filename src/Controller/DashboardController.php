<?php


namespace App\Controller;


use App\Entity\User;
use App\Enum\UserActionEnum;
use App\Repository\Dsnpapi\GroupDsnpapiRepository;
use App\Service\Jwt\Expiration\JwtExpirationDate;
use App\Service\UserActionService;
use Firebase\JWT\JWT;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/api/tableau-de-bord", name="dashboard", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @param UserActionService $userActionService
     * @param GroupDsnpapiRepository $dsnpapiGroupService
     * @return JsonResponse
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     * @throws \App\Exception\DsnpapiUnexpectedErrorCodeException
     */
    public function view(
        UserActionService $userActionService,
        GroupDsnpapiRepository $dsnpapiGroupService
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $userActionService->add($user, UserActionEnum::DASHBOARD);

        if (sizeof($user->getAvailableSiren()) === 0 && $user->getGroupAccesses() === 0) {
            return new JsonResponse([
                'error' => 'Vous n\'avez pas d\'entreprise ou de groupement associé à votre compte utilisateur.'
            ], 200);
        }

        // If client and no dsnpdata can't find data
        // Else If child and access granted, then try access parent data
        if ($user->hasRole('ROLE_BUSINESS') && $user->getDsnpData() == null) {
            return new JsonResponse([
                'title' => 'Accès à vos données',
                'message' => 'Aucune donnée disponible pour le moment'
            ], 403);
        } elseif ($user->hasRole('ROLE_BUSINESS') && $user->getDsnpData() != null) {
            $uuid = $user->getDsnpData()->getUuid();
            $sirens = $user->getSirens();
            $groups = [];
            $response = $dsnpapiGroupService->list($user->getDsnpData()->getUuid());
            foreach ($response['groups'] as $group) {
                $groups[] = $group['id'];
            }
        } elseif (!$user->hasRole('ROLE_BUSINESS') && $user->hasRole('ROLE_USER')
            && $user->getCanAccessData() === true) {
            $finalParent = $user->getFinalParent();
            $uuid = $finalParent->getDsnpData()->getUuid();
            $groups = [];
            $sirens = [];
            foreach ($user->getSirenAccesses() as $sirenAccess) {
                array_push($sirens, $sirenAccess->getSiren());
            }
            foreach ($user->getGroupAccesses() as $groupAccess) {
                array_push($groups, $groupAccess->getDsnpapiGroupId());
            }
        } elseif ($user->hasRole('ROLE_USER')) {
            return new JsonResponse([
                'title' => 'Accès à vos données',
                'message' => 'Vous n\'avez pas l\'autorisation d\'accéder à ces données'
            ], 403);
        }

        $url = $_SERVER['APP_DSNP_URL'];
        $clientId = $_SERVER['APP_DSNP_CLIENT_ID'];

        $header = array('clientId' => $clientId);
        $key = file_get_contents(__DIR__ . '/../../config/jwt/private.pem');

        $sirets = [];
        foreach ($sirens as $siren) {
            array_push($sirets, $siren->getNumber() . '*****');
        }

        foreach ($groups as $group) {
            array_push($sirets, $group . '*****');
        }

        $jwtExpirationDate = new JwtExpirationDate();
        $payload = array(
            "exp" => $jwtExpirationDate->expirationDateStringForPayload(),
            "user" => [
                "uuid" => $uuid,
                "nom" => $user->getEmail(),
                "roles" => $user->getDsnpRoles(),
                "sirets" => $sirets,
                "matricule" => "*"
            ]
        );

        $token = JWT::encode($payload, $key, 'RS256', null, $header);

        return new JsonResponse([
            'url' => $url . '?path=&tok=' . $token
        ], 200);
    }
}