<?php


namespace App\Controller;


use App\Entity\User;
use App\Model\Siret;
use App\Model\SiretFactory;
use App\Presenter\CollectionPresenter;
use App\Service\DsnFileDeclarationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SiretController extends AbstractController
{
    /**
     * @Route("/api/sirets", name="all-sirets", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function all(DsnFileDeclarationService $dsnFileDeclarationService): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $sirets = [];
        foreach ($dsnFileDeclarationService->getGrouped($user) as $declaration) {
            $sirets[] =  SiretFactory::createFromNumber($declaration->getSiret());
        }

        $presenter = new CollectionPresenter(
            $sirets,
            '\\App\\Presenter\\SiretPresenter'
        );

        return new JsonResponse($presenter->present(),200);
    }
}