<?php


namespace App\Controller;


use App\Entity\GroupAccess;
use App\Entity\SirenAccess;
use App\Entity\User;
use App\Enum\UserActionEnum;
use App\Presenter\ChildPresenter;
use App\Presenter\CollectionPresenter;
use App\Service\ChildService;
use App\Service\CommandLauncherService;
use App\Service\CSRFService;
use App\Service\Dsnpapi\DsnpapiSirenCollectionService;
use App\Service\JsonRequestAcceptor;
use App\Service\PasswordGeneratorService;
use App\Service\SirenAccessService;
use App\Service\SirenService;
use App\Service\UserActionService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ChildController extends AbstractController
{
    /**
     * @Route("/api/child", name="all-child", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @param UserService $userService
     * @return JsonResponse
     */
    public function all(UserService $userService): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->getCanCreateUser()) {
            return new JsonResponse([
                'title' => 'Gestion utilisateur',
                'message' => 'Permission refusée'
            ], 403);
        }

        $presenter = new CollectionPresenter(
            $userService->findChildren($user),
            '\\App\\Presenter\\ChildPresenter'
        );
        return new JsonResponse($presenter->present(), 200);
    }

    /**
     * @Route("/api/child", name="create-child", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param UserService $userService
     * @param PasswordGeneratorService $passwordGeneratorService
     * @param UserActionService $userActionService
     * @param SirenService $sirenService
     * @param CommandLauncherService $commandLauncherService
     * @return JsonResponse
     */
    public function create(
        Request $request,
        CSRFService $cSRFService,
        JsonRequestAcceptor $acceptor,
        UserService $userService,
        PasswordGeneratorService $passwordGeneratorService,
        UserActionService $userActionService,
        SirenService $sirenService,
        CommandLauncherService $commandLauncherService
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        if (!$user->getCanCreateUser()) {
            return new JsonResponse([
                'title' => 'Gestion utilisateur',
                'message' => 'Permission refusée'
            ], 403);
        }

        if (!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }

        $data = $acceptor->data();

        $existingUser = $userService->findByEmail($data['email']);
        if ($existingUser != null) {
            return new JsonResponse([
                'title' => 'Ajout utilisateur',
                'message' => 'Cet Email est déjà utilisé.'
            ], 409);
        }

        // Generate password
        $password = $passwordGeneratorService->generate();

        $child = $userService->createChild(
            $user,
            $data['email'],
            $password,
            $data['canAccessData'],
            $data['canCreateUser'],
            $data['canUploadDsnFile'],
            $data['dsnpRole']
        );

        // SIREN MANAGEMENT
        $sirenAccesses = [];
        foreach ($data['sirens'] as $siren) {
            $siren = $sirenService->one(intval($siren['id']));
            
            $sirenAccessObject = new SirenAccess();
            $sirenAccessObject->setCreatedAt(new \DateTime());
            $sirenAccessObject->setUser($child);
            $sirenAccessObject->setSiren($siren);

            $sirenAccesses[] = $sirenAccessObject;
        }

        $groupAccesses = [];
        foreach ($data['groups'] as $group) {

            $groupAccessObject = new GroupAccess();
            $groupAccessObject->setCreatedAt(new \DateTime());
            $groupAccessObject->setUser($child);
            $groupAccessObject->setDsnpapiGroupId($group['id']);
            $groupAccessObject->setName($group['name']);

            $groupAccesses[] = $groupAccessObject;
        }

        $child = $userService->updateChildSirenAccess(
            $child,
            $sirenAccesses
        );

        $child = $userService->updateChildGroupAccess(
            $child,
            $groupAccesses
        );

        $userActionService->add($user, UserActionEnum::ADD);

        $commandLauncherService->executeWithoutOutput('app:registration-mail-confirmation');

        $presenter = new ChildPresenter($child);
        return new JsonResponse($presenter->present(), 200);
    }

    /**
     * @Route("/api/child", name="update-child", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param JsonRequestAcceptor $acceptor
     * @param UserService $userService
     * @param UserActionService $userActionService
     * @param SirenService $sirenService
     * @param SirenAccessService $sirenAccessService
     * @param DsnpapiSirenCollectionService $dsnpapiSirenCollectionService
     * @return JsonResponse
     */
    public function update(
        Request $request,
        CSRFService $cSRFService,
        JsonRequestAcceptor $acceptor,
        UserService $userService,
        UserActionService $userActionService,
        SirenService $sirenService
    ): JsonResponse {
        /** @var User $user */
        $user = $this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        if (!$user->getCanCreateUser()) {
            return new JsonResponse([
                'title' => 'Gestion utilisateur',
                'message' => 'Permission refusée'
            ], 403);
        }

        if (!$acceptor->accept($request)) {
            return $acceptor->jsonResponse();
        }

        $data = $acceptor->data();

        $child = $userService->findById($data['id']);
        if ($child == null) {
            return new JsonResponse([
                'title' => 'Modification utilisateur',
                'message' => 'Impossible de trouver l\'utilisateur'
            ], 404);
        }

        $sirenAccesses = [];
        foreach ($data['sirens'] as $siren) {
            $siren = $sirenService->one(intval($siren['id']));

            $sirenAccessObject = new SirenAccess();
            $sirenAccessObject->setCreatedAt(new \DateTime());
            $sirenAccessObject->setUser($child);
            $sirenAccessObject->setSiren($siren);

            $sirenAccesses[] = $sirenAccessObject;
        }

        $groupAccesses = [];
        foreach ($data['groups'] as $group) {

            $groupAccessObject = new GroupAccess();
            $groupAccessObject->setCreatedAt(new \DateTime());
            $groupAccessObject->setUser($child);
            $groupAccessObject->setDsnpapiGroupId($group['id']);
            $groupAccessObject->setName($group['name']);

            $groupAccesses[] = $groupAccessObject;
        }

        $child = $userService->updateChildSirenAccess(
            $child,
            $sirenAccesses
        );

        $child = $userService->updateChildGroupAccess(
            $child,
            $groupAccesses
        );

        $child = $userService->updateChild(
            $child,
            $data['canAccessData'],
            $data['canCreateUser'],
            $data['canUploadDsnFile'],
            $data['dsnpRole']
        );

        $userActionService->add($user, UserActionEnum::UPDATE);

        $presenter = new ChildPresenter($child);
        return new JsonResponse($presenter->present(), 200);
    }

    /**
     * @Route("/api/child/{id}", name="delete-child", methods={"DELETE"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param ChildService $childService
     * @param UserActionService $userActionService
     * @param DsnpapiSirenCollectionService $dsnpapiSirenCollectionService
     * @return JsonResponse
     */
    public function delete(
        Request $request,
        CSRFService $cSRFService,
        ChildService $childService,
        UserActionService $userActionService,
        DsnpapiSirenCollectionService $dsnpapiSirenCollectionService
    ): JsonResponse {
        /** @var User $parent */
        $parent = $this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));
        
        $child = $childService->one($request->get('id'));
        if ($child->getParent()->getId() !== $parent->getId()) {
            return new JsonResponse([
                'title' => 'Suppression utilisateur',
                'message' => 'Permission refusée'
            ], 403);
        }

        $childService->delete($child);
        $userActionService->add($parent, UserActionEnum::REMOVE);

        return new JsonResponse([], 200);
    }

    /**
     * @Route("/api/child/{id}", name="one-child", methods={"GET"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param ChildService $childService
     * @return JsonResponse
     */
    public function one(Request $request, ChildService $childService)
    {
        $child = $childService->one($request->get('id'));

        if ($child == null) {
            return new JsonResponse([
                'title' => 'Modification utilisateur',
                'message' => 'Impossible de trouver l\'utilisateur'
            ], 404);
        }

        $presenter = new ChildPresenter($child);
        return new JsonResponse($presenter->present(), 200);
    }
}