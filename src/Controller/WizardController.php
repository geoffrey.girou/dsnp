<?php


namespace App\Controller;


use App\Entity\User;
use App\Presenter\WizardPresenter;
use App\Service\CSRFService;
use App\Service\WizardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class WizardController extends AbstractController
{
    /**
     * @Route("/api/wizards", name="wizard", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function wizard()
    {
        /** @var User $user */
        $user=$this->getUser();

        $presenter = new WizardPresenter($user->getWizard());
        return new JsonResponse($presenter->present(),200);
    }

    /**
     * @Route("/api/wizards/welcome-ok", name="update-wizard-welcome-ok", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function welcomeOk(
        Request $request,
        WizardService $wizardService,
        CSRFService $cSRFService
    )
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $wizard = $wizardService->welcomeOk($user->getWizard());

        $presenter = new WizardPresenter($wizard);
        return new JsonResponse($presenter->present(),200);
    }

    /**
     * @Route("/api/wizards/first-siren-added", name="update-wizard-first-siren-added", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function firstSirenAdded(
        Request $request,
        WizardService $wizardService,
        CSRFService $cSRFService
    )
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $wizard = $wizardService->firstSirenAdded($user->getWizard());

        $presenter = new WizardPresenter($wizard);
        return new JsonResponse($presenter->present(),200);
    }

    /**
     * @Route("/api/wizards/first-dsn-file-added", name="update-first-dsn-file-added", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function firstDsnFileAddedAndStatusOk(
        Request $request,
        WizardService $wizardService,
        CSRFService $cSRFService
    )
    {
        /** @var User $user */
        $user=$this->getUser();

        $cSRFService->verify($request->headers->get('anti-csrf-token'));

        $wizard = $wizardService->firstDsnFileAddedAndStatusOk($user->getWizard());

        $presenter = new WizardPresenter($wizard);
        return new JsonResponse($presenter->present(),200);
    }
}