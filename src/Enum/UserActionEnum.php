<?php


namespace App\Enum;


abstract class UserActionEnum
{
    const CONNECTION    = 'CONNECTION_SUCCESS';
    const ADD           = 'ADD_CHILD';
    const UPDATE        = 'UPDATE_CHILD';
    const REMOVE        = 'REMOVE_CHILD';
    const FILE          = 'SEND_DSN_FILE';
    const DASHBOARD     = 'ACCESS_DSNP_DASHBOARD';
    const PROFILE       = 'UPDATE_PROFILE';
}
