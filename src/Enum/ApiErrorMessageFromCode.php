<?php

namespace App\Enum;

class ApiErrorMessageFromCode
{
    public static array $message = [
        -1 => 'Exception inattendue',
        1000 => 'L\'identifiant de la collection d\'entreprises n\'est pas bien formé',
        1001 => 'L\'identifiant de client n\'est pas bien formé',
        1002 => 'Un argument attendu par l\'api n\'a pas été fourni',
        1003 => 'L\'identifiant de la collection d\'entreprises n\'a pas été fourni',
        1004 => 'L\'identifiant et/ou le mot de passe sont incorrects',
        2000 => 'L\'identifiant de client existe déjà',
        2001 => 'La collection d\'entreprises existe déjà',
        2002 => 'L\'entreprise existe déjà',
        2003 => 'Le SIREN n\'est pas bien formé',
        2004 => 'La collection d\'entreprises n\'existe pas',
        2005 => 'L\'entreprise n\'existe pas',
        2006 => 'La propriété de client n\'existe pas',
        3000 => 'Le fichier DSN n\'a pas été fourni',
        3001 => 'Erreur de traitement : Les fichiers DSN de test ne sont pas pris en charge',
        3002 => 'Erreur de traitement : Le fichier DSN contient 0 ou plusieurs déclarations',
        3003 => 'Erreur de traitement : L\'identifiant de fichier DSN soumis n\'existe pas',
        3004 => 'Erreur de traitement : Trop d\'individus / au nombre d\'individus déclaré dans l\'entité',
        3005 => 'Erreur de traitement : Au moins un salarié(s) n\'a pas de matricule.',
        4000 => 'Le fichier DSN est en file d\'attente',
        4001 => 'Le fichier DSN est en cours de traitement',
        4002 => 'Le fichier DSN a été traité avec succès',
        4003 => 'Le traitement du fichier DSN n\'a pas abouti',
        4004 => 'Les données du fichier DSN sont marquées pour effacement',
        4005 => 'Les données du fichier DSN sont effacées',
        4006 => 'Le fichier DSN a déjà été analysé',
        5000 => 'Le fichier App_Data/Indicators.json n\'est pas valide',
        5001 => 'L\'indicateur n\'est pas défini dans la configuration de l\'API',
        5002 => 'Une erreur est survenue en exécutant l\'indicateur',
    ];
}