<?php


namespace App\Enum;


abstract class DsnArchiveEnum
{
    const EXTENSIONS    = [
        'zip',
        'ZIP'
    ];

    const MIMETYPES    = [
        'application/zip'
    ];
}