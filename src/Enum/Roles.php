<?php


namespace App\Enum;


abstract class Roles
{
    const ADMIN = 'ROLE_ADMIN';
    const MANAGER = 'ROLE_MANAGER';
    const BUSINESS = 'ROLE_BUSINESS';
    const USER = 'ROLE_USER';
}