<?php


namespace App\Enum;


abstract class PngLogoEnum
{
    const EXTENSIONS    = [
        'png',
        'PNG'
    ];

    const MIMETYPES    = [
        'image/png'
    ];
}