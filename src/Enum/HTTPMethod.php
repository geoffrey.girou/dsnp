<?php


namespace App\Enum;


abstract class HTTPMethod
{
    const PUT = 'PUT';
    const POST = 'POST';
    const PATCH = 'PATCH';
    const GET = 'GET';
    const DELETE = 'DELETE';
}