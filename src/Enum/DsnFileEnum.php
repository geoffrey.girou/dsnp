<?php


namespace App\Enum;


abstract class DsnFileEnum
{
    const EXTENSIONS    = [
        'dsn',
        'DSN',
        'txt',
        'TXT',
        'edi',
        'EDI'
    ];

    const MIMETYPES    = [
        'text/plain'
    ];
}