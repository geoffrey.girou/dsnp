<?php


namespace App\Model;


class Siret
{
    private $nic;
    private $siren;

    public function nic()
    {
        return $this->nic;
    }

    public function siren()
    {
        return $this->siren;
    }

    /**
     * @param mixed $nic
     */
    public function setNic($nic): void
    {
        $this->nic = $nic;
    }

    /**
     * @param mixed $siren
     */
    public function setSiren($siren): void
    {
        $this->siren = $siren;
    }
}