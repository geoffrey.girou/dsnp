<?php


namespace App\Model;


use _HumbugBoxec8571fe8659\Nette\Utils\DateTime;

class Group
{
    /**
     * @var string $id
     */
    private $id;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var Siret[] $sirets
     */
    private $sirets;

    /**
     * @var \DateTime $sirets
     */
    private $createdAt;

    public function __construct($id, $description, $time)
    {
        $this->id = $id;
        $this->description = $description;
        $this->sirets = [];
        $this->createdAt = \DateTime::createFromFormat('Y-m-d\TH:i:s',substr($time,0,19));
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Siret[]
     */
    public function getSirets(): array
    {
        return $this->sirets;
    }

    /**
     * @param Siret[] $sirets
     */
    public function setSirets(array $sirets): void
    {
        $this->sirets = $sirets;
    }

    public function addSiret(Siret $siret)
    {
        $this->sirets[] = $siret;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }


}