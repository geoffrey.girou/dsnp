<?php


namespace App\Model;


class SiretFactory
{
    public static function createFromNumber($number)
    {
        $siret = new Siret();
        $siret->setNic(substr($number,9,14));
        $siret->setSiren(substr($number,0,9));

        return $siret;
    }

    public static function create($nic,$siren)
    {
        $siret = new Siret();
        $siret->setNic($nic);
        $siret->setSiren($siren);

        return $siret;
    }
}