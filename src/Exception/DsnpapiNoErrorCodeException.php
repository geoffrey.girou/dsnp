<?php

namespace App\Exception;

use Throwable;

class DsnpapiNoErrorCodeException extends \Exception
{
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('Dsnp API Error no error_code found in dsnpapi response', 500, $previous);
    }
}