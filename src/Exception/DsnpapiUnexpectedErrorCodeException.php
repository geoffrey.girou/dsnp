<?php

namespace App\Exception;

use Throwable;

class DsnpapiUnexpectedErrorCodeException extends \Exception
{
    public function __construct($message = '', Throwable $previous = null)
    {
        parent::__construct($message, 500, $previous);
    }
}