<?php

namespace App\EventSubscriber;

use App\Controller\Api\ApiAccessController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiAccessSubscriber implements EventSubscriberInterface
{
    /**
     * @param ControllerEvent $event
     * @return void
     */
    public function onKernelController(ControllerEvent $event)
    {
        //get the controller
        $controller = $event->getController();
        if (is_array($controller)) {
            $controller = $controller[0];
        }

        //if the controller implements ApiAccessController...
        if ($controller instanceof ApiAccessController) {
            $request = $event->getRequest();
            $controller->init($request);
        }

        return;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}
