<?php

namespace App\JSONHttpRequest\Team;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ViewTeamRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'équipe n'a pas été trouvée."
     * )
     * @Assert\Type(
     *      type="integer",
     *      message="La valeur {{ value }} ne doit contenir que des chiffres."
     * )
     */
    public int $id;
}