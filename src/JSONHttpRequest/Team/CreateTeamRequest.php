<?php

namespace App\JSONHttpRequest\Team;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class CreateTeamRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="Le nom ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $description;
}