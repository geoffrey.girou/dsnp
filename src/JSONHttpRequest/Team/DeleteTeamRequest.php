<?php

namespace App\JSONHttpRequest\Team;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteTeamRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'équipe n'a pas été trouvée."
     * )
     */
    public int $id;
}