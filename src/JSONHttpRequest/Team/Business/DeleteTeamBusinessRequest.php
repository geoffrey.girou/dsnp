<?php

namespace App\JSONHttpRequest\Team\Business;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteTeamBusinessRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'équipe n'a pas été trouvée."
     * )
     */
    public int $teamId;
    /**
     * @Assert\NotBlank(
     *     message="Le domaine n'a pas été trouvé."
     * )
     */
    public int $businessId;
}