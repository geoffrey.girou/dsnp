<?php

namespace App\JSONHttpRequest\Team;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateTeamRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'équipe n'a pas été trouvée."
     * )
     */
    public int $id;
    /**
     * @Assert\NotBlank(
     *     message="Le nom ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $description;
}