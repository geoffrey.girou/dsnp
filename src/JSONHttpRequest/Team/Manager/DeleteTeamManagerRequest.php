<?php

namespace App\JSONHttpRequest\Team\Manager;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteTeamManagerRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'équipe n'a pas été trouvée."
     * )
     */
    public int $teamId;
    /**
     * @Assert\NotBlank(
     *     message="Le gestionnaire n'a pas été trouvé."
     * )
     */
    public int $managerId;
}