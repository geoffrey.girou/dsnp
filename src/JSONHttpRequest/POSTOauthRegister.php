<?php

namespace App\JSONHttpRequest;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class POSTOauthRegister extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=250
     * )
     */
    public $companyName;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=250
     * )
     */
    public $firstname;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=250
     * )
     */
    public $lastname;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=250
     * )
     */
    public $addressLine1;
    /**
     * @Assert\Length(
     *     max=250
     * )
     */
    public $addressLine2;
    /**
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="digit"
     * )
     * @Assert\Length(
     *     min=5,
     *     max=5
     * )
     */
    public $postalCode;
    /**
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="digit"
     * )
     * @Assert\Length(
     *     min=10,
     *     max=10
     * )
     */
    public $phoneNumber;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max=250
     * )
     */
    public $city;

    public function __construct(ValidatorInterface $validator)
    {
        parent::__construct($validator);
        $this->validate();
    }
}