<?php

namespace App\JSONHttpRequest\Manager;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateManagerRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="Le gestionnaire n'a pas été trouvée."
     * )
     * @Assert\Type(
     *      type="integer",
     *      message="La valeur {{ value }} ne doit contenir que des chiffres."
     * )
     */
    public int $id;
    /**
     * @Assert\NotBlank(
     *     message="L'email ne doit pas être vide."
     * )
     * @Assert\Email(
     *      message="L'email {{ value }} n'est pas valide."
     * )
     */
    public string $email;
}