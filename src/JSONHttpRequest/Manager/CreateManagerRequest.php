<?php

namespace App\JSONHttpRequest\Manager;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class CreateManagerRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'email ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $email;
}