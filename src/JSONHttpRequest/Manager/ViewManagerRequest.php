<?php

namespace App\JSONHttpRequest\Manager;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class ViewManagerRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="Le gestionnaire n'a pas été trouvé."
     * )
     * @Assert\Type(
     *      type="integer",
     *      message="La valeur {{ value }} ne doit contenir que des chiffres."
     * )
     */
    public int $id;
}