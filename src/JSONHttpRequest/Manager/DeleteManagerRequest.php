<?php

namespace App\JSONHttpRequest\Manager;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteManagerRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="Le gestionnaire n'a pas été trouvé."
     * )
     */
    public int $id;
}