<?php

namespace App\JSONHttpRequest\PersonalData;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class CreatePersonalDataRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'utilisateur n'a pas été trouvé."
     * )
     */
    public int $userId;
    /**
     * @Assert\NotBlank(
     *     message="Le nom du domaine ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $companyName;
    /**
     * @Assert\NotBlank(
     *     message="Le prénom ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $firstname;
    /**
     * @Assert\NotBlank(
     *     message="Le nom ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $lastname;
    /**
     * @Assert\NotBlank(
     *     message="L'adresse ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $addressLine1;
    /**
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $addressLine2;
    /**
     * @Assert\NotBlank(
     *     message="Le code postal ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=5
     * )
     */
    public string $postalCode;
    /**
     * @Assert\NotBlank(
     *     message="La ville ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $city;
    /**
     * @Assert\NotBlank(
     *     message="Le téléphone ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=10
     * )
     */
    public string $phoneNumber;
}