<?php

namespace App\JSONHttpRequest\Siren;

use App\JSONHttpRequest\JSONHttpRequest;
use App\Service\SirenService;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

class SirenCreateRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="Le numéro SIREN ne doit pas être vide."
     * )
     * @Assert\Type(
     *      type="integer",
     *      message="La valeur {{ value }} ne doit contenir que des chiffres."
     * )
     * @Assert\Length(
     *     min=9,
     *     max=9,
     * )
     */
    public $number;
    /**
     * @Assert\NotBlank(
     *     message="L'effectif mensuel maximum ne doit pas être vide."
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="La valeur {{ value }} ne doit contenir que des chiffres."
     * )
     * @Assert\Length(
     *     min=1,
     *     max=10000
     * )
     */
    public $maxPersonnelNumbers;
    /**
     * @Assert\NotBlank(
     *     message="Le nom de l'entreprise ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public $name;
}