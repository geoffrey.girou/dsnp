<?php

namespace App\JSONHttpRequest;

use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;

class POSTRegister extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'email ne doit pas être vide."
     * )
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas valide."
     * )
     * @Assert\Length(
     *     min=6,
     *     max=180
     * )
     */
    public $email;
    /**
     * @Assert\NotBlank(
     *     message="Le mot de passe ne doit pas être vide."
     * )
     * @Assert\Length(
     *     min=12,
     *     max=250
     * )
     */
    public $password;
    /**
     * @Assert\NotBlank(
     *     message="Le nom du domaine ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public $companyName;
    /**
     * @Assert\NotBlank(
     *     message="Le prénom ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public $firstname;
    /**
     * @Assert\NotBlank(
     *     message="Le nom de famille ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public $lastname;
    /**
     * @Assert\NotBlank(
     *     message="L'adresse principale ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public $addressLine1;
    /**
     * @Assert\Length(
     *     max=250
     * )
     */
    public $addressLine2;
    /**
     * @Assert\NotBlank(
     *     message="Le code postal ne doit pas être vide."
     * )
     * @Assert\Type(
     *     type="digit"
     * )
     * @Assert\Length(
     *     min=5,
     *     max=5
     * )
     */
    public $postalCode;
    /**
     * @Assert\NotBlank(
     *     message="Le numéro de téléphone ne doit pas être vide."
     * )
     * @Assert\Type(
     *     type="digit"
     * )
     * @Assert\Length(
     *     min=10,
     *     max=10
     * )
     */
    public $phoneNumber;
    /**
     * @Assert\NotBlank(
     *     message="Le nom de la ville ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public $city;

    protected bool $isCSRFProtected = false;
    protected bool $isAutoValidation = true;

    public function verifyMailExists(UserService $userService)
    {
        $user = $userService->findByEmail($this->email);
        if ($user != null) {
            $messages = ['message' => 'validation_failed', 'errors' => []];

            $messages['errors'][] = [
                'property' => 'email',
                'value' => $this->email,
                'message' => 'Cet email n\'est pas disponible',
            ];

            $response = new JsonResponse($messages);
            $response->send();
        }
    }
}