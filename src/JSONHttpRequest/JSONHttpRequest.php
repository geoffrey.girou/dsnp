<?php

namespace App\JSONHttpRequest;

use App\Service\CSRFService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class JSONHttpRequest
{
    protected ValidatorInterface $validator;

    protected Request $request;

    protected array $data;

    protected bool $isCSRFProtected = true;
    protected bool $isAutoValidation = true;

    public function __construct(RequestStack $requestStack, ValidatorInterface $validator, CSRFService $cSRFService)
    {
        $this->validator = $validator;
        $this->data = [];
        $this->request = $requestStack->getCurrentRequest();
        if(!empty($this->request->getContent())) {
            $this->canHandleJSON();
        }
        if($this->isCSRFProtected) {
            $cSRFService->verify($this->request->headers->get('anti-csrf-token'));
        }
        $this->populate();
        if($this->isAutoValidation) {
            $this->validate();
        }
    }

    private function canHandleJSON()
    {
        if (false === $this->supportsJSON($this->request)) {
            $response = new JsonResponse([
                'message' => 'Unable to handle the request',
            ], 500);
            $response->send();
            die();
        }
    }

    public function validate()
    {
        $errors = $this->validator->validate($this);

        $messages = ['message' => 'validation_failed', 'errors' => []];

        /** @var ConstraintViolation  */
        foreach ($errors as $message) {
            $messages['errors'][] = [
                'property' => $message->getPropertyPath(),
                'value' => $message->getInvalidValue(),
                'message' => $message->getMessage(),
            ];
        }

        if (count($messages['errors']) > 0) {
            $response = new JsonResponse($messages);
            $response->send();
        }
    }

    protected function populate(): void
    {
        if(!empty($this->request->getContent())) {
            try {
                $this->data = json_decode((string)$this->request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $exception) {
                $response = new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
                $response->send();
            }
        }

        foreach ($this->request->attributes as $attribute => $value) {
            if (property_exists($this, $attribute)) {
                $this->{$attribute} = $value;
            }
        }

        foreach ($this->data as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function  supportsJSON(Request $request): bool
    {
        return 'json' === $request->getContentType();
    }
}