<?php

namespace App\JSONHttpRequest\Business;

use App\JSONHttpRequest\JSONHttpRequest;
use Symfony\Component\Validator\Constraints as Assert;

class CreateBusinessRequest extends JSONHttpRequest
{
    /**
     * @Assert\NotBlank(
     *     message="L'email ne doit pas être vide."
     * )
     * @Assert\Length(
     *     max=250
     * )
     */
    public string $email;
}