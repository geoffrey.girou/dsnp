<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;

class MaintenanceListener
{
    private Environment $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    public function onKernelRequest(RequestEvent $event) : void
    {
        if($_SERVER['MAINTENANCE']==="on") {
            // Customize your response object to display the exception details
            $response = new Response();
            $response->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE);

            $response->setContent($this->environment->render('maintenance.html.twig'));
            // sends the modified response object to the event
            $event->setResponse($response);
        }
    }
}