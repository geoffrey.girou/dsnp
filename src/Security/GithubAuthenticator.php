<?php

namespace App\Security;

use App\Entity\User;
use App\Service\PasswordGeneratorService;
use App\Service\UserService;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class GithubAuthenticator extends SocialAuthenticator
{
    private RouterInterface $router;
    private ClientRegistry $clientRegistry;
    private UserService $userService;
    private UserPasswordEncoderInterface $encoder;
    private PasswordGeneratorService $passwordGeneratorService;
    private string $service;

    public function __construct(RouterInterface $router,
        ClientRegistry $clientRegistry,
        UserService $userService,
        UserPasswordEncoderInterface $encoder,
        PasswordGeneratorService $passwordGeneratorService
    )
    {
        $this->router = $router;
        $this->clientRegistry = $clientRegistry;
        $this->userService = $userService;
        $this->encoder = $encoder;
        $this->passwordGeneratorService = $passwordGeneratorService;
    }

    public function start(Request $request, AuthenticationException $authException = null): RedirectResponse
    {
        return new RedirectResponse($this->router->generate('login'));
    }

    public function supports(Request $request): bool
    {
        return 'oauth_check' === $request->attributes->get('_route') &&
            (
                $request->get('service')==='github' ||
                $request->get('service')==='google'
            );
    }

    public function getCredentials(Request $request)
    {
        $this->service = $request->get('service');
        return $this->fetchAccessToken($this->getClient($this->service));
    }

    /**
     * @param AccessToken $credentials
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = $this->getClient($this->service)->fetchUserFromToken($credentials);
        $password = $this->encoder->encodePassword(new User(), $this->passwordGeneratorService->generate());

        switch ($this->service) {
            case 'github':
                /** @var GithubResourceOwner $user */
                $user = $this->getClient('github')->fetchUserFromToken($credentials);
                $user = $this->userService->findOrCreateFromOauthGithub($user,$password);
                break;
            case 'google':
                /** @var GoogleUser $user */
                $user = $this->getClient('google')->fetchUserFromToken($credentials);
                $user = $this->userService->findOrCreateFromOauthGoogle($user,$password);
                break;
        }
        return $user;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // TODO: Implement onAuthenticationFailure() method.
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): RedirectResponse
    {
        if($token->getUser()->getPersonalData()===null) {
            return new RedirectResponse('/complete_inscription');
        }
        return new RedirectResponse('/');
    }

    private function getClient(string $service): OAuth2ClientInterface
    {
        return $this->clientRegistry->getClient($service);
    }
}