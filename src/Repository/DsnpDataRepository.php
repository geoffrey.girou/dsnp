<?php

namespace App\Repository;

use App\Entity\DsnpData;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DsnpData|null find($id, $lockMode = null, $lockVersion = null)
 * @method DsnpData|null findOneBy(array $criteria, array $orderBy = null)
 * @method DsnpData[]    findAll()
 * @method DsnpData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DsnpDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DsnpData::class);
    }

    public function create(User $user, string $uuid): DsnpData
    {
        $dsnpData = new DsnpData();
        $dsnpData->setCreatedAt(new \DateTime());
        $dsnpData->setUser($user);
        $dsnpData->setUuid($uuid);

        return $this->save($dsnpData);
    }

    public function save(DsnpData $dsnpData): DsnpData
    {
        $this->_em->persist($dsnpData);
        $this->_em->flush();
        return $dsnpData;
    }

    // /**
    //  * @return DsnpData[] Returns an array of DsnpData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DsnpData
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
