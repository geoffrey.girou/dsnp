<?php

namespace App\Repository;

use App\Entity\Siren;
use App\Entity\Siret;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Siret|null find($id, $lockMode = null, $lockVersion = null)
 * @method Siret|null findOneBy(array $criteria, array $orderBy = null)
 * @method Siret[]    findAll()
 * @method Siret[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiretRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Siret::class);
    }

    public function create(Siren $siren, string $nic, string $name)
    {
        $siret = new Siret();
        $siret->setCreatedAt(new \DateTime());
        $siret->setSiren($siren);
        $siret->setNic($nic);
        $siret->setName($name);

        return $this->save($siret);
    }

    public function save(Siret $siret)
    {
        $this->_em->persist($siret);
        $this->_em->flush();

        return $siret;
    }

    public function delete(Siret $siret)
    {
        $this->_em->remove($siret);
        $this->_em->flush();
    }

    public function update(Siret $siret, Siren $siren, string $nic, string $name)
    {
        $siret->setSiren($siren);
        $siret->setNic($nic);
        $siret->setName($name);

        return $this->save($siret);
    }
}
