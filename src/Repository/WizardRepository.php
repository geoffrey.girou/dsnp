<?php

namespace App\Repository;

use App\Entity\Wizard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Wizard|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wizard|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wizard[]    findAll()
 * @method Wizard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WizardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wizard::class);
    }

    public function save(Wizard $wizard): Wizard
    {
        $this->_em->persist($wizard);
        $this->_em->flush();
        return $wizard;
    }
}
