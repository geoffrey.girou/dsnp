<?php

namespace App\Repository;

use App\Entity\GroupAccess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GroupAccess|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroupAccess|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroupAccess[]    findAll()
 * @method GroupAccess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupAccessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroupAccess::class);
    }

    public function save(GroupAccess $groupAccess)
    {
        $this->_em->persist($groupAccess);
        $this->_em->flush();
        return $groupAccess;
    }

    public function delete(GroupAccess $groupAccess)
    {
        $this->_em->remove($groupAccess);
        $this->_em->flush();
    }
}
