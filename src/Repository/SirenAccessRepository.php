<?php

namespace App\Repository;

use App\Entity\Siren;
use App\Entity\SirenAccess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SirenAccess|null find($id, $lockMode = null, $lockVersion = null)
 * @method SirenAccess|null findOneBy(array $criteria, array $orderBy = null)
 * @method SirenAccess[]    findAll()
 * @method SirenAccess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SirenAccessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SirenAccess::class);
    }

    public function save(SirenAccess $sirenAccess)
    {
        $this->_em->persist($sirenAccess);
        $this->_em->flush();

        return $sirenAccess;
    }

    public function findBySirenAndUserIds(Siren $siren, array $userIds)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('sa')
            ->from($this->_entityName, 'sa')
            ->where('sa.user IN (:userIds)')
            ->andWhere('sa.siren = :siren')
            ->setParameter('userIds', $userIds)
            ->setParameter('siren', $siren);

        return $qb->getQuery()->getResult();
    }
}
