<?php

namespace App\Repository;

use App\Entity\DsnFile;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DsnFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method DsnFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method DsnFile[]    findAll()
 * @method DsnFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DsnFileRepository extends ServiceEntityRepository
{
    /**
     * DsnFileRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DsnFile::class);
    }

    /**
     * @param User $user
     * @param string $name
     * @param string $storageName
     * @return DsnFile
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(User $user, string $name, string $storageName) : DsnFile
    {
        $dsnFile = new DsnFile();
        $dsnFile->setUser($user);
        $dsnFile->setName($name);
        $dsnFile->setStorageName($storageName);
        $dsnFile->setStatus('EN COURS DE TRAITEMENT');
        $dsnFile->setCreatedAt(new \DateTime());

        return $this->save($dsnFile);
    }

    /**
     * @param DsnFile $file
     * @return DsnFile
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(DsnFile $file): DsnFile
    {
        $this->_em->persist($file);
        $this->_em->flush();
        return $file;
    }

    /**
     * @param DsnFile $dsnFile
     * @return DsnFile
     */
    public function softDelete(DsnFile $dsnFile)
    {
        $dsnFile->setDeletedAt(new \DateTime());
        return $this->save($dsnFile);
    }

    public function delete(DsnFile $dsnFile)
    {
        $this->_em->remove($dsnFile);
        $this->_em->flush();
    }

    /**
     * @return int|mixed|string
     */
    public function findDsnFilesToSend()
    {
        return $this->createQueryBuilder('f')
            ->join(
                'App\Entity\DsnpData',
                'd',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'd.user = f.user'
            )
            ->andWhere('f.status = :status')
            ->setParameter('status','en_cours_de_transmission')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return int|mixed|string
     */
    public function findDsnFilesToUpdate()
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.status = :status')
            ->setParameter('status','en_cours')
            ->getQuery()
            ->getResult();
    }

    // SELECT count(DISTINCT(dsn_file_id)) FROM dsn_file INNER JOIN dsn_file_declaration WHERE siret LIKE '111111111%'
    public function countBySiren($number)
    {
        return $this->createQueryBuilder('f')
            ->select('COUNT(DISTINCT fd.dsnFile)')
            ->innerJoin('f.dsnFileDeclarations','fd')
            ->where('fd.siret LIKE :number')
            ->setParameter('number', $number.'%')
            ->getQuery()
            ->getSingleScalarResult();
    }

    // SELECT DISTINCT(dsn_file_id) FROM dsn_file INNER JOIN dsn_file_declaration WHERE siret LIKE '111111111%'
    public function findBySiren($number)
    {
        return $this->createQueryBuilder('f')
            ->select('DISTINCT(fd.dsnFile)')
            ->innerJoin('f.dsnFileDeclarations','fd')
            ->where('fd.siret LIKE :number')
            ->setParameter('number', $number.'%')
            ->getQuery()
            ->getResult();
    }
}
