<?php


namespace App\Repository\Dsnpapi;


use App\Enum\HTTPMethod;
use App\Exception\DsnpapiUnexpectedErrorCodeException;
use App\Model\Group;
use App\Model\SiretFactory;
use App\Service\Dsnpapi\Client\DsnpapiHttpClient;
use App\Service\Dsnpapi\Request\DsnpapiRequest;


class GroupDsnpapiRepository
{
    private DsnpapiHttpClient $client;
    
    public function __construct(DsnpapiHttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $uuid
     * @param string $groupIdentifier
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function one(string $uuid, string $groupIdentifier)
    {
        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::GET);
        $request->setPath($uuid.'/groupe//'.$groupIdentifier);
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function list(string $uuid)
    {
        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::GET);
        $request->setPath($uuid.'/groupe');
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param string $groupId
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function details(string $uuid, string $groupId)
    {
        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::GET);
        $request->setPath($uuid.'/groupe/'.$groupId);
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param string $name
     * @param array $sirets
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function create(string $uuid, string $name, array $sirets)
    {
        $data = [
            'description'=>$name,
            'sirets' => $sirets
        ];

        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::POST);
        $request->setPath($uuid.'/groupe');
        $request->setBody(json_encode($data));
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param string $dsnpapiId
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function delete(string $uuid, string $dsnpapiId)
    {
        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::DELETE);
        $request->setPath($uuid.'/groupe/'.$dsnpapiId);
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param string $dsnpapiId
     * @param string $name
     * @param array|null $sirets
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function update(string $uuid, string $dsnpapiId, string $name, array $sirets=null)
    {
        $data = [
            'description'=>$name
        ];
        if($sirets!=null) {
            $data['sirets'] = $sirets;
        }

        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::PUT);
        $request->setPath($uuid.'/groupe/'.$dsnpapiId);
        $request->setBody(json_encode($data));
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @param string $dsnpapiId
     * @param array $sirets
     * @return array
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function deleteSirets(string $uuid, string $dsnpapiId, array $sirets)
    {
        $data = [
            'sirets'=>$sirets
        ];

        $request = new DsnpapiRequest();
        $request->setMethod(HTTPMethod::DELETE);
        $request->setPath($uuid.'/groupe/'.$dsnpapiId);
        $request->setBody(json_encode($data));
        $response = $this->client->execute($request);

        if ($response->hasError()) {
            throw new DsnpapiUnexpectedErrorCodeException($response->getErrorMessage());
        }

        return $response->getData();
    }

    /**
     * @param string $uuid
     * @return array<Group>
     * @throws DsnpapiUnexpectedErrorCodeException
     * @throws \App\Exception\DsnpapiUnauthorizedHttpException
     */
    public function listWithDetails($uuid): array
    {
        $groupsResponse = $this->list($uuid);
        $groupModels = [];
        if(isset($groupsResponse['groups'])) {
            foreach ($groupsResponse['groups'] as $group) {
                $detailsResponse = $this->details($uuid,$group['id']);
                $groupModel = new Group($detailsResponse['id'],$detailsResponse['description'], $detailsResponse['time']);
                foreach ($detailsResponse['sirets'] as $siret) {
                    $groupModel->addSiret(SiretFactory::create($siret['nic'],$siret['siren']));
                }
                $groupModels[] = $groupModel;
            }
        }
        return $groupModels;
    }
}