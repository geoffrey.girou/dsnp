<?php

namespace App\Repository;

use App\Entity\DsnFile;
use App\Entity\DsnFileDeclaration;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Mailer\Transport\Dsn;

/**
 * @method DsnFileDeclaration|null find($id, $lockMode = null, $lockVersion = null)
 * @method DsnFileDeclaration|null findOneBy(array $criteria, array $orderBy = null)
 * @method DsnFileDeclaration[]    findAll()
 * @method DsnFileDeclaration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DsnFileDeclarationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DsnFileDeclaration::class);
    }

    public function create(
        DsnFile $dsnFile,
        int $personnelNumbersCount,
        int $siret,
        \DateTime $month
    )
    {
        $dsnFileDeclaration = new DsnFileDeclaration();
        $dsnFileDeclaration->setDsnFile($dsnFile);
        $dsnFileDeclaration->setCreatedAt(new \DateTime());
        $dsnFileDeclaration->setPersonnelNumbersCount($personnelNumbersCount);
        $dsnFileDeclaration->setSiret($siret);
        $dsnFileDeclaration->setMonth($month);

        return $dsnFileDeclaration;
    }
    public function save(DsnFileDeclaration $declaration)
    {
        $this->_em->persist($declaration);
        $this->_em->flush();

        return $declaration;
    }

    public function findBySiren($siren)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('d')
            ->from($this->_entityName, 'd')
            ->where('d.siret LIKE :siret')
            ->setParameter('siret', $siren.'%');

        return $qb->getQuery()->getResult();
    }

    public function getGrouped(array $dsnFileIds)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('d')
            ->from($this->_entityName, 'd')
            ->groupBy('d.siret')
            ->where('d.dsnFile IN (:dsnFileIds)')
            ->setParameter('dsnFileIds', $dsnFileIds);

        return $qb->getQuery()->getResult();
    }
}
