<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Wizard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->save($user);
    }

    public function findFromGithub(GithubResourceOwner $owner): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.githubId = :githubId')
            ->orWhere('u.email = :email')
            ->setParameters([
                'githubId' => $owner->getId(),
                'email' => $owner->getEmail()
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findFromGoogle(GoogleUser $owner): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.googleId = :googleId')
            ->orWhere('u.email = :email')
            ->setParameters([
                'googleId' => $owner->getId(),
                'email' => $owner->getEmail()
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function createFromGithub(ResourceOwnerInterface $owner, string $password): User
    {
        $user = new User();
        $user->setGithubId($owner->getId());
        $user->setEmail($owner->getEmail());

        return $this->createFromOauth($user,$password);
    }

    public function createFromGoogle(ResourceOwnerInterface $owner, string $password): User
    {
        $user = new User();
        $user->setGoogleId($owner->getId());
        $user->setEmail($owner->getEmail());

        return $this->createFromOauth($user,$password);
    }

    public function createFromOauth(User $user, string $password): User
    {
        $user->setCreatedAt(new \DateTime());
        $user->setIsActive(0);
        $user->setIsFrozen(0);
        $user->setCanAccessData(1);
        $user->setCanCreateUser(1);
        $user->setCanUploadDsnFile(1);
        $user->setPassword($password);
        $user->setRoles(['ROLE_BUSINESS','ROLE_USER']);
        $user->setDsnpRoles(['admindsnp']);

        return $this->save($user);
    }

    public function createClient(string $email, string $password): User
    {
        $user = new User();
        $user->setCreatedAt(new \DateTime());
        $user->setIsActive(0);
        $user->setIsFrozen(0);
        $user->setCanAccessData(1);
        $user->setCanCreateUser(1);
        $user->setCanUploadDsnFile(1);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setRoles(['ROLE_BUSINESS', 'ROLE_USER']);
        $user->setDsnpRoles(['admindsnp']);

        return $user;
    }

    public function createChild(
        User $parent,
        string $email,
        string $password,
        bool $canAccessData,
        bool $canCreateUser,
        bool $canUploadDsnFile,
        string $dsnpRole
    ): User {
        $user = new User();
        $user->setCreatedAt(new \DateTime());
        $user->setIsActive(0);
        $user->setIsFrozen(0);
        $user->setCanAccessData($canAccessData);
        $user->setCanCreateUser($canCreateUser);
        $user->setCanUploadDsnFile($canUploadDsnFile);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setRoles(['ROLE_USER']);
        $user->setDsnpRoles([$dsnpRole]);
        $completedWizard = new Wizard();
        $completedWizard->setCreatedAt(new \DateTime());
        $completedWizard->setUser($user);
        $completedWizard->setIsFirstSirenAdded(true);
        $completedWizard->setIsFirstDsnFileAddedAndStatusOk(true);
        $completedWizard->setIsWelcomeOk(true);
        $user->setWizard($completedWizard);
        $user->setParent($parent);

        return $user;
    }

    public function updateChild(
        User $child,
        bool $canAccessData,
        bool $canCreateUser,
        bool $canUploadDsnFile,
        string $dsnpRole
    ): User {
        $child->setCanAccessData($canAccessData);
        $child->setCanCreateUser($canCreateUser);
        $child->setCanUploadDsnFile($canUploadDsnFile);
        $child->setDsnpRoles([$dsnpRole]);

        return $child;
    }

    public function updateChildSirenAccess(
        User $child,
        array $sirenAccesses
    ) {
        foreach ($child->getSirenAccesses() as $sirenAccess) {
            $child->removeSirenAccess($sirenAccess);
        }
        foreach ($sirenAccesses as $sirenAccess) {
            $child->addSirenAccess($sirenAccess);
        }
        return $this->save($child);
    }

    public function updateChildGroupAccess(
        User $child,
        array $groupAccesses
    ) {
        foreach ($child->getGroupAccesses() as $groupAccess) {
            $child->removeGroupAccess($groupAccess);
        }
        foreach ($groupAccesses as $groupAccess) {
            $child->addGroupAccess($groupAccess);
        }
        return $this->save($child);
    }

    public function save(User $user): User
    {
        $user->setUpdatedAt(new \DateTime());
        $this->_em->persist($user);
        $this->_em->flush();
        return $user;
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findResetPasswordAsked()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.isResetPassword = :isResetPassword')
            ->andWhere('u.resetPasswordAskedAt IS NOT NULL')
            ->setParameter('isResetPassword', true)
            ->getQuery()
            ->getResult();
    }

    public function resetPasswordCodeSent(User $user, string $code)
    {
        $user->setResetPasswordCodeSentAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $user->setResetPasswordCode($code);
        $this->save($user);
    }

    public function mailConfirmationLinkSent(User $user)
    {
        $user->setMailConfirmationSentAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());
        $this->save($user);
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findNewRegisteredClient()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.reportedAt IS NULL')
            ->andWhere('u.parent IS NULL')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findNotActiveClient()
    {
        return $this->createQueryBuilder('u')
            ->where('u.mailConfirmationSentAt IS NULL')
            ->andWhere('u.isActive = :isActive')
            ->setParameter('isActive', 0)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return User[]
     */
    public function findClientToFreez()
    {
        $date = new \DateTime();
        $date->modify('-14 day');
        $minus14daysDatetime = $date->format("Y-m-d H:i:s");
        return $this->createQueryBuilder('u')
            ->andWhere('u.createdAt < :createdAt')
            ->andWhere('u.isFrozen = :isFrozen')
            ->andWhere('u.paymentReportedAt IS NULL')
            ->andWhere('u.isActive = 1')
            ->andWhere('u.parent IS NULL')
            ->setParameter('createdAt', $minus14daysDatetime)
            ->setParameter('isFrozen', 0)
            ->getQuery()
            ->getResult();
    }

    public function findUserWithDsnFileWaiting()
    {
        try {
            return $this->createQueryBuilder('u')
                ->select('DISTINCT(u.id)')
                ->leftJoin(
                    'App\Entity\DsnpData',
                    'd',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'd.user = u.id'
                )
                ->join(
                    'App\Entity\DsnFile',
                    'f',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'f.user = u.id'
                )
                ->andWhere('d.id IS NULL')
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
            return false;
        }
    }

    public function freezUser(User $user): void
    {
        $user->setIsFrozen(true);
        $this->save($user);
    }

    public function delete(User $user)
    {
        $this->_em->remove($user);
        $this->_em->flush();
    }

    /**
     * @param $role
     * @return User[]
     */
    public function findByRole($role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.roles LIKE :roles')
            ->orderBy('u.createdAt', 'DESC')
            ->setParameter('roles', '%"' . $role . '"%');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $manager
     * @return float|int|mixed|string
     */
    public function findWithManager(User $manager)
    {

        $sql = "SELECT u.id, u.email, u.created_at, u.is_active, u.is_frozen, p.id as personal_data_id, p.created_at as personal_data_created_at, p.updated_at as personal_data_updated_at, p.company_name FROM user u 
    INNER JOIN personal_data p ON u.id = p.user_id
    INNER JOIN businessteam_user ON u.id = businessteam_user.user_id WHERE businessteam_user.team_id IN (SELECT team_id FROM managerteam_user WHERE user_id = ?)";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata('App\\Entity\\User', 'u');
        $rsm->addJoinedEntityFromClassMetadata('App\\Entity\\PersonalData', 'p', 'u', 'personalData', array('id' => 'personal_data_id', 'created_at' => 'personal_data_created_at', 'updated_at' => 'personal_data_updated_at'));

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $manager->getId());

        return $query->getResult();
    }

    public function active(User $user)
    {
        $user->setIsActive(1);
        $this->save($user);
    }
}
