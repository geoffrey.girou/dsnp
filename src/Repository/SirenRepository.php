<?php

namespace App\Repository;

use App\Entity\Siren;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Siren|null find($id, $lockMode = null, $lockVersion = null)
 * @method Siren|null findOneBy(array $criteria, array $orderBy = null)
 * @method Siren[]    findAll()
 * @method Siren[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SirenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Siren::class);
    }

    public function delete(Siren $siren)
    {
        $this->_em->remove($siren);
        $this->_em->flush();
    }

    public function save(Siren $siren)
    {
        $this->_em->persist($siren);
        $this->_em->flush();

        return $siren;
    }
    // /**
    //  * @return Siren[] Returns an array of Siren objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findWithAccess($criteria, $orderBy, $limit)
    {
        return $this->createQueryBuilder('s')
            ->innerJoin('s.sirenAccesses', 'sa', 'WITH', 'sa.user = :user')
            ->setParameters($criteria)
            ->getQuery()
            ->getResult()
        ;
    }
}
