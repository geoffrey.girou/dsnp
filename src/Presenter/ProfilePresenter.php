<?php


namespace App\Presenter;


use App\Entity\User;

class ProfilePresenter implements Presenter
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function present()
    {
        return [
            'email'=>$this->user->getEmail(),
            'addressLine1'=>$this->user->getPersonalData()->getAddressLine1(),
            'addressLine2'=>$this->user->getPersonalData()->getAddressLine2(),
            'city'=>$this->user->getPersonalData()->getCity(),
            'companyName'=>$this->user->getPersonalData()->getCompanyName(),
            'firstname'=>$this->user->getPersonalData()->getFirstname(),
            'lastname'=>$this->user->getPersonalData()->getLastname(),
            'postalCode'=>$this->user->getPersonalData()->getPostalCode(),
            'phoneNumber'=>$this->user->getPersonalData()->getPhoneNumber()
        ];
    }

    public function feed($data)
    {
        $this->user = $data;
    }
}