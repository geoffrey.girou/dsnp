<?php


namespace App\Presenter;


use App\Entity\Wizard;

class WizardPresenter implements Presenter
{
    private Wizard $wizard;

    public function __construct(Wizard $wizard=null)
    {
        if($wizard!==null) {
            $this->wizard = $wizard;
        }
    }

    public function present()
    {
        return [
            'id' => $this->wizard->getId(),
            'isFirstSirenAdded' => $this->wizard->getIsFirstSirenAdded(),
            'isFirstDsnFileAddedAndStatusOk' => $this->wizard->getIsFirstDsnFileAddedAndStatusOk(),
            'isWelcomeOk' => $this->wizard->getIsWelcomeOk()
        ];
    }

    public function feed($data)
    {
        $this->wizard = $data;
    }
}