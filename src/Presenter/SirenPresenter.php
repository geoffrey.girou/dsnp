<?php


namespace App\Presenter;


use App\Entity\Siren;

class SirenPresenter implements Presenter
{
    private Siren $siren;

    public function __construct(Siren $siren = null)
    {
        if($siren!==null) {
            $this->siren = $siren;
        }
    }

    public function present()
    {
        return [
            'id' => $this->siren->getId(),
            'name'=> $this->siren->getName(),
            'number'=> $this->siren->getNumber(),
            'createdAt' => $this->siren->getCreatedAt(),
            'maxPersonnelNumbers' => $this->siren->getMaxPersonnelNumbers()
        ];
    }

    public function feed($data)
    {
        $this->siren = $data;
    }
}