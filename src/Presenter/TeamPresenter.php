<?php

namespace App\Presenter;

use App\Entity\Team;

class TeamPresenter implements Presenter
{

    private Team $team;

    public function __construct(Team $team = null)
    {
        if ($team !== null) {
            $this->team = $team;
        }
    }

    public function present()
    {
        $data = [
            'id' => $this->team->getId(),
            'description' => $this->team->getDescription(),
            'createdAt' => $this->team->getCreatedAt(),
            'businesses' => [],
            'managers' => []
        ];
        foreach ($this->team->getBusinesses() as $business) {
            $data['businesses'][] = [
                'id' => $business->getId(),
                'email' => $business->getEmail(),
                'createdAt' => $business->getCreatedAt(),
                'isActive' => $business->getIsActive(),
                'isFrozen' => $business->getIsFrozen(),
            ];
        }
        foreach ($this->team->getManagers() as $manager) {
            $data['managers'][] = [
                'id'=>$manager->getId(),
                'email'=>$manager->getEmail(),
                'createdAt'=>$manager->getCreatedAt(),
                'isActive'=>$manager->getIsActive(),
                'isFrozen'=>$manager->getIsFrozen(),
                ];
        }

        return $data;
    }

    public function feed($data)
    {
        $this->team = $data;
    }
}