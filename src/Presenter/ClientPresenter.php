<?php


namespace App\Presenter;


use App\Entity\User;

class ClientPresenter implements Presenter
{
    private User $client;

    public function __construct(User $client = null)
    {
        if($client!==null) {
            $this->client = $client;
        }
    }

    public function present()
    {
        $collectionPresenter = new CollectionPresenter(
            $this->client->getSirens(),
            '\\App\\Presenter\\SirenPresenter'
        );

        return [
            'id'=>$this->client->getId(),
            'email'=>$this->client->getEmail(),
            'createdAt'=>$this->client->getCreatedAt(),
            'sirens'=>$collectionPresenter->present(),
            'isValidPayment'=>($this->client->getPaymentReportedAt()!==null),
            'isActive'=>$this->client->getIsActive(),
            'isFrozen'=>$this->client->getIsFrozen(),
            'uuid'=> ($this->client->getDsnpData()) ? (($this->client->getDsnpData()->getUuid()) ? $this->client->getDsnpData()->getUuid() : 'N/A') : 'N/A'
        ];
    }

    public function feed($data)
    {
        $this->client = $data;
    }
}