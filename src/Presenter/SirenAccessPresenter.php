<?php


namespace App\Presenter;


use App\Entity\SirenAccess;

class SirenAccessPresenter implements Presenter
{
    private SirenAccess $sirenAccess;

    public function __construct(SirenAccess $sirenAccess = null)
    {
        if($sirenAccess!==null) {
            $this->sirenAccess = $sirenAccess;
        }
    }

    public function present()
    {
        return [
            'id'=> $this->sirenAccess->getSiren()->getId(),
            'number'=> $this->sirenAccess->getSiren()->getNumber(),
            'name'=> $this->sirenAccess->getSiren()->getName()
        ];
    }

    public function feed($data)
    {
        $this->sirenAccess = $data;
    }
}