<?php


namespace App\Presenter;


use App\Entity\DsnFile;

class DsnFilePresenter implements Presenter
{
    private DsnFile $dsnFile;

    public function __construct(DsnFile $dsnFile = null)
    {
        if($dsnFile !== null) {
            $this->dsnFile = $dsnFile;
        }
    }

    public function present()
    {
        $declartions = $this->dsnFile->getDsnFileDeclarations();
        $presentedDeclarations = [];
        foreach ($declartions as $declaration) {
            $presentedDeclarations[] = [
                'id' => $declaration->getId(),
                'firstOfMonth' => $declaration->getMonth(),
                'siret' => $declaration->getSiret(),
                'personnelNumbersCount' => $declaration->getPersonnelNumbersCount()
            ];
        }

        if(substr($this->dsnFile->getStatus(), 0, strlen("La DSN a déjà été soumise") ) === "La DSN a déjà été soumise") {
            $this->dsnFile->setStatus("INFO : Fichier déjà envoyé");
        }
        //ERREUR DE TRAITEMENT : L'entreprise 111111124 n'a pas été trouvée dans la collection d'entreprises a700dc31-712b-4fb5-bb56-ab6f7727f597
        if(strlen($this->dsnFile->getStatus()) === 138) {
            $status = str_replace("Erreur de traitement : L'entreprise", "Erreur de traitement : Le siren", $this->dsnFile->getStatus());
            //Erreur de traitement : Le siren 111111124 n'a pas été trouvée dans la collection d'entreprises a700dc31-712b-4fb5-bb56-ab6f7727f597
            $status = substr($status,0, 42);
            $this->dsnFile->setStatus($status." n'est pas déclaré dans votre compte.");
        }

        return [
            'id' => $this->dsnFile->getId(),
            'createdAt' => $this->dsnFile->getCreatedAt(),
            'status' => $this->dsnFile->getStatus(),
            'name' => $this->dsnFile->getName(),
            'userId' => $this->dsnFile->getUser()->getId(),
            'declarations' => $presentedDeclarations
        ];
    }

    public function feed($data)
    {
        $this->dsnFile = $data;
    }
}