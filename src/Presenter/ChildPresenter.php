<?php


namespace App\Presenter;


use App\Entity\User;

class ChildPresenter implements Presenter
{
    private User $child;

    public function __construct(User $child = null)
    {
        if($child!==null) {
            $this->child = $child;
        }
    }

    public function feed($data)
    {
        $this->child = $data;
    }

    public function present()
    {
        $collectionSirenPresenter = new CollectionPresenter(
            $this->child->getSirenAccesses(),
            '\\App\\Presenter\\SirenAccessPresenter'
        );

        $collectionGroupPresenter = new CollectionPresenter(
            $this->child->getGroupAccesses(),
            '\\App\\Presenter\\GroupAccessPresenter'
        );

        return [
            'id'=>$this->child->getId(),
            'email'=>$this->child->getEmail(),
            'sirens'=>$collectionSirenPresenter->present(),
            'groups'=>$collectionGroupPresenter->present(),
            'createdAt'=>$this->child->getCreatedAt(),
            'canCreateUser' => $this->child->getCanCreateUser(),
            'canAccessData' => $this->child->getCanAccessData(),
            'canUploadDsnFile' => $this->child->getCanUploadDsnFile(),
            'dsnpRole' => (sizeof($this->child->getDsnpRoles()) !== 0) ? $this->child->getDsnpRoles()[0] : ''
        ];
    }
}