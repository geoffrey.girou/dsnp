<?php

namespace App\Presenter;

use App\Entity\User;

class ManagerPresenter implements Presenter
{
    private User $manager;

    public function __construct(User $manager = null)
    {
        if($manager!==null) {
            $this->manager = $manager;
        }
    }

    public function present()
    {
        $data = [
            'id'=>$this->manager->getId(),
            'email'=>$this->manager->getEmail(),
            'createdAt'=>$this->manager->getCreatedAt(),
            'isActive'=>$this->manager->getIsActive(),
            'isFrozen'=>$this->manager->getIsFrozen(),
            'teams' => []
        ];
        foreach ($this->manager->getManagerTeams() as $team) {
            $data['teams'][] = [
                'id' => $team->getId(),
                'description' => $team->getDescription(),
                'createdAt' => $team->getCreatedAt(),
            ];
        }
        return $data;
    }

    public function feed($data)
    {
        $this->manager = $data;
    }
}