<?php


namespace App\Presenter;


use App\Entity\User;

class UserPresenter implements Presenter
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function present()
    {
        $wizardPresenter = new WizardPresenter($this->user->getWizard());
        return [
            'id' => $this->user->getId(),
            'email' => $this->user->getUsername(),
            'roles' => $this->user->getRoles(),
            'lastUpdatedAt' => $this->user->getUpdatedAt(),
            'createdAt' => $this->user->getCreatedAt(),
            'canCreateUser' => $this->user->getCanCreateUser(),
            'canAccessData' => $this->user->getCanAccessData(),
            'canUploadDsnFile' => $this->user->getCanUploadDsnFile(),
            'dsnpRole' => (sizeof($this->user->getDsnpRoles()) !== 0) ? $this->user->getDsnpRoles()[0] : '',
            'wizard' => ($this->user->getWizard()) ? $wizardPresenter->present() : null,
            'logo' => ($this->user->getFinalParent()->getLogo()) ? '../../../logo/' . $this->user->getFinalParent()->getLogo() : null
        ];
    }

    public function feed($data)
    {
        $this->user = $data;
    }
}