<?php

namespace App\Presenter;

use App\Entity\PersonalData;

class PersonalDataPresenter implements Presenter
{
    private PersonalData $personalData;

    public function __construct(PersonalData $personalData = null)
    {
        if($personalData!==null) {
            $this->personalData = $personalData;
        }
    }

    public function present(): array
    {
        return [
            'id'=>$this->personalData->getId(),
            'createdAt'=>$this->personalData->getCreatedAt(),
            'updatedAt'=>$this->personalData->getUpdatedAt(),
            'companyName'=>$this->personalData->getCompanyName(),
            'firstname'=>$this->personalData->getFirstName(),
            'lastname'=>$this->personalData->getLastName(),
            'addressLine1'=>$this->personalData->getAddressLine1(),
            'addressLine2'=>$this->personalData->getAddressLine2(),
            'postalCode'=>$this->personalData->getPostalCode(),
            'city'=>$this->personalData->getCity(),
            'phoneNumber'=>$this->personalData->getPhoneNumber(),
        ];
    }

    public function feed($data)
    {
        $this->personalData = $data;
    }
}