<?php


namespace App\Presenter;


use App\Entity\GroupAccess;

class GroupAccessPresenter implements Presenter
{
    private GroupAccess $groupAccess;

    public function __construct(GroupAccess $groupAccess = null)
    {
        if($groupAccess!==null) {
            $this->groupAccess = $groupAccess;
        }
    }

    public function present()
    {
        return [
            'id' => $this->groupAccess->getId(),
            'apiId' => $this->groupAccess->getDsnpapiGroupId(),
            'name' => $this->groupAccess->getName()
        ];
    }

    public function feed($data)
    {
        $this->groupAccess = $data;
    }
}