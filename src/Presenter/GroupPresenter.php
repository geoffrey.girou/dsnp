<?php


namespace App\Presenter;

use App\Model\Group;

class GroupPresenter implements Presenter
{
    private Group $group;

    public function __construct(Group $group = null)
    {
        if($group!==null) {
            $this->group = $group;
        }
    }

    public function present()
    {
        $siretCollectionPresenter = new CollectionPresenter(
            $this->group->getSirets(),
            '\\App\\Presenter\\SiretPresenter'
        );
        return [
            'id' => $this->group->getId(),
            'name' => $this->group->getDescription(),
            'createdAt' => $this->group->getCreatedAt(),
            'sirets' => $siretCollectionPresenter->present()
        ];
    }

    public function feed($data)
    {
        $this->group = $data;
    }
}