<?php


namespace App\Presenter;

class CollectionPresenter implements Presenter
{
    private $collection;
    private Presenter $presenter;

    public function __construct($collection, string $presenterClass)
    {
        $this->collection = $collection;
        $this->presenter = new $presenterClass();
    }

    public function present()
    {
        $content = [];
        foreach ($this->collection as $item) {
            $this->presenter->feed($item);
            $content[] = $this->presenter->present();
        }
        return $content;
    }

    public function feed($data)
    {
        $this->collection = $data;
    }
}