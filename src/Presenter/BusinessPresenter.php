<?php

namespace App\Presenter;

use App\Entity\User;

class BusinessPresenter implements Presenter
{
    private User $business;

    public function __construct(User $business = null)
    {
        if($business!==null) {
            $this->business = $business;
        }
    }

    public function present()
    {
        $data = [
            'id'=>$this->business->getId(),
            'companyName' =>  $this->business->getPersonalData() ? $this->business->getPersonalData()->getCompanyName() : '',
            'uuid' => $this->business->getDsnpData() ? $this->business->getDsnpData()->getUuid() : '',
            'email'=>$this->business->getEmail(),
            'createdAt'=>$this->business->getCreatedAt(),
            'isValidPayment'=>($this->business->getPaymentReportedAt()!==null),
            'isActive'=>$this->business->getIsActive(),
            'isFrozen'=>$this->business->getIsFrozen(),
            'sirens' => [],
            'teams' => []
        ];

        foreach ($this->business->getSirens() as $siren) {
            $data['sirens'][] = [
                'id' => $siren->getId(),
                'createdAt' => $siren->getCreatedAt(),
                'name' => $siren->getName(),
                'number' => $siren->getNumber(),
            ];
        }

        foreach ($this->business->getBusinessTeams() as $team) {
            $data['teams'][] = [
                'id' => $team->getId(),
                'description' => $team->getDescription(),
                'createdAt' => $team->getCreatedAt(),
            ];
        }
        return $data;
    }

    public function feed($data)
    {
        $this->business = $data;
    }
}