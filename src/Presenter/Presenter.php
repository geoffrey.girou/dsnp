<?php


namespace App\Presenter;


interface Presenter
{
    public function present();
    public function feed($data);
}