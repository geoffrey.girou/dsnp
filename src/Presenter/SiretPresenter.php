<?php


namespace App\Presenter;

use App\Model\Siret;

class SiretPresenter implements Presenter
{
    private Siret $siret;

    public function __construct(Siret $siret = null)
    {
        if($siret!==null) {
            $this->siret = $siret;
        }
    }

    public function present()
    {
        return [
            'nic'=> $this->siret->nic(),
            'siren' => $this->siret->siren()
        ];
    }

    public function feed($data)
    {
        $this->siret = $data;
    }
}