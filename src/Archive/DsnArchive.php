<?php

namespace App\Archive;

use App\Enum\DsnArchiveEnum;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ZipArchive;
use Symfony\Component\Filesystem\Filesystem;

class DsnArchive
{
    private UploadedFile $uploadedFile;
    private bool $isExtensionValid;
    private bool $isMimeTypeValid;
    private string $extractFolder;
    private string $name;

    public function __construct()
    {
    }

    public function createFromUploadedFile(UploadedFile $uploadedFile)
    {
        $extension = $uploadedFile->getClientOriginalExtension();
        if(!in_array($extension,DsnArchiveEnum::EXTENSIONS)) {
            $this->isExtensionValid = false;
        }else {
            $this->isExtensionValid = true;
        }

        $mimeType = $uploadedFile->getMimeType();
        if(!in_array($mimeType,DsnArchiveEnum::MIMETYPES)) {
            $this->isMimeTypeValid = false;
        }else {
            $this->isMimeTypeValid = true;
        }
        $this->uploadedFile = $uploadedFile;
    }

    public function createFromFile($folder,$name)
    {
        $this->extractFolder = $folder;
        $this->name = $name;
    }

    public function isValid()
    {
        return $this->isExtensionValid && $this->isMimeTypeValid;
    }

    public function move($folder,$name)
    {
        $this->extractFolder = $folder;
        $this->name = $name;
        $this->uploadedFile->move($folder,$name);
    }
    public function extract()
    {
        $zip = new ZipArchive;
        if($zip->open($this->extractFolder.'/'.$this->name) === TRUE) {
            $zip->extractTo($this->extractFolder);
            $zip->close();
            return true;
        }
        return false;
    }

    public function deleteArchive()
    {
        unlink($this->extractFolder.'/'.$this->name);
    }

    public function deleteExtractFolder()
    {
        $filesystem = new Filesystem();
        $filesystem->remove($this->extractFolder);
    }

    public function getExtractFolder()
    {
        return $this->extractFolder;
    }
}