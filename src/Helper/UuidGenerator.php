<?php


namespace App\Helper;


use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UuidGenerator
{
    public UuidInterface $uuid;

    public function __construct()
    {
    }

    public function generate()
    {
        $this->uuid = Uuid::uuid4();
    }
    /**
     * @return string
     */
    public function toString(): string
    {
        return ($this->uuid!=null) ? $this->uuid->toString() : 'not an uuid';
    }
}