<?php

namespace App\DataFixtures;

use App\Entity\Team;
use App\Entity\User;
use App\Entity\Wizard;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TeamManagerFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) : void
    {
        $team1 = new Team();
        $team1->setCreatedAt(new \DateTime());
        $team1->setUpdatedAt(new \DateTime());
        $team1->setDescription('team-manager1');

        $team2 = new Team();
        $team2->setCreatedAt(new \DateTime());
        $team2->setUpdatedAt(new \DateTime());
        $team2->setDescription('team-manager2');

        $team3 = new Team();
        $team3->setCreatedAt(new \DateTime());
        $team3->setUpdatedAt(new \DateTime());
        $team3->setDescription('team-manager3');

        $team4 = new Team();
        $team4->setCreatedAt(new \DateTime());
        $team4->setUpdatedAt(new \DateTime());
        $team4->setDescription('team-manager4');

        $userManager = new User();
        $userManager->setCreatedAt(new \DateTime());
        $userManager->setEmail('manager2@dsnplus.com');
        $userManager->setPassword($this->passwordEncoder->encodePassword(new User(),'tT1*testtest'));
        $userManager->setRoles(['ROLE_MANAGER','ROLE_USER']);
        $userManager->setIsFrozen(false);
        $userManager->setIsActive(true);
        $userManager->setCanAccessData(true);
        $userManager->setCanCreateUser(true);
        $userManager->setCanUploadDsnFile(true);

        $wizard = new Wizard();
        $wizard->setCreatedAt(new \DateTime());
        $wizard->setIsWelcomeOk(true);
        $wizard->setIsFirstDsnFileAddedAndStatusOk(true);
        $wizard->setIsFirstSirenAdded(true);
        $userManager->setWizard($wizard);


        $manager->persist($userManager);
        $team1->addManager($userManager);
        $manager->persist($team1);
        $team2->addManager($userManager);
        $manager->persist($team2);
        $team3->addManager($userManager);
        $manager->persist($team3);
        $team4->addManager($userManager);
        $manager->persist($team4);
        $manager->flush();
    }
}
