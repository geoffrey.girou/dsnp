<?php

namespace App\DataFixtures;

use App\Entity\PersonalData;
use App\Entity\Siren;
use App\Entity\User;
use App\Entity\Wizard;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) : void
    {
       $this->loadUser($manager);
       for($i=0;$i<50;$i++) {
        $this->loadBusiness($manager);
       }
        for($i=0;$i<50;$i++) {
            $this->loadManager($manager);
        }
       $this->loadAdmin($manager);
    }

    private function loadUser(ObjectManager $manager)
    {
        $user = new User();
        $user->setCreatedAt(new \DateTime());
        $user->setEmail('user@dsnplus.com');
        $user->setPassword($this->passwordEncoder->encodePassword($user,'tT1*testtest'));
        $user->setRoles(['ROLE_USER']);
        $user->setIsFrozen(false);
        $user->setIsActive(true);
        $user->setCanAccessData(false);
        $user->setCanCreateUser(false);
        $user->setCanUploadDsnFile(false);

        $manager->persist($user);

        $manager->flush();
    }

    private function loadBusiness(ObjectManager $manager)
    {
        $user = new User();
        $user->setCreatedAt(new \DateTime());
        $user->setEmail('business@dsnplus.'.uniqid().'.com');
        $user->setPassword($this->passwordEncoder->encodePassword($user,'tT1*testtest'));
        $user->setRoles(['ROLE_BUSINESS','ROLE_USER']);
        $user->setIsFrozen(false);
        $user->setIsActive(true);
        $user->setCanAccessData(true);
        $user->setCanCreateUser(true);
        $user->setCanUploadDsnFile(true);

        $personalData = new PersonalData();
        $personalData->setCreatedAt(new \DateTime());
        $personalData->setFirstname('Firstname');
        $personalData->setLastname('Lastname');
        $personalData->setAddressLine1('AddressLine1');
        $personalData->setAddressLine2('AddressLine2');
        $personalData->setCity('City');
        $personalData->setPostalCode('11111');
        $personalData->setPhoneNumber('0404040404');
        $personalData->setCompanyName('CompanyName'.uniqid());
        $user->setPersonalData($personalData);

        $siren = new Siren();
        $siren->setCreatedAt(new \DateTime());
        $siren->setName('Siren'.uniqid());
        $siren->setNumber(random_int(100000000,999999999));
        $siren->setMaxPersonnelNumbers(111);
        $user->addSiren($siren);

        $wizard = new Wizard();
        $wizard->setCreatedAt(new \DateTime());
        $wizard->complete();
        $user->setWizard($wizard);

        $manager->persist($user);

        $manager->flush();
    }

    private function loadManager(ObjectManager $manager)
    {
        $user = new User();
        $user->setCreatedAt(new \DateTime());
        $user->setEmail('manager@dsnplus'.uniqid().'.com');
        $user->setPassword($this->passwordEncoder->encodePassword($user,'tT1*testtest'));
        $user->setRoles(['ROLE_MANAGER','ROLE_USER','ROLE_ALLOWED_TO_SWITCH']);
        $user->setIsFrozen(false);
        $user->setIsActive(true);
        $user->setCanAccessData(true);
        $user->setCanCreateUser(true);
        $user->setCanUploadDsnFile(true);

        $wizard = new Wizard();
        $wizard->setCreatedAt(new \DateTime());
        $wizard->setIsWelcomeOk(true);
        $wizard->setIsFirstDsnFileAddedAndStatusOk(true);
        $wizard->setIsFirstSirenAdded(true);

        $user->setWizard($wizard);

        $manager->persist($user);
        $manager->flush();
    }

    private function loadAdmin(ObjectManager $manager)
    {
        $user = new User();
        $user->setCreatedAt(new \DateTime());
        $user->setEmail('admin@dsnplus.com');
        $user->setPassword($this->passwordEncoder->encodePassword($user,'tT1*testtest'));
        $user->setRoles(['ROLE_ADMIN','ROLE_USER','ROLE_ALLOWED_TO_SWITCH']);
        $user->setIsFrozen(false);
        $user->setIsActive(true);
        $user->setCanAccessData(false);
        $user->setCanCreateUser(false);
        $user->setCanUploadDsnFile(false);

        $wizard = new Wizard();
        $wizard->setCreatedAt(new \DateTime());
        $wizard->setIsWelcomeOk(true);
        $wizard->setIsFirstDsnFileAddedAndStatusOk(true);
        $wizard->setIsFirstSirenAdded(true);

        $user->setWizard($wizard);

        $manager->persist($user);

        $manager->flush();
    }


}
