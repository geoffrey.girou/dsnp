<?php

namespace App\DataFixtures;

use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TeamFixtures extends Fixture
{
    public function load(ObjectManager $manager) : void
    {
        $team = new Team();
        $team->setCreatedAt(new \DateTime());
        $team->setUpdatedAt(new \DateTime());
        $team->setDescription('team1');

        $manager->persist($team);

        $manager->flush();
    }
}
