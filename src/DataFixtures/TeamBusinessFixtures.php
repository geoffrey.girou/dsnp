<?php

namespace App\DataFixtures;

use App\Entity\PersonalData;
use App\Entity\Siren;
use App\Entity\Team;
use App\Entity\User;
use App\Entity\Wizard;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TeamBusinessFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) : void
    {
        $team = new Team();
        $team->setCreatedAt(new \DateTime());
        $team->setUpdatedAt(new \DateTime());
        $team->setDescription('team-business1');

        $business = new User();
        $business->setCreatedAt(new \DateTime());
        $business->setEmail('business2@dsnplus.com');
        $business->setPassword($this->passwordEncoder->encodePassword(new User(),'tT1*testtest'));
        $business->setRoles(['ROLE_BUSINESS','ROLE_USER','ROLE_ALLOWED_TO_SWITCH']);
        $business->setIsFrozen(false);
        $business->setIsActive(true);
        $business->setCanAccessData(true);
        $business->setCanCreateUser(true);
        $business->setCanUploadDsnFile(true);

        $personalData = new PersonalData();
        $personalData->setCreatedAt(new \DateTime());
        $personalData->setFirstname('Firstname');
        $personalData->setLastname('Lastname');
        $personalData->setAddressLine1('AddressLine1');
        $personalData->setAddressLine2('AddressLine2');
        $personalData->setCity('City');
        $personalData->setPostalCode('11111');
        $personalData->setPhoneNumber('0404040404');
        $personalData->setCompanyName('CompanyName'.uniqid());
        $business->setPersonalData($personalData);

        $siren = new Siren();
        $siren->setCreatedAt(new \DateTime());
        $siren->setName('Siren'.uniqid());
        $siren->setNumber(random_int(100000000,999999999));
        $siren->setMaxPersonnelNumbers(111);
        $business->addSiren($siren);

        $wizard = new Wizard();
        $wizard->setCreatedAt(new \DateTime());
        $wizard->complete();
        $business->setWizard($wizard);

        $manager->persist($business);
        $team->addBusiness($business);
        $manager->persist($team);
        $manager->flush();
    }
}
