Contributing
------------

Dsnplus-saas is an private project.

If you'd like to contribute, please read the following insctructions :

* Use issues / merge-requests in order to contribute to source code
* At least one code review has to be done before approval
* check gitlab ci pipeline after resolving merge-requests
* prod server is using master branch
* master branch is protected from pushes
* prod server is without composer dev dependencies
* prod server has composer optimized autoloader
* preprod server is using preprod
* preprod server has composer dev dependencies
