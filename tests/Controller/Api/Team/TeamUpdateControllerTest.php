<?php

namespace App\Tests\Controller\Api\Team;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamUpdateControllerTest extends WebTestCase
{

    public function testUpdate()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@dsnplus.com',
            'PHP_AUTH_PW' => 'tT1*testtest'
        ]);
        $client->xmlHttpRequest('PUT',
            '/api/teams/1',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'description' => 'team1-updated'
            ]));

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);

        $jsonData = json_decode((string)$client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertSame($jsonData['description'],'team1-updated');
    }

    public function testNotFoundUpdate()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@dsnplus.com',
            'PHP_AUTH_PW' => 'tT1*testtest'
        ]);
        $client->xmlHttpRequest('PUT',
            '/api/teams/0',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'description' => 'team1-updated'
            ])
        );

        $this->assertResponseStatusCodeSame(404);
    }
}
