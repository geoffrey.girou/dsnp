<?php

namespace App\Tests\Controller\Api\Team\Business;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamBusinessDeleteControllerTest extends WebTestCase
{

    public function testDelete()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@dsnplus.com',
            'PHP_AUTH_PW' => 'tT1*testtest'
        ]);
        $client->xmlHttpRequest('DELETE',
            '/api/teams/1/businesses/1',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
    }
}
