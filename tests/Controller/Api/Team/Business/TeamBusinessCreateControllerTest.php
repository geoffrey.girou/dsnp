<?php

namespace App\Tests\Controller\Api\Team\Business;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamBusinessCreateControllerTest extends WebTestCase
{

    public function testCreate()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@dsnplus.com',
            'PHP_AUTH_PW' => 'tT1*testtest'
        ]);
        $client->xmlHttpRequest('POST',
            '/api/teams/1/businesses',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"businessId":1}'
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
    }
}
