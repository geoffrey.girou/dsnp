<?php

namespace App\Tests\Controller\Api\Team\Manager;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamManagerCreateControllerTest extends WebTestCase
{

    public function testCreate()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@dsnplus.com',
            'PHP_AUTH_PW' => 'tT1*testtest'
        ]);
        $client->xmlHttpRequest('POST',
            '/api/teams/1/managers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'managerId' => 3
            ])
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
    }
}
