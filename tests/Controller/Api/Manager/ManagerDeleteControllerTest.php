<?php

namespace App\Tests\Controller\Api\Manager;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ManagerDeleteControllerTest extends WebTestCase
{
    public function testDelete()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@dsnplus.com',
            'PHP_AUTH_PW' => 'tT1*testtest'
        ]);
        $client->xmlHttpRequest('DELETE',
            '/api/managers/3',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
    }
}
