<?php

namespace App\Tests\Controller\Api\Manager;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ManagerCreateControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@dsnplus.com',
            'PHP_AUTH_PW' => 'tT1*testtest'
        ]);
        $client->xmlHttpRequest('POST',
            '/api/managers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'email' => 'new-admin@dsnplus.com'
            ]));

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);

        $jsonData = json_decode((string)$client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertSame($jsonData['email'],'new-admin@dsnplus.com');
    }
}
