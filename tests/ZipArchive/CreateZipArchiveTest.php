<?php

namespace Tests\ZipArchive;

use PHPUnit\Framework\TestCase;
use ZipArchive;

class CreateZipArchiveTest extends TestCase
{
    public function testCreateObject()
    {
        $savePath = __DIR__ . '/../../tmp/';

        $zip = new ZipArchive();
        $res = $zip->open($savePath . 'test.zip', ZipArchive::CREATE);

        $this->assertSame($res,true);

        $zip->addFromString('test.txt', 'file content goes here');
        
        $isClosed = $zip->close();

        $this->assertSame($isClosed,true);

        unlink($savePath . 'test.zip');
    }    
}