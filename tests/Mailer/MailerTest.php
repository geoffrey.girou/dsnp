<?php

namespace App\Tests\Mailer;

use App\Mailer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MailerTest extends KernelTestCase
{
    public function testSendMailOk()
    {
        self::bootKernel();

        $container = self::$container;

        $mailer = $container->get(Mailer\Mailer::class);

        $sent = $mailer->sendEmail(
            'test',
            'user@dsnplus.com',
            'test.html.twig'
        );

        $this->assertSame($sent,true);
    }    
}