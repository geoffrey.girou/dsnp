<?php


namespace Tests\Service\Jwt\Expiration;

use App\Service\Jwt\Expiration\JwtExpirationDate;
use App\Service\String\PolyfillPhp8;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertSame;

class DisplayExpirationDateTest extends TestCase
{
    public function testStringContainsTAndZ()
    {
        $jwtExpirationDate = new JwtExpirationDate();
        $string = $jwtExpirationDate->expirationDateStringForPayload();
        
        assertSame(PolyfillPhp8::polyfill_str_contains(
            $string,
            'T'
        ),true);

        assertSame(PolyfillPhp8::polyfill_str_contains(
            $string,
            'Z'
        ),true);
    }

    public function testStringLength()
    {
        $jwtExpirationDate = new JwtExpirationDate();
        $string = $jwtExpirationDate->expirationDateStringForPayload();
        
        assertSame(strlen($string),20);
    }
}