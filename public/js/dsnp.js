function dsnpJsFunc() {
  var
    isExpanded = false,
    isPersistant = true,
    menu = document.querySelector('.navbar'),
    menuBtn = document.querySelector('.navbar-button'),
    iframe = document.querySelector('#dashboardFrame');


  menuBtn.addEventListener('click', function (ev) {
    if (menu.classList.contains('expanded')) {
      menu.classList.remove('expanded');
    } else {
      menu.classList.add('expanded');
    }
    ev.stopPropagation();
  });

  window.addEventListener('click', function (ev) {
    menu.classList.remove('expanded');
  });

  window.addEventListener('beforeunload', function (ev) {
    var path = location.pathname.match(/^\/[^\/]+/)[0], target, href, frame;

    if (document.activeElement.classList.contains('navbar-item') || !iframe || !isPersistant) {
      document.cookie = 'dsnp-location=; path=' + path;
      document.cookie = 'dsnp-iframe-src=; path=' + path;
    } else {
      target = document.activeElement;

      try {
        if (target.contentDocument) {
          target = target.contentDocument.activeElement;
        }
      } catch(e) {
        // ignore error.
      }

      href = target.attributes['href'];
      frame = target.attributes['data-frame'];

      if (href && href.value && frame && frame.value) {
        document.cookie = 'dsnp-location=' + href.value + '; path=' + path;
        document.cookie = 'dsnp-iframe-src=' + frame.value + '; path=' + path;
      } else {
        document.cookie = 'dsnp-location=' + location.pathname + '; path=' + path;
        document.cookie = 'dsnp-iframe-src=' + iframe.contentWindow.location.href + '; path=' + path;
      }
    }
  });

  window.stopPersistance = function () {
    isPersistant = false;
  }
}
