# dsnp

User Access to dashboard

## Stack 

### Backend API
- PHP 8.0 (7.3+ comptatible)
- Composer (dependency manager)
- MariaDB 10.5.9
- Symfony Framework LTS 4.4
- PhpStan level 6

### Frontend app
- VueJS
- ESLint
- Yarn (dependency manager)

#### Libs
- [Element UI](https://element.eleme.io/#/fr-FR)
- Tippy (tooltip)
- VanillaToasts (toast)
- Cropper JS (crop)

## Docker

A Docker LEMP stack is provided for easy local development. It creates the following fully configured containers : 

- NginX
- MariaDB
- PHP
- Mail catcher (mailhog on port 8025)

DB volume is persistent.

## Install

Run :
```bash
$ git clone ssh://git@gitlab.com/geoffreygirou/dsnp.git
```
```bash
$ make start // starts containers
```
```bash
$ make bash // start a bash terminal inside php container
```

inside docker terminal

```bash
$ composer install
```
```bash
$ mkdir public/logo
```
go to http://dsnp/

## Configuration

Copy .env.example file which already use docker containers as default configuration.

Rename to .env and adjust configuration if necessary.

## DB
Set Up database
```bash
$ php bin/console doctrine:database:create
```
```bash
$ php bin/console doctrine:schema:create
```
### Migrate to the latest version
```bash
$ php bin/console doctrine:migrations:migrate
```
## Tests
Set Up test database
```bash
$ php bin/console --env=test doctrine:database:create
```
```bash
$ php bin/console --env=test doctrine:schema:create
```
Load fixtures
```bash
$ php bin/console --env=test doctrine:fixtures:load --purger=truncate_purger
```
Execute tests
```bash
$ php ./vendor/bin/phpunit
```


Note : [DAMA](https://github.com/dmaicher/doctrine-test-bundle) bundle provides auto-transactions (with rollback) for each tests.
## SSO

Provider list available for Oauth [knpuniversity](https://github.com/knpuniversity/oauth2-client-bundle)