build:
	@docker-compose -f docker-compose.yml build

start:
	@docker-compose -f docker-compose.yml up -d

start-build:
	@docker-compose -f docker-compose.yml up -d --build --force-recreate

stop:
	@docker-compose -f docker-compose.yml down

bash:
	@docker exec -ti dsnp-php bash

logs:
	@docker-compose logs -f dsnp-php

mstart:
	mutagen-compose up -d

mstop:
	docker cp dsnp-php:/var/www/vendor - | tar -x --directory .
	mutagen-compose down